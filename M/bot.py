from linepy import *
from akad.ttypes import *
from multiprocessing import Pool, Process
from datetime import datetime
from time import sleep
from bs4 import BeautifulSoup
from humanfriendly import format_timespan, format_size, format_number, format_length
import time, random, sys, json, codecs, threading, glob, re, string, os, requests, subprocess, ast, pytz, urllib.request, urllib.parse, urllib.error, urllib.parse
from gtts import gTTS
from googletrans import Translator
#==============================================================================#
botStart = time.time()
#================Q===============================================================#
with open("token.txt","r") as z:
    jepangpunya = z.read()
line = LINE(jepangpunya)
line.log("Auth Token : " + str(line.authToken))
line.log("Timeline Token : " + str(line.tl.channelAccessToken))

print ("Login Succes")

lineMID = line.profile.mid
lineProfile = line.getProfile()
lineSettings = line.getSettings()
zxcv = lineMID

oepoll = OEPoll(line)
readOpen = codecs.open("read.json","r","utf-8")
settingsOpen = codecs.open("temp.json","r","utf-8")
read = json.load(readOpen)
settings = json.load(settingsOpen)
Rfu = [line]
Exc = [line]
lineMID = line.getProfile().mid
bot1 = line.getProfile().mid
RfuBot =[lineMID]
Bots = [lineMID]
Family =[lineMID]
admin =[lineMID]
creator = [lineMID]
staff = [lineMID]
RfuFamily = RfuBot + Family + Bots + admin + staff
msg_dict = {}
msg_image={}
msg_video={}
msg_sticker={}
unsendchat = {}
temp_flood = {}
wbanlist = []
wblacklist = []
protectqr = []
protectkick = []
protectjoin = []
protectinvite = []
protectcancel = []
protectantijs = []
ghost = []
protectname = []
protecturl = []
protection = []
autocancel = {}
autoinvite = []
autoleaveroom = []
targets = []
items = []
pson = {"kw":{}}

welcome = []
simisimi = []
translateen = []
translateid = []
translateth = []
translatetw = []
translatear = []
#==============================================================================#

settings = {
	"RText": True,
    "limit": 10,
    "owner":{},
    "admin":{},
    "addadmin":False,
    "delladmin":False,
    "staff":{},
    "addstaff":False,
    "dellstaff":False,
    "bots":{},
    "selfbot": True,
    "addbots":False,
    "dellbots":False,
    "autoAdd": False,
    "autoBlock": False,
    "autoJoin": False,
    'autoCancel':{"on":True,"members":1},	
    "autoLeave": False,
    "autoRead": False,
    "autoReply": False,
    "botcancel": False,
    "leaveRoom": False,
    "detectMention": True,
    "checkSticker": False,
    "checkContact": False,
    "checkPost": False,
    "kickMention": False,
    "potoMention": False,
    "delayMention": False,
    "lang":"JP",
    "Wc": False,
    "Lv": False,
    "Nk": False,
    "Api": False,
    "Aip": False,
    "blacklist":{},
    "wbanlist":{},
    "winvite": False,
    "wblacklist": False,
    "dblacklist": False,
    "detectMentionPM": False,
    "dwhitelist": False,
    "gift": False,
    "likeOn": False,
    "timeline": False,
    "commentOn":True,
    "commentBlack":{},
    "wblack": False,
    "dblack": False,
    "clock": False,
    "cName":"",
    "cNames":"",
    "changeGroupPicture": [],
    "changePictureProfile": False,
    "kickContact": False,
    "changeVideo": False,
    "chatMessage": "dih",
    "Talkblacklist":{},
    "Talkwblacklist":False,
    "Talkdblacklist":False,
    "talkban":True,
    "unsendMessage": False,
    "autoJoinTicket": False,
    "welcome":"คุณยังไม่ได้ตั้งข้อความคนเข้า",
    "kick":"คุณยังไม่ได้ตั้งข้อความคนลบ",
    "leave":"คุณยังไม่ได้ตั้งข้อความคนออก",
    "Respontag":"คุณยังไม่ได้ตั้งข้อความแทค",
    "eror":"คุณใช้คำสั่งผิด กรุณาศึกษาวิธีใช้ หรือสอบถามกับผู้สร้าง โดยพิมคำสั่ง *.ผส*เพื่อแสดง คท ของผู้สร้าง",
    "spam":{},
    "invite": {},
    "winvite": False,
    "pname": {},
    "pro_name": {},
    "notag": False,
    "pcancel": False,
    "pinvite": False,
    "pmMessage": "ว่างัยครับว่างเดื้ยวเห็นข้อความจะมาตอบนะครับ",
    "qrp": False,
    "readerPesan": " แอบทมายเดะจิ้มตาบอด",
    "blockPesan": "ขออภัย...คุณถูกบล็อค",
    "replyPesan": "Sorry , i'm busy right now.",
    "responGc": True,
    "responcall": False,
    "responcallgc": False,
    "restartPoint": "c07540238e0ddffa5187313406f7f3c67",
    "server": "VPS",
    "ksticker": False,
    "userMentioned": False,
    "timeRestart": "18000",
    "message1":"รับแก้ไฟล์+เพิ่มไฟล์+แก้ภาษา\n💝ราคาดูที่หน้างาน💝\n👉มีบริการให้เช่าบอท\nราคา300บาทต่อเดือน💖\n#เพิ่มคิกเกอร์ตัวละ100👌\n🎀สนใจรีบทัก..บอทpython3ฟังชั่นล้นหลาม🎁กำลังรอให้คุณเป็นเจ้าของ\nselfbot by:\n╔══════════════┓\n╠™❍✯͜͡тєαмвσтмυѕι¢✯͜͡❂➣ \n╚══════════════┛",
    "message":"สวัสดีครับ เพื่อนใหม่ \nขอบคุณที่แอดผมเป็นเพื่อนนะครับ \nสนใจรับบริการเรื่องใด เชิญอ่านที่ใบโปรข้างล่างได้เลยครับ\n💝👇👇👇👇👇👇👇👇👇👇💝",
    "comment":"""  💗тєαмвσтмυѕι¢💗 
💗💗💗💗💗💗💗💗💗💗💗
📌บริการติดตั้งบอทสิริ V.10
📌มีบริการให้เช่าเซลบอทไลน์ 
📌ร่างครึ่งคนครึ่งบอท
📌ราคาว่ากันตามคุณภาพนะครับ
📌ราคา 300บาทต่อเดือน (ถูกมาก..)
📌เดือนต่อไป 200 บาท ตกลกกันได้
📌ดูแลใส่ใจตลอดเวลาหลังการติดตั้ง
📌สนใจสคริปบอทบิน ลิ้งบิน เราก็มี
📌สนใจทำเซลบอทเป็นเราก็พร้อมสอน
👉ยกเลิกข้อความได้
👉ดักจับคนยกเลิกข้อความได้
👉ป้องกันกลุ่มเจอบินได้
👉มีเปิดปิดแสกนคำหยาบกับบอทบิน
👉แอบดูคนอ่านแบบดึง คท.ได้
👉แทคได้ทั้งห้อง
👉แทคแบบสั่งจำนวนเป็นรายบุคคลได้
👉แทคแบบสั่งจำนวนในแชทส่วนตัวได้
👉รันแชทได้
👉สั่งบล็อคใครก็ได้
👉สั่งเพิ่มเพื่อนแบบแทคได้
👉ลบแชทได้
👉กันรันได้100%
👉ตั้งแอด-ตั้งรองได้
👉ป้องกันแบบเหมารวมหรือแบบแยกห้องได้
👉มุดลิ้งได้
👉เช็คโพส,เช็คคท,เช็คข้อมูลคนอื่นได้
👉เช็คข้อมูลตัวเอง,เช็คข้อมูลกลุ่มได้
👉ปฏิเสธคำเชิญแบบใส่ข้อความลงไปได้
👉ดึงห้องรวมได้
👉ตั้งปฏิเสธกลุ่มเชิญตามจำนวนสมาชิกได้
👉เล่นเซลในแชทสตได้
👉ตั้งข้อความคนเข้าคนออกได้
👉ตั้งข้อความคนลบสมาชิกได้
👉ตั้งข้อความคนแอดได้
👉สมารถเรียกดูการตั้งค่าข้อความได้ทั้งหมด
👉ตั้งหรือลบสติ๊กเกอร์ คนเข้า คนออก คนลบ
👉ตั้งหรือลบสติ๊กเกอร์ คนแอด คนแอบ คนแทค
👉ตั้งคำห้ามพิม ใครพิมจะโดนเตะ
👉ถามปัญหาได้ทุกคำถาม
👉สั่งเขียนข้อความได้ตามใจ
👉ดึงคท จากไอดีไลน์ หรือจาก mid ได้
👉อัพเดตลูกเล่นใหม่ๆทุกเดือน
🍷มีความสามารถอีกเยอะดูเอาระกัน🍷
🎀สนใจรีบทัก🎀
🎉บอทpython3ฟังชั่นล้นหลาม คุณภาพแน่นปึ๊ก
🎁กำลังรอให้คุณเป็นเจ้าของ....
╔══════════════┓
╠™❍✯͜͡тєαмвσтмυѕι¢✯͜͡❂➣ 
╚══════════════┛
🔝สนใจจิ้มลิ้งด้านล่างได้เลยคับ 👇👇👇👇

1. http://line.me/ti/p/~kaisexlove ☑K.ไก่

2. http://line.me/ti/p/vGEu0TKv4g ☑K.เอก

3. http://line.me/ti/p/FL39d-uDoQ ☑K.เนย

ขอแสดงความนับถือ
จากทีมงาน тєαмвσтмυѕι¢ ทุกคน
line://nv/recommendOA/@clh6202h""",
    "userAgent": [
        "Mozilla/5.0 (X11; U; Linux i586; de; rv:5.0) Gecko/20100101 Firefox/5.0",
        "Mozilla/5.0 (X11; U; Linux amd64; rv:5.0) Gecko/20100101 Firefox/5.0 (Debian)",
        "Mozilla/5.0 (X11; U; Linux amd64; en-US; rv:5.0) Gecko/20110619 Firefox/5.0",
        "Mozilla/5.0 (X11; Linux) Gecko Firefox/5.0",
        "Mozilla/5.0 (X11; Linux x86_64; rv:5.0) Gecko/20100101 Firefox/5.0 FirePHP/0.5",
        "Mozilla/5.0 (X11; Linux x86_64; rv:5.0) Gecko/20100101 Firefox/5.0 Firefox/5.0",
        "Mozilla/5.0 (X11; Linux x86_64) Gecko Firefox/5.0",
        "Mozilla/5.0 (X11; Linux ppc; rv:5.0) Gecko/20100101 Firefox/5.0",
        "Mozilla/5.0 (X11; Linux AMD64) Gecko Firefox/5.0",
        "Mozilla/5.0 (X11; FreeBSD amd64; rv:5.0) Gecko/20100101 Firefox/5.0",
        "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0",
        "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:5.0) Gecko/20110619 Firefox/5.0",
        "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:5.0) Gecko/20100101 Firefox/5.0",
        "Mozilla/5.0 (Windows NT 6.1; rv:6.0) Gecko/20100101 Firefox/5.0",
        "Mozilla/5.0 (Windows NT 6.1.1; rv:5.0) Gecko/20100101 Firefox/5.0",
        "Mozilla/5.0 (Windows NT 5.2; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0",
        "Mozilla/5.0 (Windows NT 5.1; U; rv:5.0) Gecko/20100101 Firefox/5.0",
        "Mozilla/5.0 (Windows NT 5.1; rv:2.0.1) Gecko/20100101 Firefox/5.0",
        "Mozilla/5.0 (Windows NT 5.0; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0",
        "Mozilla/5.0 (Windows NT 5.0; rv:5.0) Gecko/20100101 Firefox/5.0"
    ],
    "addPesan": "🙏สวัสดีครับ🙏เพื่อนใหม่\n👉สนใจทำเซลบอท แก้ไฟลเซล เพิ่มไฟลเซล สอนทำเซล กด1\n👉สนใจทำปก ทำดิส กด2\n👉สนใจสั่งตั๋วสิริ ลงบอทสิริ บอทapi กด3\n👉สนใจเช่าเซิฟ Vps กด4\n👉สนใจสั่งซื้อสติ๊กเกอร์ ทีมไลน์ กด5\n👉แอดมาเก็บคท กด6\n👉แอดแล้วไม่พูดไม่ทัก กดบล็อค\n👉แต่ถ้าทักแล้วไม่ตอบ. หรือตอบช้า โทรมาเบอร์นี้เลยคับ 094 634 5913 @!",
    "addSticker": {
        "name": "",
        "status": False,
    },
    "mentionPesan": " ว่าไง? ที่รัก􀄃􀈻",
    "messageSticker": {
        "addName": "",
        "addStatus": False,
        "listSticker": {
            "addSticker": {
                "STKID": "409",
                "STKPKGID": "1",
                "STKVER": "100"
            },
            "leaveSticker": {
                "STKID": "51626533",
                "STKPKGID": "11538",
                "STKVER": "1"
            },
            "kickSticker": {
                "STKID": "123",
                "STKPKGID": "1",
                "STKVER": "100"
            },
            "readerSticker": {
                "STKID": "51626530",
                "STKPKGID": "11538",
                "STKVER": "1"
            },
            "responSticker": {
                "STKID": "103",
                "STKPKGID": "1",
                "STKVER": "100"
            },
            "sleepSticker": "",
            "welcomeSticker": {
                "STKID": "51626527",
                "STKPKGID": "11538",
                "STKVER": "1"
            }
        }
    },
    "mimic": {
       "copy": False,
       "status": False,
       "target": {}
    }
}
RfuProtect = {
    "protect": False,
    "cancelprotect": False,
    "inviteprotect": False,
    "linkprotect": False,
    "Protectguest": False,
    "Protectjoin": False,
}

Setmain = {
    "foto": {},
}

read = {
    "readPoint": {},
    "readMember": {},
    "readTime": {},
    "setTime":{},
    "ROM": {}
}

myProfile = {
	"displayName": "",
	"statusMessage": "",
	"pictureStatus": ""
}

mimic = {
    "copy":False,
    "copy2":False,
    "status":False,
    "target":{}
    }
    
RfuCctv={
    "cyduk":{},
    "point":{},
    "sidermem":{}
}

rfuSet = {
    'setTime':{},
    'ricoinvite':{},
    'winvite':{},
    }

user1 = lineMID
user2 = ""
	
setTime = {}
setTime = rfuSet['setTime']

contact = line.getProfile() 
backup = line.getProfile() 
backup.dispalyName = contact.displayName 
backup.statusMessage = contact.statusMessage
backup.pictureStatus = contact.pictureStatus

mulai = time.time()
msg_dict = {}

myProfile["displayName"] = lineProfile.displayName
myProfile["statusMessage"] = lineProfile.statusMessage
myProfile["pictureStatus"] = lineProfile.pictureStatus
#==============================================================================#
with open('creator.json', 'r') as fp:
    creator = json.load(fp)
#with open('owner.json', 'r') as fp:
#    owner = json.load(fp)

#===============================================#
def RhyN_(to, mid):
    try:
        aa = '{"S":"0","E":"3","M":'+json.dumps(mid)+'}'
        text_ = '@Rh'
        line.sendMessage(to, text_, contentMetadata={'MENTION':'{"MENTIONEES":['+aa+']}'}, contentType=0)
    except Exception as error:
        logError(error)
#==============================================================================================================
def sendMention(to, mid, firstmessage, lastmessage):
    try:
        arrData = ""
        text = "%s " %(str(firstmessage))
        arr = []
        mention = "@x "
        slen = str(len(text))
        elen = str(len(text) + len(mention) - 1)
        arrData = {'S':slen, 'E':elen, 'M':mid}
        arr.append(arrData)
        text += mention + str(lastmessage)
        line.sendMessage(to, text, {'MENTION': str('{"MENTIONEES":' + json.dumps(arr) + '}')}, 0)
    except Exception as error:
        logError(error)
        line.sendMessage(to, "[ INFO ] Error :\n" + str(error))                        
def cTime_to_datetime(unixtime):
    return datetime.datetime.fromtimestamp(int(str(unixtime)[:len(str(unixtime))-3]))
def dt_to_str(dt):
    return dt.strftime('%H:%M:%S')
def waktu(secs):
	mins, secs = divmod(secs,60)
	hours, mins = divmod(mins,60)
	days,hours = divmod(hours,24)
	return '【%02d วัน %02d ชั่วโมง %02d นาที %02d วินาที】' % (days, hours, mins, secs)
def delExpire():
    if temp_flood != {}:
        for tmp in temp_flood:
            if temp_flood[tmp]["expire"] == True:
                if time.time() - temp_flood[tmp]["time"] >= 3*10:
                    temp_flood[tmp]["expire"] = False
                    temp_flood[tmp]["time"] = time.time()
                    try:
                        userid = "https://line.me/ti/p/~" + line.profile.userid
                        line.sendFooter(tmp, "Spam is over , Now Bots Actived !", str(userid), "http://dl.profile.line-cdn.net/"+line.getContact(lineMID).pictureStatus, line.getContact(lineMID).displayName)
                    except Exception as error:
                        logError(error)
                        
def load():
    global images
    global stickers
    with open("image.json","r") as fp:
        images = json.load(fp)
    with open("sticker.json","r") as fp:
        stickers = json.load(fp)
        
def sendSticker(to, version, packageId, stickerId):
    contentMetadata = {
        'STKVER': version,
        'STKPKGID': packageId,
        'STKID': stickerId
    }
    line.sendMessage(to, '', contentMetadata, 7)

def sendImage(to, path, name="image"):
    try:
        if settings["server"] == "VPS":
            line.sendImageWithURL(to, str(path))
    except Exception as error:
        logError(error)
        
def Rapid1Say(mtosay):
    line.sendText(Rapid1To,mtosay)

def delete_log():
    ndt = datetime.datetime.now()
    for data in msg_dict:
        if (datetime.datetime.utcnow() - cTime_to_datetime(msg_dict[data]["createdTime"])) > datetime.timedelta(1):
            del msg_dict[msg_id]
            
def changeVideoAndPictureProfile(pict, vids):
    try:
        files = {'file': open(vids, 'rb')}
        obs_params = line.genOBSParams({'oid': lineMID, 'ver': '2.0', 'type': 'video', 'cat': 'vp.mp4'})
        data = {'params': obs_params}
        r_vp = line.server.postContent('{}/talk/vp/upload.nhn'.format(str(line.server.LINE_OBS_DOMAIN)), data=data, files=files)
        if r_vp.status_code != 201:
            return "Failed update profile"
        line.updateProfilePicture(pict, 'vp')
        return "Success update profile"
    except Exception as e:
        raise Exception("Error change video and picture profile {}".format(str(e)))

def cloneProfile(mid):
    contact = line.getContact(mid)
    if contact.videoProfile == None:
        line.cloneContactProfile(mid)
    else:
        profile = line.getProfile()
        profile.displayName, profile.statusMessage = contact.displayName, contact.statusMessage
        line.updateProfile(profile)
        pict = line.downloadFileURL('http://dl.profile.line-cdn.net/' + contact.pictureStatus, saveAs="tmp/pict.bin")
        vids = line.downloadFileURL( 'http://dl.profile.line-cdn.net/' + contact.pictureStatus + '/vp', saveAs="tmp/video.bin")
        changeVideoAndPictureProfile(pict, vids)
    coverId = line.getProfileDetail(mid)['result']['objectId']
    line.updateProfileCoverById(coverId)
def download_page(url):
    version = (3,0)
    cur_version = sys.version_info
    if cur_version >= version:     
        import urllib,request
        try:
            headers = {}
            headers['User-Agent'] = "Mozilla/5.0 (Linux; Android 6.0; ALE-L21 Build/HuaweiALE-L21; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/68.0.3440.91 Mobile Safari/537.36"
            req = urllib,request.Request(url, headers = headers)
            resp = urllib,request.urlopen(req)
            respData = str(resp.read())
            return respData
        except Exception as e:
            print(str(e))
    else:                        
        import urllib2
        try:
            headers = {}
            headers['User-Agent'] = "Mozilla/5.0 (Linux; Android 6.0; ALE-L21 Build/HuaweiALE-L21; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/68.0.3440.91 Mobile Safari/537.36"
            req = urllib2.Request(url, headers = headers)
            response = urllib2.urlopen(req)
            page = response.read()
            return page
        except:
            return"Page Not found"
def atend():
    print("Saving")
    with open("Log_data.json","w",encoding='utf8') as f:
        json.dump(msg_dict, f, ensure_ascii=False, indent=4,separators=(',', ': '))
    print("BYE")

def download_page(url):
    version = (3,0)
    cur_version = sys.version_info
    if cur_version >= version:     
        import urllib,request    
        try:
            headers = {}
            headers['User-Agent'] = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"
            req = urllib,request.Request(url, headers = headers)
            resp = urllib,request.urlopen(req)
            respData = str(resp.read())
            return respData
        except Exception as e:
            print(str(e))
    else:                        
        import urllib2
        try:
            headers = {}
            headers['User-Agent'] = "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 Safari/537.17"
            req = urllib2.Request(url, headers = headers)
            response = urllib2.urlopen(req)
            page = response.read()
            return page
        except:
            return"Page Not found"

def _images_get_next_item(s):
    start_line = s.find('rg_di')
    if start_line == -1:    
        end_quote = 0
        link = "no_links"
        return link, end_quote
    else:
        start_line = s.find('"class="rg_meta"')
        start_content = s.find('"ou"',start_line+90)
        end_content = s.find(',"ow"',start_content-90)
        content_raw = str(s[start_content+6:end_content-1])
        return content_raw, end_content


def _images_get_all_items(page):
    items = []
    while True:
        item, end_content = _images_get_next_item(page)
        if item == "no_links":
            break
        else:
            items.append(item)      
            time.sleep(0.1)        
            page = page[end_content:]
    return items
def backupData():
    try:
        backup = settings
        f = codecs.open('temp.json','w','utf-8')
        json.dump(backup, f, sort_keys=True, indent=4, ensure_ascii=False)
        backup = read
        f = codecs.open('read.json','w','utf-8')
        json.dump(backup, f, sort_keys=True, indent=4, ensure_ascii=False)
        return True
    except Exception as error:
        logError(error)
        return False

def sendAudio(self, to_, path):
       M = Message()
       M.text = None
       M.to = to_
       M.contentMetadata = None
       M.contentPreview = None
       M.contentType = 3
       M_id = self._client.sendMessage(0,M).id
       files = {
             'file': open(path,  'rb'),
       }
    
def sendImage(self, to_, path):
      M = Message(to=to_, text=None, contentType = 1)
      M.contentMetadata = None
      M.contentPreview = None
      M2 = self._client.sendMessage(0,M)
      M_id = M2.id
      files = {
         'file': open(path, 'rb'),
      }
      params = {
         'name': 'media',
         'oid': M_id,
         'size': len(open(path, 'rb').read()),
         'type': 'image',
         'ver': '1.0',
      }
      data = {
         'params': json.dumps(params)
      }
      r = self.post_content('https://obs-sg.line-apps.com/talk/m/upload.nhn', data=data, files=files)
      if r.status_code != 201:
         raise Exception('Upload image failure.')
      return True


def sendImageWithURL(self, to_, url):
      path = '%s/pythonLine-%i.data' % (tempfile.gettempdir(), randint(0, 9))
      r = requests.get(url, stream=True)
      if r.status_code == 200:
         with open(path, 'w') as f:
            shutil.copyfileobj(r.raw, f)
      else:
         raise Exception('Download image failure.')
      try:
         self.sendImage(to_, path)
      except:
         try:
            self.sendImage(to_, path)
         except Exception as e:
            raise e

def sendAudioWithURL(self, to_, url):
        path = self.downloadFileWithURL(url)
        try:
            self.sendAudio(to_, path)
        except Exception as e:
            raise Exception(e)

def sendAudioWithUrl(self, to_, url):
        path = '%s/pythonLine-%1.data' % (tempfile.gettempdir(), randint(0, 9))
        r = requests.get(url, stream=True, verify=False)
        if r.status_code == 200:
           with open(path, 'w') as f:
              shutil.copyfileobj(r.raw, f)
        else:
           raise Exception('Download audio failure.')
        try:
            self.sendAudio(to_, path)
        except Exception as e:
            raise e
            
def downloadFileWithURL(self, fileUrl):
        saveAs = '%s/pythonLine-%i.data' % (tempfile.gettempdir(), randint(0, 9))
        r = self.get_content(fileUrl)
        if r.status_code == 200:
            with open(saveAs, 'wb') as f:
                shutil.copyfileobj(r.raw, f)
            return saveAs
        else:
            raise Exception('Download file failure.')

def summon(to, nama):
    aa = ""
    bb = ""
    strt = int(14)
    akh = int(14)
    nm = nama
    for mm in nm:
      akh = akh + 2
      aa += """{"S":"""+json.dumps(str(strt))+""","E":"""+json.dumps(str(akh))+""","M":"""+json.dumps(mm)+"},"""
      strt = strt + 6
      akh = akh + 4
      bb += "\xe2\x95\xa0 @x \n"
    aa = (aa[:int(len(aa)-1)])
    msg = Message()
    msg.to = to
    msg.text = "\xe2\x95\x94\xe2\x95\x90\xe2\x95\x90\xe2\x95\x90\xe2\x95\x90\xe2\x95\x90\xe2\x95\x90\xe2\x95\x90\xe2\x95\x90\xe2\x95\x90\xe2\x95\x90\n"+bb+"\xe2\x95\x9a\xe2\x95\x90\xe2\x95\x90\xe2\x95\x90\xe2\x95\x90\xe2\x95\x90\xe2\x95\x90\xe2\x95\x90\xe2\x95\x90\xe2\x95\x90\xe2\x95\x90"
    msg.contentMetadata ={'MENTION':'{"MENTIONEES":['+aa+']}','EMTVER':'4'}
    print ("TAG ALL")
    try:
       line.sendMessage(msg)
    except Exception as error:
       print(error)
def restartBot():
    print ("RESTART SERVER")
    time.sleep(3)
    python = sys.executable
    os.execl(python, python, *sys.argv)
    
def logError(text):
    line.log("[ แจ้งเตือน ] " + str(text))
    time_ = datetime.now()
    with open("errorLog.txt","a") as error:
        error.write("\n[%s] %s" % (str(time), text))

def sendMessage(to, text, contentMetadata={}, contentType=0):
    mes = Message()
    mes.to, mes.from_ = to, profile.mid
    mes.text = text
    mes.contentType, mes.contentMetadata = contentType, contentMetadata
    if to not in messageReq:
        messageReq[to] = -1
    messageReq[to] += 1
        
def sendMessageWithMention(to, mid):
    try:
        aa = '{"S":"0","E":"3","M":'+json.dumps(mid)+'}'
        text_ = '@x '
        line.sendMessage(to, text_, contentMetadata={'MENTION':'{"MENTIONEES":['+aa+']}'}, contentType=0)
    except Exception as error:
        logError(error)
        
def mentionMembers(to, mid):
    try:
        arrData = ""
        textx = "╔══[ชื่อกลุ่ม {} ]\n╠".format(str(line.getGroup(to).name))
        arr = []
        no = 0 + 1
        for i in mid:
            mention = "@x\n"
            slen = str(len(textx))
            elen = str(len(textx) + len(mention) - 1)
            arrData = {'S':slen, 'E':elen, 'M':i}
            arr.append(arrData)
            textx += mention
            if no < len(mid):
                no += 1
                textx += "╠ "
            else:
                try:
                    textx += "╚══[จำนวนคนที่แท็ก {} คน] ".format(str(len(mid)))
                except:
                    pass
        line.sendMessage(to, textx, {'MENTION': str('{"MENTIONEES":' + json.dumps(arr) + '}')}, 0)
    except Exception as error:
        logError(error)
        line.sendMessage(to, "[ INFO ] Error :\n" + str(error))

def myhelp():
    myHelp = """╔═══════════════┓
╠™❍✯͜͡тєαмвσтмυѕι¢✯͜͡❂➣ 
╚═══════════════┛
 ────┅═ই۝ई═┅────
             คำสั่งในกลุ่ม
 ────┅═ই۝ई═┅────
╔═══════════════┓
╠═══════════════┛
║🌀❂➣โหมดเช็คข้อมูล👀
╠════════════════
╠❂➣ ที่รัก
╠❂➣ เช็ค [เช็คการตั้งค่า]
╠❂➣ ผส [คท.ผู้สร้าง]
╠❂➣ ข้อมูล [ข้อมูลตัวเอง]
╠❂➣ ข้อมูล @ [ข้อมูลคนแทค]
╠❂➣ ไอดีไลน์ [id]
╠❂➣ คท: [mid]
╠═════════════════
║🌀❂➣โหมดคำสั่ง👀
╠═════════════════
╠❂➣ รัก   [ชุดคำสั่ง]
╠❂➣ รัก1 [คำสั่งเซลบอท]
╠❂➣ รัก2 [คำสั่งในกลุ่ม]
╠❂➣ รัก3 [คำสั่งตั้งค่า]
╠❂➣ รัก4 [คำสั่งโซเชล]
╠═════════════════
║🌀โหมดตรวจสอบ👀
╠═════════════════
╠❂➣ ไอดี @ [ไอดีคนแทค]
╠❂➣ ชื่อ @ [ชื่อคนแทค]
╠❂➣ ตัส @ [สเตตัสคนแทค]
╠❂➣ รูปโปร @ [รูปโปรคนแทค]
╠❂➣ รูปปก @ [รูปปกคนแทค]
╠❂➣ คท @ [คทคนแท็ก]
╠❂➣ วีดีโอโปร @ [วีดีโอคนแทค]
╠❂➣ ไอดีล่อง [ไอดีล่องหน]
╠❂➣ คทล่อง [คทล่องหน]
╠❂➣ แทคล่อง [แทคร่องหน]
╠❂➣ ปฏิทิน [เช็ควันเวลา]
╠═════════════════
║🌀โหมดลอกเลียนแบบ👀
╠═════════════════
╠❂➣ Mimic on/off [เปิด/ปิด]
╠❂➣ MimicList [ชื่อเลียนแบบ]
╠❂➣ MimicAdd @ [เพิ่มรายชื่อ]
╠❂➣ MimicDel @ [ลบรายชื่อ]
╠═════════════════
║🌀โหมดประกาศข้อความ👀
╠═════════════════
╠❂➣ ส่งเสียงกลุ่ม [ข้อความ]
╠❂➣ ส่งเสียงแชท [ข้อความ]
╠❂➣ ประกาศกลุ่ม [ข้อความ]
╠❂➣ ประกาศแชท [ข้อความ]
╠❂➣ ส่งรูปภาพตามกลุ่ม [ลิ้งรูป]
╠❂➣ ส่งรูปภาพตามแชท [ลิ้งรูป]
╠═════════════════
╠❂➣ เริ่มใหม่ [รีบูสระบบใหม่]
╠❂➣ ออน [เช็คเวลาออน]
╠❂➣ Sayonara [ออกจากกลุ่ม]
╠❂➣ ออกทุกกลุ่ม [ออกทุกกลุ่ม]
╠═════════════════
╰═✰™❍✯͜͡тєαмвσтмυѕι¢✯͜͡❂➣"""
    return myHelp

def listgrup():
    listGrup = """╔═══════════════┓
╠™❍✯͜͡тєαмвσтмυѕι¢✯͜͡❂➣ 
╚═══════════════┛
 ────┅═ই۝ई═┅────
             คำสั่งในกลุ่ม
 ────┅═ই۝ई═┅────
╔═══════════════┓
╠❂➣ คทแอด
╠❂➣ คทรอง
╠❂➣ คทบอท
╠❂➣ ตั้งแอด @
╠❂➣ ลบแอด @
╠❂➣ ตั้งรอง @
╠❂➣ ลบรอง @
╠❂➣ ตั้งบอท @
╠❂➣ ลบบอท @
╠❂➣ เพิ่มแอด
╠❂➣ ลบแอด
╠❂➣ เพิ่มรอง
╠❂➣ ลบรอง
╠❂➣ เพิ่มบอท
╠❂➣ ลบบอท
╠❂➣ รายชื่อแอดมิน
╠❂➣ รายชื่อรอง
╠❂➣ รายชื่อบอท
╠❂➣ รายชื่อป้องกัน
╠❂➣ ชื่อกลุ่ม
╠❂➣ ไอดีกลุ่ม
╠❂➣ เปิดลิ้ง
╠❂➣ ปิดลิ้ง
╠❂➣ ลิ้ง
╠❂➣ ลิ้งกลุ่ม
╠❂➣ กลุ่ม
╠❂➣ เช็คกลุ่ม @
╠❂➣ สมาชิกกลุ่ม
╠❂➣ ข้อมูลกลุ่ม
╠❂➣ รูปกลุ่ม
╠❂➣ แทค [แทคต่ำกว่า20]
╠❂➣ เช็คไอดี
╠❂➣ ไอดีล่อง
╠❂➣ คทล่อง
╠❂➣ แทคล่อง
╠❂➣ แสปมแทค [จำนวน] @
╠❂➣ คลอ [จำนวน]
╠❂➣ โทร [จำนวน]
╠❂➣ ลบข้อความ [จำนวน]
╠❂➣ แทคล่อง
╠❂➣ จับ
╠❂➣ เลิกจับ
╠❂➣ จับใหม่
╠❂➣ อ่าน
╠❂➣ ของขวัญ
╠❂➣ เพิ่มเพื่อน @
╠❂➣ คำห้ามพิม [ข้อความ]
╠❂➣ เช็คคำห้ามพิม
╠❂➣ ล้างคำห้ามพิม [ข้อความ]
╠❂➣ แจก [จำนวน] @
╠❂➣ เช็คดำ
╠❂➣ ลงดำ
╠❂➣ ล้างดำ
╠❂➣ ไล่ดำ
╠❂➣ คทดำ
╠❂➣ ซามู [บินห้อง]
╰═✰™❍✯͜͡тєαмвσтмυѕι¢✯͜͡❂➣"""
    return listGrup

def socmedia():
    socMedia = """╔══════════════┓
╠™❍✯͜͡тєαмвσтмυѕι¢✯͜͡❂➣
╚══════════════┛
 ────┅═ই۝ई═┅────
        คำสั่งโซเชลมีเดี่ย
 ────┅═ই۝ई═┅────
╔══════════════┓
╠❂➣ ปฏิทิน
╠❂➣ ค้นหายูทูป [ข้อความ]
╠❂➣ ค้นหาเพลง [ชื่อเพลง]
╠❂➣ ขอเพลง [ชื่อเพลง]
╠❂➣ Lyric
╠❂➣ ScreenshootWebsite
╠❂➣ ค้นหาหนัง [ชื่อหนัง]
╠❂➣ ดึงวีดีโอ [ชื่อวีดีโอ]
╠❂➣ ดึงรูป [ชื่อรูป]
╠❂➣ ไอจี [ชื่อยูส]
╠❂➣ Urban
╠❂➣ เพลสโต [ชื่อแอพ]
╠❂➣ เฟสบุค [ข้อความ]
╠❂➣ Github. [ข้อความ]
╠❂➣ กูเกิ้ล [ข้อความ]
╰═✰™❍✯͜͡тєαмвσтмυѕι¢✯͜͡❂➣"""
    return socMedia

def helpset():
    helpSet = """╔══════════════┓
╠™❍✯͜͡тєαмвσтмυѕι¢✯͜͡❂➣
╚══════════════┛
  ────┅═ই۝ई═┅────
           คำสั่งเซลบอท
  ────┅═ই۝ई═┅────
╔═══════════════
║🌀โหมดเช็คข้อมูลตัวเอง👀
╠═══════════════
╠❂➣ โย่ว
╠❂➣ ไอดี
╠❂➣ ชื่อ
╠❂➣ ตัส
╠❂➣ รูปโปร
╠❂➣ รูปปก
╠❂➣ สุดหล่อ
╠❂➣ ข้อมูล
╠❂➣ วัดใจ
╠❂➣ Sp
╠❂➣ เพื่อน
╠❂➣ ไอดีเพื่อน
╠❂➣ กลุ่ม
╠❂➣ เช็ค
╠════════════════
║🌀โหมดเช็คค่าข้อความ👀
╠════════════════
╠❂➣ ทักเข้า
╠❂➣ ติ๊กคนเข้า
╠❂➣ ทักออก
╠❂➣ ติ๊กคนออก
╠❂➣ ทักเตะ
╠❂➣ ติ๊กคนเตะ
╠❂➣ คอมเม้น
╠❂➣ ข้อความแทค
╠❂➣ ติ๊กคนแทค
╠❂➣ ข้อความแอด
╠❂➣ ติ๊กคนแอด
╠════════════════
║🌀โหมดตั้งค่าข้อความ👀
╠════════════════
╠❂➣ ชื่อ:
╠❂➣ ตัส:
╠❂➣ ทักเข้า ตามด้วยข้อความ
╠❂➣ ตั้งติ๊กคนเข้า
╠❂➣ ลบติ๊กคนเข้า
╠❂➣ ทักออก ตามด้วยข้อความ
╠❂➣ ตั้งติ๊กคนออก
╠❂➣ ลบติ๊กคนออก
╠❂➣ ทักเตะ ตามด้วยข้อความ
╠❂➣ ตั้งติ๊กคนเตะ
╠❂➣ ลบติ๊กคนเตะ
╠❂➣ ตั้งแทค ตามด้วยข้อความ
╠❂➣ ตั้งติ๊กคนแทค
╠❂➣ ลบติ๊กคนแทค
╠❂➣ ตั้งแอด ตามด้วยข้อความ
╠❂➣ ตั้งติ๊กคนแอด
╠❂➣ ลบติ๊กคนแอด
╠❂➣ ตั้งทักแชท:
╠❂➣ คอมเม้น:
╠════════════════
║🌀โหมดคำสั่งอันตราย👀
╠════════════════
╠❂➣ ดำ
╠❂➣ ขาว
╠❂➣ แบน @
╠❂➣ ลบแบน @
╠❂➣ แบนโทร @
╠❂➣ ลบแบนโทร @
╠❂➣ บล็อค @
╠❂➣ ดึง
╠❂➣ ดึง: [ไอดี]
╠❂➣ ทัก [จำนวน] (แชท.สต)
╠❂➣ หวด @
╠❂➣ หวด: [ไอดี]
╠❂➣ สอย @
╠❂➣ ลาก่อย @
╠❂➣ ปลิว @
╠❂➣ ดับไฟ [ไวรัสGOT]
╠❂➣ ดับไฟ [จำนวน] @
╠❂➣ ปวดตับ
╠❂➣ แปลงโฉม
╠❂➣ โหลด
╠❂➣ ก๊อป @
╠❂➣ คืนร่าง
╠❂➣ Gcancel:(จำนวนสมาชิก)
╰═✰™❍✯͜͡тєαмвσтмυѕι¢✯͜͡❂➣

**คำเตือน** คำสั่งในโหมดอันตราย
ควรสอบถามวิธีใช้กับผู้สร้างก่อนใช้"""
    return helpSet

def helpsetting():
    helpSetting = """╔══════════════┓
╠™❍✯͜͡тєαмвσтмυѕι¢✯͜͡❂➣
╚══════════════┛
 ────┅═ই۝ई═┅────
         คำสั่งการตั้งค่า
 ────┅═ই۝ई═┅────
╔══════════════┓
╠❂➣ เปิดกัน/ปิดกัน
╠❂➣ กันยก/ปิดกันยก
╠❂➣ กันเชิญ/ปิดกันเชิญ
╠❂➣ กันลิ้ง/ปิดกันลิ้ง
╠❂➣ กันเข้า/ปิดกันเข้า
╠❂➣ เปิดหมด/ปิดหมด
╠❂➣ เปิดกทม/ปิดกทม
╠❂➣ เปิดเข้า/ปิดเข้า
╠❂➣ เปิดออก/ปิดออก
╠❂➣ เปิดติ๊ก/ปิดติ๊ก
╠❂➣ เปิดบล็อค/ปิดบล็อค
╠❂➣ เปิดมุด/ปิดมุด
╠❂➣ เปิดเผือก/ปิดเผือก
╠❂➣ เปิดอ่าน/ปิดอ่าน
╠❂➣ เปิดพูด/ปิดพูด
╠❂➣ เปิดแทค/ปิดแทค
╠❂➣ เปิดแทค1/ปิดแทค1
╠❂➣ เปิดแทค2/ปิดแทค2
╠❂➣ เปิดแทคเจ็บ/ปิดแทคเจ็บ
╠❂➣ เปิดคท/ปิดคท
╠❂➣ เปิดตรวจสอบ/ปิดตรวจสอบ
╠❂➣ เปิดเช็คโพส/ปิดเช็คโพส
╠❂➣ เปิดสแกน/ปิดสแกน
╠❂➣ เปิดรับแขก/ปิดรับแขก
╠❂➣ เปิดส่งแขก/ปิดส่งแขก
╠❂➣ เปิดทักเตะ/ปิดทักเตะ
╠❂➣ เปิดข้อความ/ปิดข้อความ
╠❂➣ เปิดแทคแชท/ปิดแทคแชท
╠══════════════════
║🌱คำสั่งแบบแยกห้อง🌱
╠══════════════════
╠❂➣ ต้อนรับ เปิด/ปิด
╠❂➣ กันลิ้ง เปิด/ปิด
╠❂➣ กันเชิญ เปิด/ปิด
╠❂➣ กันเข้า เปิด/ปิด
╠❂➣ กันยก เปิด/ปิด
╠❂➣ กันเตะ เปิด/ปิด
╰══™❍✯͜͡тєαмвσтмυѕι¢✯͜͡❂➣"""
    return helpSetting

#==============================================================================#
def lineBot(op):
    global time
    global ast
    global groupParam
    try:
        if op.type == 0:
            return
        
        if op.type == 11:
            if op.param1 in protectqr:
                try:
                    if line.getGroup(op.param1).preventedJoinByTicket == False:
                        if op.param2 not in Bots and op.param2 not in admin and op.param2 not in staff:
                            line.kickoutFromGroup(op.param1,[op.param2])
                            X = line.getGroup(op.param1)
                            X.preventedJoinByTicket = True
                            line.updateGroup(X)
                            line.sendMessage(op.param1, text=None, contentMetadata={'mid': op.param2}, contentType=13)
                except:
                    pass
                                                
        if op.type == 13:
            if lineMID in op.param3:
                if settings["autoLeave"] == True:
                    if op.param2 not in Bots and op.param2 not in admin and op.param2 not in staff:
                        line.acceptGroupInvitation(op.param1)
                        ginfo = line.getGroup(op.param1)
                        line.sendMessage(op.param1,"บ๊าย..บาย\nกลุ่ม " +str(ginfo.name))
                        line.leaveGroup(op.param1)
                    else:
                        line.acceptGroupInvitation(op.param1)
                        ginfo = line.getGroup(op.param1)
                        line.sendMessage(op.param1,"สวัสดีครับ" + str(ginfo.name))

        if op.type == 13:
            if lineMID in op.param3:
                if settings["autoJoin"] == True:
                    if op.param2 not in Bots and op.param2 not in admin and op.param2 not in staff:
                        line.acceptGroupInvitation(op.param1)
                        ginfo = line.getGroup(op.param1)
                        line.sendMessage(op.param1,"สวัสดีครับ ^^")
                    else:
                        line.acceptGroupInvitation(op.param1)
                        ginfo = line.getGroup(op.param1)
                        line.sendMessage(op.param1,"สวัสดีครับ ^^")

        if op.type == 13:
            if op.param1 in protectinvite:
                if op.param2 not in Bots and op.param2 not in admin and op.param2 not in staff:
                    try:
                        group = line.getGroup(op.param1)
                        gMembMids = [contact.mid for contact in group.invitee]
                        for _mid in gMembMids:
                            line.kickoutFromGroup(op.param1,[op.param2])
                    except:
                        try:
                            group = line.getGroup(op.param1)
                            gMembMids = [contact.mid for contact in group.invitee]
                            for _mid in gMembMids:
                                line.kickoutFromGroup(op.param1,[op.param2])
                        except:
                            pass
                
        if op.type == 17:
            if op.param2 in settings["blacklist"]:
                line.kickoutFromGroup(op.param1,[op.param2])
            else:
                pass

        if op.type == 17:
            if op.param1 in protectjoin:
                if op.param2 not in Bots and op.param2 not in admin and op.param2 not in staff:
                    settings["blacklist"][op.param2] = True
                    try:
                        if op.param3 not in wait["blacklist"]:
                         line.kickoutFromGroup(op.param1,[op.param2])
                    except:
                        pass
                return

        if op.type == 0:
            return
        if op.type == 5:
            if settings["autoAdd"] == True:
                line.findAndAddContactsByMid(op.param1)
                line.sendMessage(op.param1, settings["message"])
                line.sendMessage(op.param1,str(settings["comment"]))
                line.sendMessage(op.param1,str(settings["addPesan"]))
                line.sendMessage(op.param1, None, contentMetadata={'mid': lineMID}, contentType=13)
            msgSticker = settings["messageSticker"]["listSticker"]["addSticker"]
            if msgSticker != None:
                sid = msgSticker["STKID"]
                spkg = msgSticker["STKPKGID"]
                sver = msgSticker["STKVER"]
                sendSticker(op.param1, sver, spkg, sid)
        if op.type == 5:
            if settings["autoBlock"] == True:
                line.blockContact(op.param1)
            msgSticker = settings["messageSticker"]["listSticker"]["addSticker"]
            if msgSticker != None:
                sid = msgSticker["STKID"]
                spkg = msgSticker["STKPKGID"]
                sver = msgSticker["STKVER"]
                sendSticker(op.param1, sver, spkg, sid)
#===========KICK============#
        if op.type == 19:
            if op.param1 in protectkick:
                if op.param2 not in Bots and op.param2 not in admin and op.param2 not in staff:
                    settings["blacklist"][op.param2] = True
                    line.kickoutFromGroup(op.param1,[op.param2])
                    #random.choice(ABC).kickoutFromGroup(op.param1,[op.param2])
                else:
                    pass
                
#===========Cancel============#

        if op.type == 32:
            if op.param1 in protectcancel:
              if op.param3 in Bots:    
                if op.param2 not in Bots and op.param2 not in admin and op.param2 not in staff:
                    settings["blacklist"][op.param2] = True
                    try:
                        if op.param3 not in settings["blacklist"]:
                            line.kickoutFromGroup(op.param1,[op.param2])
                    except:
                        try:
                            if op.param3 not in settings["blacklist"]:
                                line.kickoutFromGroup(op.param1,[op.param2])
                        except:
                            pass
                return

        if op.type == 19:
            if admin in op.param3:
                if op.param2 in Bots:
                    pass
                if op.param2 in Family:
                    pass
                if op.param2 in admin:
                    pass
                if op.param2 in staff:
                    pass
                else:
                    settings["blacklist"][op.param2] = True
                    try:
                        line.findAndAddContactsByMid(op.param1,[admin])
                        line.inviteIntoGroup(op.param1,[admin])
                        line.kickoutFromGroup(op.param1,[op.param2])
                    except:
                        try:
                            line.findAndAddContactsByMid(op.param1,[admin])
                            line.inviteIntoGroup(op.param1,[admin])
                            line.kickoutFromGroup(op.param1,[op.param2])
                        except:
                            pass

                return

            if staff in op.param3:
                if op.param2 in Bots:
                    pass
                if op.param2 in Family:
                    pass
                if op.param2 in admin:
                    pass
                if op.param2 in staff:
                    pass
                else:
                    settings["blacklist"][op.param2] = True
                    try:
                        line.findAndAddContactsByMid(op.param1,[staff])
                        line.inviteIntoGroup(op.param1,[staff])
                        line.kickoutFromGroup(op.param1,[op.param2])
                    except:
                        try:
                            line.findAndAddContactsByMid(op.param1,[staff])
                            line.inviteIntoGroup(op.param1,[staff])
                            line.kickoutFromGroup(op.param1,[op.param2])
                        except:
                            pass

                return
        if op.type == 13:
            if lineMID in op.param3:
                G = line.getGroup(op.param1)
                if settings["autoJoin"] == True:
                    if settings["autoCancel"]["on"] == True:
                        if len(G.members) <= settings["autoCancel"]["members"]:
                            line.rejectGroupInvitation(op.param1)
                        else:
                            line.acceptGroupInvitation(op.param1)
                    else:
                        line.acceptGroupInvitation(op.param1)
                elif settings["autoCancel"]["on"] == True:
                    if len(G.members) <= settings["autoCancel"]["members"]:
                        line.rejectGroupInvitation(op.param1)
            else:
                Inviter = op.param3.replace(" ",',')
                InviterX = Inviter.split(",")
                matched_list = []
                for tag in settings["blacklist"]:
                    matched_list+=[str for str in InviterX if str == tag]
                if matched_list == []:
                    pass
                else:
                    line.cancelGroupInvitation(op.param1, matched_list)
        if op.type == 19:
            contact1 = line.getContact(op.param2)
            group = line.getGroup(op.param1)
            contact2 = line.getContact(op.param3)
            #print(("[19] กลุ่มชื่อ: " + str(group.name) + "\n" + op.param1 +"\n踢人者: " + contact1.displayName + "\nMid:" + contact1.mid + "\n被踢者: " + contact2.displayName + "\nMid:" + contact2.mid ))
            if RfuProtect["protect"] == True:
                if op.param2 in admin:
                    pass
                else:
                    if settings["kickContact"] == True:
                        line.kickoutFromGroup(op.param1,[op.param2])
                        settings["blacklist"][op.param2] = True
                        time.sleep(0.1)
                        line.sendReplyMessage(msg.id,op.param1, "คนเตะมีดังนี้！")
                        line.sendContact(op.param1,op.param2)
                        time.sleep(0.1)
                        line.sendReplyMessage(msg.id,op.param1, "ผู้ที่ถูกเตะมีดังนี้！")
                        line.sendContact(op.param1,op.param3)
                    else:
                        line.kickoutFromGroup(op.param1,[op.param2])
                        settings["blacklist"][op.param2] = True
                        time.sleep(0.1)
            else:
                if settings["kickContact"] == True:
                    line.sendReplyMessage(msg.id,op.param1, "คนเตะ！")
                    line.sendContact(op.param1,op.param2)
                    time.sleep(0.1)
                    line.sendReplyMessage(msg.id,op.param1, "ผู้ที่ถูกเตะ！")
                    line.sendContact(op.param1,op.param3)
                else:
                    pass
        if op.type == 15:
            print ("[ 15 ]  NOTIFIED LEAVE GROUP")
            if op.param1 in welcome:
                if "{gname}" in settings['leave']:
                    gName = line.getGroup(op.param1).name
                    msg = settings['leave'].replace("{gname}", gName)
                    msgSticker = settings["messageSticker"]["listSticker"]["leaveSticker"]
                    if msgSticker != None:
                        sid = msgSticker["STKID"]
                        spkg = msgSticker["STKPKGID"]
                        sver = msgSticker["STKVER"]
                        sendSticker(op.param2, sver, spkg, sid)
                msgSticker = settings["messageSticker"]["listSticker"]["leaveSticker"]
                if msgSticker != None:
                    sid = msgSticker["STKID"]
                    spkg = msgSticker["STKPKGID"]
                    sver = msgSticker["STKVER"]
                    sendSticker(op.param1, sver, spkg, sid)
        if op.type == 19:
            if op.param1 in welcome:
                if "{gname}" in settings['kick']:
                    gName = line.getGroup(op.param1).name
                    msg = settings['kick'].replace("{gname}", gName)
                    msgSticker = settings["messageSticker"]["listSticker"]["kickSticker"]
                    if msgSticker != None:
                        sid = msgSticker["STKID"]
                        spkg = msgSticker["STKPKGID"]
                        sver = msgSticker["STKVER"]
                        sendSticker(op.param2, sver, spkg, sid)
                msgSticker = settings["messageSticker"]["listSticker"]["kickSticker"]
                if msgSticker != None:
                    sid = msgSticker["STKID"]
                    spkg = msgSticker["STKPKGID"]
                    sver = msgSticker["STKVER"]
                    sendSticker(op.param1, sver, spkg, sid)
        if op.type == 17:
            print ("[ 17 ]  NOTIFIED ACCEPT GROUP INVITATION")
            if op.param1 in welcome:
                if "{gname}" in settings['welcome']:
                    gName = line.getGroup(op.param1).name
                    msg = settings['welcome'].replace("{gname}", gName)
                    msgSticker = settings["messageSticker"]["listSticker"]["welcomeSticker"]
                    if msgSticker != None:
                        sid = msgSticker["STKID"]
                        spkg = msgSticker["STKPKGID"]
                        sver = msgSticker["STKVER"]
                        sendSticker(op.param2, sver, spkg, sid)
                msgSticker = settings["messageSticker"]["listSticker"]["welcomeSticker"]
                if msgSticker != None:
                    sid = msgSticker["STKID"]
                    spkg = msgSticker["STKPKGID"]
                    sver = msgSticker["STKVER"]
                    sendSticker(op.param1, sver, spkg, sid)
        if op.type == 15:
            print ("[ 15 ]  NOTIFIED LEAVE GROUP")
            if settings["Lv"] == True:
                if "{gname}" in settings['leave']:
                    gName = line.getGroup(op.param1).name
                    msg = settings['leave'].replace("{gname}", gName)
                    msgSticker = settings["messageSticker"]["listSticker"]["leaveSticker"]
                    if msgSticker != None:
                        sid = msgSticker["STKID"]
                        spkg = msgSticker["STKPKGID"]
                        sver = msgSticker["STKVER"]
                        sendSticker(op.param2, sver, spkg, sid)
                msgSticker = settings["messageSticker"]["listSticker"]["leaveSticker"]
                if msgSticker != None:
                    sid = msgSticker["STKID"]
                    spkg = msgSticker["STKPKGID"]
                    sver = msgSticker["STKVER"]
                    sendSticker(op.param1, sver, spkg, sid)
        if op.type == 19:
            if settings["Nk"] == True:
                if "{gname}" in settings['kick']:
                    gName = line.getGroup(op.param1).name
                    msg = settings['kick'].replace("{gname}", gName)
                    msgSticker = settings["messageSticker"]["listSticker"]["kickSticker"]
                    if msgSticker != None:
                        sid = msgSticker["STKID"]
                        spkg = msgSticker["STKPKGID"]
                        sver = msgSticker["STKVER"]
                        sendSticker(op.param2, sver, spkg, sid)
                msgSticker = settings["messageSticker"]["listSticker"]["kickSticker"]
                if msgSticker != None:
                    sid = msgSticker["STKID"]
                    spkg = msgSticker["STKPKGID"]
                    sver = msgSticker["STKVER"]
                    sendSticker(op.param1, sver, spkg, sid)
        if op.type == 17:
            print ("[ 17 ]  NOTIFIED ACCEPT GROUP INVITATION")
            if settings["Wc"] == True:
                if "{gname}" in settings['welcome']:
                    gName = line.getGroup(op.param1).name
                    msg = settings['welcome'].replace("{gname}", gName)
                    msgSticker = settings["messageSticker"]["listSticker"]["welcomeSticker"]
                    if msgSticker != None:
                        sid = msgSticker["STKID"]
                        spkg = msgSticker["STKPKGID"]
                        sver = msgSticker["STKVER"]
                        sendSticker(op.param2, sver, spkg, sid)
                msgSticker = settings["messageSticker"]["listSticker"]["welcomeSticker"]
                if msgSticker != None:
                    sid = msgSticker["STKID"]
                    spkg = msgSticker["STKPKGID"]
                    sver = msgSticker["STKVER"]
                    sendSticker(op.param1, sver, spkg, sid)
                    
        if op.type == 26:
            msg = op.message
            if msg._from not in RfuFamily:
              if settings["talkban"] == True:
                if msg._from in settings["Talkblacklist"]:
                   try:
                       line.kickoutFromGroup(msg.to, [msg._from])
                   except:
                       try:
                           line.kickoutFromGroup(msg.to, [msg._from])
                       except:
                           line.kickoutFromGroup(msg.to, [msg._from])

        if op.type == 25:
            msg = op.message
            if msg.contentType == 13:
            	if settings["winvite"] == True:
                     if msg._from in admin:
                         _name = msg.contentMetadata["displayName"]
                         invite = msg.contentMetadata["mid"]
                         groups = line.getGroup(msg.to)
                         pending = groups.invitee
                         targets = []
                         for s in groups.members:
                             if _name in s.displayName:
                                 line.sendText(msg.to,"-> " + _name + " \nทำการเชิญสำเร็จ")
                                 break
                             elif invite in settings["blacklist"]:
                                 line.sendText(msg.to, "ขออภัย, " + _name + " บุคคนนี้อยู่ในรายการบัญชีดำ")
                                 line.sendText(msg.to,"ใช้คำสั่ง!, \n➡ล้างดำ➡ดึง" )
                                 break                             
                             else:
                                 targets.append(invite)
                         if targets == []:
                             pass
                         else:
                             for target in targets:
                                 try:
                                     line.findAndAddContactsByMid(target)
                                     line.inviteIntoGroup(msg.to,[target])
                                     line.sendText(msg.to,"เชิญคนนี้สำเร็จแล้ว : \n➡" + _name)
                                     settings["winvite"] = False
                                     break
                                 except:
                                     try:
                                         line.findAndAddContactsByMid(invite)
                                         line.inviteIntoGroup(op.param1,[invite])
                                         settings["winvite"] = False
                                     except:
                                         line.sendText(msg.to,"😧ตรวจพบข้อผิดพลาดที่ไม่ทราบสาเหตุ😩อาจเป็นได้ว่าบัญชีของคุณถูกแบนเชิญ😨")
                                         settings["winvite"] = False
                                         break

        if op.type == 25:
            msg = op.message
            if msg.contentType == 13:
               if settings["wblack"] == True:
                    if msg.contentMetadata["mid"] in settings["commentBlack"]:
                        line.sendReplyMessage(msg.id,to,"รับทราบ")
                        settings["wblack"] = False
                    else:
                        settings["commentBlack"][msg.contentMetadata["mid"]] = True
                        settings["wblack"] = False
                        line.sendReplyMessage(msg.id,to,"decided not to comment")

               elif settings["dblack"] == True:
                   if msg.contentMetadata["mid"] in settings["commentBlack"]:
                        del settings["commentBlack"][msg.contentMetadata["mid"]]
                        line.sendReplyMessage(msg.id,to,"ลบจากรายการที่ถูกแบนแล้ว")
                        settings["dblack"] = False

                   else:
                        settings["dblack"] = False
                        line.sendReplyMessage(msg.id,to,"Tidak Ada Dalam Daftar Blacklist")
               elif settings["wblacklist"] == True:
                 if msg._from in admin: 
                   if msg.contentMetadata["mid"] in settings["blacklist"]:
                        line.sendReplyMessage(msg.id,to,"Sudah Ada")
                        settings["wblacklist"] = False
                   else:
                        settings["blacklist"][msg.contentMetadata["mid"]] = True
                        settings["wblacklist"] = False
                        line.sendReplyMessage(msg.id,to,"เพิ่มบัญชีนี้ในรายการสีดำเรียบร้อยแล้ว")

               elif settings["dblacklist"] == True:
                 if msg._from in admin: 
                   if msg.contentMetadata["mid"] in settings["blacklist"]:
                        del settings["blacklist"][msg.contentMetadata["mid"]]
                        line.sendReplyMessage(msg.id,to,"เพิ่มบัญชีนี้ในรายการสีขาวเรียบร้อยแล้ว")
                        settings["dblacklist"] = False

                   else:
                        settings["dblacklist"] = False
                        line.sendReplyMessage(msg.id,to,"Tidak Ada Dalam Da ftar Blacklist")
#============================================================================================#
        if op.type == 26:
            msg = op.message
            text = msg.text
            msg_id = msg.id
            receiver = msg.to
            sender = msg._from
            if msg.toType == 0 or msg.toType == 2:
                if msg.toType == 0:
                    to = sender
                elif msg.toType == 2:
                    to = receiver
                if settings["autoRead"] == True:
                    line.sendChatChecked(to, msg_id)
                if msg.contentType == 0:
                    if settings["unsendMessage"] == True:
                        try:
                            if msg.location != None:
                                unsendmsg = time.time()
                                msg_dict[msg.id] = {"lokasi":msg.location,"from":msg._from,"waktu":unsendmsg}
                            else:
                                unsendmsg = time.time()
                                msg_dict[msg.id] = {"text":msg.text,"from":msg._from,"waktu":unsendmsg}
                        except Exception as e:
                            print (e)
                if msg.contentType == 1:
                    if settings["unsendMessage"] == True:
                        try:
                            unsendmsg1 = time.time()
                            path = line.downloadObjectMsg(msg_id)
                            msg_dict[msg.id] = {"from":msg._from,"image":path,"waktu":unsendmsg1}
                        except Exception as e:
                            print (e)
                if msg.contentType == 2:
                    if settings["unsendMessage"] == True:
                        try:
                            unsendmsg2 = time.time()
                            path = line.downloadObjectMsg(msg_id)
                            msg_dict[msg.id] = {"from":msg._from,"video":path,"waktu":unsendmsg2}
                        except Exception as e:
                            print (e)
                if msg.contentType == 3:
                    if settings["unsendMessage"] == True:
                        try:
                            unsendmsg3 = time.time()
                            path = line.downloadObjectMsg(msg_id)
                            msg_dict[msg.id] = {"from":msg._from,"audio":path,"waktu":unsendmsg3}
                        except Exception as e:
                            print (e)
                if msg.contentType == 7:
                    if settings["unsendMessage"] == True:
                        try:
                            unsendmsg7 = time.time()
                            sticker = msg.contentMetadata["STKID"]
                            link = "http://dl.stickershop.line.naver.jp/stickershop/v1/sticker/{}/android/sticker.png".format(sticker)
                            msg_dict[msg.id] = {"from":msg._from,"sticker":link,"waktu":unsendmsg7}
                        except Exception as e:
                            print (e)
                if msg.contentType == 13:
                    if settings["unsendMessage"] == True:
                        try:
                            unsendmsg13 = time.time()
                            mid = msg.contentMetadata["mid"]
                            msg_dict[msg.id] = {"from":msg._from,"mid":mid,"waktu":unsendmsg13}
                        except Exception as e:
                            print (e)
                if msg.contentType == 14:
                    if settings["unsendMessage"] == True:
                        try:
                            unsendmsg14 = time.time()
                            path = line.downloadObjectMsg(msg_id)
                            msg_dict[msg.id] = {"from":msg._from,"file":path,"waktu":unsendmsg14}
                        except Exception as e:
                            print (e)       
#==============================================================================#
        if op.type == 25:
            msg = op.message
            text = msg.text
            msg_id = msg.id
            receiver = msg.to
            sender = msg._from
            if msg.toType == 0 or msg.toType == 1 or msg.toType == 2:
                if msg.toType == 0:
                    if sender != line.profile.mid:
                        to = sender
                    else:
                        to = receiver
                elif msg.toType == 1:
                    to = receiver
                elif msg.toType == 2:
                    to = receiver
            if msg.contentType == 0:
                if text is None:
                    return
#==============================================================================#
                if ".พูด " in msg.text.lower():
                    spl = re.split(".พูด ",msg.text,flags=re.IGNORECASE)
                    if spl[0] == "":
                        mts = spl[1]
                        mtsl = mts.split()
                        mtsTimeArg = len(mtsl) - 1
                        mtsTime = mtsl[mtsTimeArg]
                        del mtsl[mtsTimeArg]
                        mtosay = " ".join(mtsl)
                        global Rapid1To
                        Rapid1To = msg.to
                        RapidTime = mtsTime
                        rmtosay = []
                        for count in range(0,int(RapidTime)):
                            rmtosay.insert(count,mtosay)
                        p = Pool(20)
                        p.map(Rapid1Say,rmtosay)
                        p.close()
                elif "รับแขก" in msg.text:
                    gs = line.getGroup(msg.to)
                    say = msg.text.replace("รับแขก","สวัสดีครับ ยินดีต้อนรับนะ มาใหม่แก้ผ้าด้วย อะคริ อะคริ "+ gs.name)
                    lang = 'th'
                    tts = gTTS(text=say, lang=lang)
                    tts.save("hasil.mp3")
                    line.sendAudio(msg.to,"hasil.mp3")
                if text.lower() == 'รัก':
                    myHelp = myhelp()
                    line.sendReplyMessage(msg.id,to, str(myHelp))
                    #line.sendMentionFooter(to, '「ผู้สร้างบอท」\n', sender, "https://line.me/ti/p/~e24jih", "http://dl.profile.line-cdn.net/"+line.getContact(sender).pictureStatus, line.getContact(sender).displayName);line.sendMessage(to, line.getContact(sender).displayName, contentMetadata = {'previewUrl': 'http://dl.profile.line-cdn.net/'+line.getContact(sender).pictureStatus, 'i-installUrl': 'https://line.me/ti/p/~e24jih', 'type': 'mt', 'subText': "TEAMBOT MUSIC", 'a-installUrl': 'https://line.me/ti/p/~e24jih', 'a-installUrl': ' https://line.me/ti/p/~e24jih', 'a-packageName': 'com.spotify.music', 'countryCode': 'ID', 'a-linkUri': 'https://line.me/ti/p/~e24jih', 'i-linkUri': 'https://line.me/ti/p/~e24jih', 'id': 'mt000000000a6b79f9', 'text': 'Khie', 'linkUri': 'https://line.me/ti/p/~e24jih'}, contentType=19)
                elif text.lower() == 'รัก1':
                    helpSet = helpset()
                    line.sendReplyMessage(msg.id,to, str(helpSet))
                    sendMessageWithMention(to, lineMID)
                elif text.lower() == 'รัก2':
                    listGrup = listgrup()
                    line.sendReplyMessage(msg.id,to, str(listGrup))
                elif text.lower() == 'รัก3':
                    helpSetting = helpsetting()
                    line.sendReplyMessage(msg.id,to, str(helpSetting ))
                elif text.lower() == 'รัก4':
                    socMedia = socmedia()
                    line.sendReplyMessage(msg.id,to, str(socMedia))

#==============================================================================#
                elif text.lower() == "self on":
                   if msg._from in admin:
                    settings["selfbot"] = True
                    line.sendReplyMessage(msg.id,to, "เริ่มระบบที่2")
                elif text.lower() == "self off":
                   if msg._from in admin:
                    settings["selfbot"] = False
                    line.sendReplyMessage(msg.id,to, "หยุดการทำงานของระบบที่2แล้ว")
                elif text.lower() == 'วัดใจ':
                    start = time.time()
                    line.sendReplyMessage(msg.id,to, "กำลังทดสอบความเร็วแสง")
                    elapsed_time = time.time() - start
                    line.sendReplyMessage(msg.id,to, "[ %s Seconds ] [ " % (elapsed_time) + str(int(round((time.time() - start) * 1000)))+" ms ]")
                elif text.lower() == 'sp':
                    start = time.time()
                    line.sendReplyMessage(msg.id,to, "กำลังทดสอบความเร็วแสง")
                    elapsed_time = time.time() - start
                    line.sendReplyMessage(msg.id,to, "[ %s Seconds ] [ " % (elapsed_time) + str(int(round((time.time() - start) * 1000)))+" ms ]")
                elif text.lower() == 'เริ่มใหม่':
                    line.sendReplyMessage(msg.id,to, "กำลังเริ่มต้นใหม่ ... โปรดรอสักครู่ ..")
                    line.sendReplyMessage(msg.id,to, "🔝тєαмвσтмυѕι¢🔝")
                    restartBot()
                elif text.lower() == 'ออน':
                    timeNow = time.time()
                    runtime = timeNow - botStart
                    runtime = format_timespan(runtime)
                    line.sendReplyMessage(msg.id,to, "ระยะเวลาการทำงานของบอท {}".format(str(runtime)))
                elif text.lower() == 'ข้อมูล':
                    try:
                        arr = []
                        owner = lineMID
                        creator = line.getContact(owner)
                        contact = line.getContact(lineMID)
                        grouplist = line.getGroupIdsJoined()
                        contactlist = line.getAllContactIds()
                        blockedlist = line.getBlockedContactIds()
                        ret_ = "╔══[ ™❍✯͜͡тєαмвσтмυѕι¢✯͜͡❂➣]"
                        ret_ += "\n╠۝ ชื่อ ═ {}".format(contact.displayName)
                        ret_ += "\n╠۝ กลุ่ม ═ {}".format(str(len(grouplist)))
                        ret_ += "\n╠۝ เพื่อน ═ {}".format(str(len(contactlist)))
                        ret_ += "\n╠۝ บล็อค ═ {}".format(str(len(blockedlist)))
                        ret_ += "\n╠══[สถานะ] ═ {}".format(contact.statusMessage)
                        ret_ += "\n╠۝ ผู้สร้าง ═ {}".format(creator.displayName)
                        ret_ += "\n╚══[ ™❍✯͜͡тєαмвσтмυѕι¢✯͜͡❂➣]"
                        line.sendContact(to, owner)
                        line.sendMessage(to, str(ret_))
                        #line.sendMentionFooter(to, '「ผู้สร้างบอท」\n', sender, "https://line.me/ti/p/~e24jih", "http://dl.profile.line-cdn.net/"+line.getContact(sender).pictureStatus, line.getContact(sender).displayName);line.sendMessage(to, line.getContact(sender).displayName, contentMetadata = {'previewUrl': 'http://dl.profile.line-cdn.net/'+line.getContact(sender).pictureStatus, 'i-installUrl': 'https://line.me/ti/p/~e24jih', 'type': 'mt', 'subText': "мυѕїċѕρèèđвσ†", 'a-installUrl': 'https://line.me/ti/p/~e24jih', 'a-installUrl': ' https://line.me/ti/p/~e24jih', 'a-packageName': 'com.spotify.music', 'countryCode': 'ID', 'a-linkUri': 'https://line.me/ti/p/~e24jih', 'i-linkUri': 'https://line.me/ti/p/~e24jih', 'id': 'mt000000000a6b79f9', 'text': 'Khie', 'linkUri': 'https://line.me/ti/p/~e24jih'}, contentType=19)
                    except Exception as e:
                        line.sendMessage(msg.to, str(e))
#==============================================================================#
                elif text.lower() == 'เช็ค':
                    try:
                        ret_ = "╔════[ Status ]═════┓"
                        if settings["autoAdd"] == True: ret_ += "\n╠ ออโต้แอด✔"
                        else: ret_ += "\n╠ ออโต้แอด   ✘ "
                        if settings["autoBlock"] == True: ret_ += "\n╠ ออโต้บล็อค✔"
                        else: ret_ += "\n╠ ออโต้บล็อค   ✘ "
                        if settings["autoJoinTicket"] == True: ret_ += "\n╠ มุดลิ้ง✔"
                        else: ret_ += "\n╠ มุดลิ้ง   ✘ "
                        if settings["autoJoin"] == True: ret_ += "\n╠ เข้าห้องออโต้ ✔"
                        else: ret_ += "\n╠ เข้าห้องออโต้    ✘ "
                        if settings["Api"] == True: ret_ += "\n╠ บอท api✔"
                        else: ret_ += "\n╠ บอท api   ✘ "
                        if settings["Aip"] == True: ret_ += "\n╠ แสกนคำพูด+คำสั่งบิน✔"
                        else: ret_ += "\n╠ แสกนคำพูด+คำสั่งบิน   ✘ "
                        if settings["Wc"] == True: ret_ += "\n╠ ข้อความต้อนรับสมาชิก ✔"
                        else: ret_ += "\n╠ ข้อความต้อนรับสมาชิก    ✘ "
                        if settings["Lv"] == True: ret_ += "\n╠ ข้อความอำลาสมาชิก ✔"
                        else: ret_ += "\n╠ ข้อความอำลาสมาชิก    ✘ "
                        if settings["Nk"] == True: ret_ += "\n╠ ข้อความแจ้งเตือนคนลบ ✔"
                        else: ret_ += "\n╠ ข้อความแจ้งเตือนคนลบ    ✘ "
                        if settings["autoCancel"]["on"] == True:ret_+="\n╠ ปฏิเสธกลุ่มเชิญที่มีสมาชิกต่ำกว่า: " + str(settings["autoCancel"]["members"]) + " → ✔"
                        else: ret_ += "\n╠ ปฏิเสธกลุ่มเชิญ    ✘ "						
                        if settings["autoLeave"] == True: ret_ += "\n╠ ออกแชทรวม ✔"
                        else: ret_ += "\n╠ ออกแชทรวม ✘ "
                        if settings["autoRead"] == True: ret_ += "\n╠ อ่านออโต้ ✔"
                        else: ret_ += "\n╠ อ่านออโต้   ✘ "				
                        if settings["checkContact"] == True: ret_ += "\n╠ อ่านคท ✔"
                        else: ret_ += "\n╠ อ่านคท        ✘ "
                        if settings["checkPost"] == True: ret_ += "\n╠ เช็คโพส ✔"
                        else: ret_ += "\n╠ เช็คโพส        ✘ "
                        if settings["checkSticker"] == True: ret_ += "\n╠ Sticker ✔"
                        else: ret_ += "\n╠ Sticker        ✘ "
                        if settings["pmMessage"] == True: ret_ += "\n╠ ตอบแทคแชท ✔"
                        else: ret_ += "\n╠ ตอบแทคในแชท       ✘ "
                        if settings["detectMention"] == True: ret_ += "\n╠ ตอบกลับคนแทค ✔"
                        else: ret_ += "\n╠ ตอบกลับคนแทค ✘ "
                        if settings["potoMention"] == True: ret_ += "\n╠ แสดงภาพคนแทค ✔"
                        else: ret_ += "\n╠ แสดงภาพคนแทค ✘ "
                        if settings["kickMention"] == True: ret_ += "\n╠ เตะคนแทค ✔"
                        else: ret_ += "\n╠ เตะคนแทค ✘ "
                        if settings["kickContact"] == True: ret_ += "\n╠ สั่งเตะด้วย คท ✔"
                        else: ret_ += "\n╠ สั่งเตะด้วย คท ✘ "
                        if settings["delayMention"] == True: ret_ += "\n╠ แทคกลับคนแทค ✔"
                        else: ret_ += "\n╠ แทคกลับคนแทค ✘ "
                        if RfuProtect["inviteprotect"] == True: ret_ += "\n╠ กันเชิญ ✔"
                        else: ret_ += "\n╠ กันเชิญ ✘ "
                        if RfuProtect["cancelprotect"] == True: ret_ += "\n╠ กันยกเชิญ ✔"
                        else: ret_ += "\n╠ กันยกเชิญ ✘ "
                        if RfuProtect["protect"] == True: ret_ += "\n╠ ป้องกัน ✔"
                        else: ret_ += "\n╠ ป้องกัน ✘ "
                        if RfuProtect["linkprotect"] == True: ret_ += "\n╠ ป้องกันเปิดลิ้ง ✔"
                        else: ret_ += "\n╠ ป้องกันเปิดลิ้ง ✘ "
                        if RfuProtect["Protectguest"] == True: ret_ += "\n╠ ป้องกันสมาชิก ✔"
                        else: ret_ += "\n╠ ป้องกันสมาชิก ✘ "
                        if RfuProtect["Protectjoin"] == True: ret_ += "\n╠ ป้องกันเข้ากลุ่ม ✔"
                        else: ret_ += "\n╠ ป้องกันเข้ากลุ่ม ✘ "	
                        ret_ += "\n╔════[ тєαмвσтмυѕι¢ ]═════┓"
                        ret_ += "\n╠  ระบบป้องกันแบบแยกห้อง "
                        ret_ += "\n╚════[ тєαмвσтмυѕι¢ ]═════┛"
                        if msg.to in protectinvite: ret_ += "\n┃┍◈➣✔ กันเชิญ [ ᴏɴ ]"
                        else: ret_ += "\n┃┍◈➣✖ กันเชิญ[ ᴏғғ ]"
                        if msg.to in protectcancel: ret_ += "\n┃┝◈➣✔ กันยกเชิญ [ ᴏɴ ]"
                        else: ret_ += "\n┃┝◈➣✖ กันยกเชิญ[ ᴏғғ ]"
                        if msg.to in protectjoin: ret_ += "\n┃┝◈➣✔ ป้องกันเข้าห้อง [ ᴏɴ ]"
                        else: ret_ += "\n┃┝◈➣✖ ป้องกันเข้าห้อง[ ᴏғғ ]"
                        if msg.to in protectqr: ret_ += "\n┃┝◈➣✔ ป้องกันเปิดลิ้ง [ ᴏɴ ]"
                        else: ret_ += "\n┃┝◈➣✖ ป้องกันเปิดลิ้ง[ ᴏғғ ]"
                        if msg.to in protectkick: ret_ += "\n┃┝◈➣✔ ป้องกันสมาชิก [ ᴏɴ ]"
                        else: ret_ += "\n┃┕◈➣✖ ป้องกันสมาชิก[ ᴏғғ ]"
                        ret_ += "\n╚════[ Status ]═════┛"
                        line.sendReplyMessage(msg.id,to, str(ret_))
                        line.sendMentionFooter(to, '「ผู้สร้างบอท」\n', sender, "https://line.me/ti/p/~samuri5", "http://dl.profile.line-cdn.net/"+line.getContact(sender).pictureStatus, line.getContact(sender).displayName);line.sendMessage(to, line.getContact(sender).displayName, contentMetadata = {'previewUrl': 'http://dl.profile.line-cdn.net/'+line.getContact(sender).pictureStatus, 'i-installUrl': 'https://line.me/ti/p/~samuri5', 'type': 'mt', 'subText': "SAMURAI BOT", 'a-installUrl': 'https://line.me/ti/p/~samuri5', 'a-installUrl': ' https://line.me/ti/p/~samuri5', 'a-packageName': 'com.spotify.music', 'countryCode': 'ID', 'a-linkUri': 'https://line.me/ti/p/~samuri5', 'i-linkUri': 'https://line.me/ti/p/~samuri5', 'id': 'mt000000000a6b79f9', 'text': 'Khie', 'linkUri': 'https://line.me/ti/p/~samuri5'}, contentType=19)
                    except Exception as e:
                        line.sendReplyMessage(msg.id,to, str(e))
                elif msg.text.lower().startswith(".ลบข้อความ "):
                    args = msg.text.replace(".ลบข้อความ ","")
                    mes = 0
                    try:
                        mes = int(args[1])
                    except:
                        mes = 1
                    M = line.getRecentMessagesV2(to, 101)
                    MId = []
                    for ind,i in enumerate(M):
                        if ind == 0:
                            pass
                        else:
                            if i._from == line.profile.mid:
                                MId.append(i.id)
                                if len(MId) == mes:
                                    break
                    def unsMes(msg_id):
                      line.unsendMessage(msg_id)
                    for i in MId:
                      thread1 = threading.Thread(target=unsMes, args=(i,))
                      thread1.start()
                      thread1.join()
                    line.sendMessage(msg.to, ' 「 ยกเลิกข้อความโดย тєαмвσтмυѕι¢ 」\nยกเลิกแล้ว {} คำ'.format(len(MId)))
                elif text.lower() == 'เปิดแอด':
                    settings["autoAdd"] = True
                    settings["autoBlock"] = False
                    line.sendReplyMessage(msg.id,to, "เปิดระบบแอดเพื่อนอัตโนมัติ")
                elif text.lower() == 'เปิดบล็อค':
                    settings["autoBlock"] = True
                    settings["autoAdd"] = False
                    line.sendReplyMessage(msg.id,to, "เปิดระบบออโต้บล็อค เรียบร้อย")
                elif text.lower() == 'ปิดแอด':
                    settings["autoAdd"] = False
                    line.sendReplyMessage(msg.id,to, "ปิดระบบแอดเพื่อนอัตโนมัติ")
                elif text.lower() == 'ปิดบล็อค':
                    settings["autoBlock"] = False
                    line.sendReplyMessage(msg.id,to, "ปิดระบบออโต้บล็อค เรียบร้อย")
                elif text.lower() == 'เปิดเข้า':
                    settings["autoJoin"] = True
                    line.sendReplyMessage(msg.id,to, "เปิดระบบเข้ากลุ่มอัตโนมัติ")
                elif text.lower() == 'ปิดเข้า':
                    settings["autoJoin"] = False
                    line.sendReplyMessage(msg.id,to, "ปิดระบบเข้ากลุ่มอัตโนมัติ")
                elif text.lower() == 'เปิดเผือก':
                    settings["unsendMessage"] = True
                    line.sendReplyMessage(msg.id,to, "เปิดระบบยกเลิกข้อความ")
                elif text.lower() == 'ปิดเผือก':
                    settings["unsendMessage"] = False
                    line.sendReplyMessage(msg.id,to, "ปิดระบบยกเลิกข้อความ")
                elif "โทเคน" in msg.text.lower():
                   if msg._from in admin:
                       line.sendMessage(msg.to,line.authToken)
                elif "แปลงข้อความ: " in msg.text:
                        txt = msg.text.replace("แปลงข้อความ: ", "")
                        line.kedapkedip(msg.to,txt)
                        print ("[Command] Kedapkedip")
                elif "Say " in msg.text:
                        say = msg.text.replace("Say ","")
                        lang = 'th'
                        tts = gTTS(text=say, lang=lang)
                        tts.save("hasil.mp3")
                        line.sendAudio(msg.to,"hasil.mp3")
                elif "Image " in msg.text:
                        search = msg.text.replace("Image ","")
                        url = 'https://www.google.com/search?espv=2&biw=1366&bih=667&tbm=isch&oq=kuc&aqs=mobile-gws-lite.0.0l5&q=' + search
                        raw_html = (download_page(url))
                        items = []
                        items = items + (_images_get_all_items(raw_html))
                        path = random.choice(items)
                        print (path)
                        try:
                            line.sendImageWithURL(msg.to,path)
                        except:
                            pass
                elif "gcancel:" in msg.text:
                    try:
                        strnum = msg.text.replace("gcancel:","")
                        if strnum == "off":
                                settings["autoCancel"]["on"] = False
                                if settings["lang"] == "JP":
                                    line.sendText(msg.to,"ปิดใช้งานระบบปฏิเสธคำเชิญตามจำนวนสมาชิก")
                                else:
                                    line.sendText(msg.to,"关了邀请拒绝。要时开请指定人数发送")
                        else:
                                num =  int(strnum)
                                settings["autoCancel"]["on"] = True
                                if settings["lang"] == "JP":
                                    line.sendText(msg.to, " สมาชิกในกลุ่มที่ไม่ถึง" + strnum + "จะถูกปฏิเสธคำเชิญโดยอัตโนมัติ")
                                else:
                                    line.sendText(msg.to,strnum + "使人以下的小组用自动邀请拒绝")
                    except:
                        if settings["lang"] == "JP":
                                line.sendText(msg.to,str(settings["eror"]))
                        else:
                                line.sendText(msg.to,"Bizarre ratings")					
                elif text.lower() == 'เปิดออก':
                    settings["autoLeave"] = True
                    line.sendReplyMessage(msg.id,to, "เปิดระบบออกแชทรวมอัตโนมัติ")
                elif text.lower() == 'ปิดออก':
                    settings["autoLeave"] = False
                    line.sendReplyMessage(msg.id,to, "ปิดระบบออกแชทรวมอัตโนมัติ")
                elif text.lower() == 'เปิดอ่าน':
                    settings["autoRead"] = True
                    line.sendReplyMessage(msg.id,to, "เปิดอ่านข้อความอัตโนมัติ")
                elif text.lower() == 'ปิดอ่าน':
                    settings["autoRead"] = False
                    line.sendReplyMessage(msg.id,to, "ปิดอ่านข้อความอัตโนมัติ")
                elif text.lower() == 'เปิดติ๊ก':
                    settings["checkSticker"] = True
                    line.sendReplyMessage(msg.id,to, "Check sticker enabled.")
                elif text.lower() == 'ปิดติ๊ก':
                    settings["checkSticker"] = False
                    line.sendReplyMessage(msg.id,to, "Check sticker disabled.")
                elif text.lower() == 'เปิดมุด':
                    settings["autoJoinTicket"] = True
                    line.sendReplyMessage(msg.id,to, "เปิดระบบมุดลิ้งอัตโนมัติ")
                elif text.lower() == 'ปิดมุด':
                    settings["autoJoinTicket"] = False
                    line.sendReplyMessage(msg.id,to, "ปิดระบบมุดลิ้งอัตโนมัติ")
                elif text.lower() == 'เปิดหวดคท':
                    settings["kickContact"] = True
                    line.sendReplyMessage(msg.id,to, "KickContact ✔")
                elif text.lower() == 'ปิดหวดคท':
                    settings["kickContact"] = False
                    line.sendReplyMessage(msg.id,to, "KickContact ✘")
#==============================================================================#
                elif msg.text.lower() == "me":
                    me = line.getContact(lineMID)
                    line.sendReplyMessage(msg.id,to,"[👇ชื่อของพี่👇]")
                    sendMessageWithMention(to, lineMID)
                    line.sendMessage(msg.to,"[สเตตัส]\n" + me.statusMessage)
                    line.sendReplyMessage(msg.id,to,text=None, contentMetadata={'mid': lineMID}, contentType=13)
                    line.sendImageWithURL(msg.to,"http://dl.profile.line-cdn.net/" + me.pictureStatus)
                    cover = line.getProfileCoverURL(lineMID)
                    line.sendImageWithURL(msg.to, cover)
                    line.sendReplyMessage(msg.id,to,str(settings["comment"]))
                elif text.lower() == "ที่รัก":
                    line.sendMentionFooter(to, '「ผู้สร้างบอท」\n', sender, "https://line.me/ti/p/~samuri5", "http://dl.profile.line-cdn.net/"+line.getContact(sender).pictureStatus, line.getContact(sender).displayName);line.sendMessage(to, line.getContact(sender).displayName, contentMetadata = {'previewUrl': 'http://dl.profile.line-cdn.net/'+line.getContact(sender).pictureStatus, 'i-installUrl': 'https://line.me/ti/p/~samuri5', 'type': 'mt', 'subText': "SAMURAI BOT", 'a-installUrl': 'https://line.me/ti/p/~samuri5', 'a-installUrl': ' https://line.me/ti/p/~samuri5', 'a-packageName': 'com.spotify.music', 'countryCode': 'ID', 'a-linkUri': 'https://line.me/ti/p/~samuri5', 'i-linkUri': 'https://line.me/ti/p/~samuri5', 'id': 'mt000000000a6b79f9', 'text': 'Khie', 'linkUri': 'https://line.me/ti/p/~samuri5'}, contentType=19)
                elif text.lower() == 'คท':
                    line.sendReplyMessage(msg.id,to,text=None, contentMetadata={'mid': lineMID}, contentType=13)
                elif text.lower() == 'โย่ว':
                    sendMessageWithMention(to, lineMID)
                    line.sendContact(to, lineMID)
                    line.sendReplyMessage(msg.id,to,text=None, contentMetadata={'mid': lineMID}, contentType=13)
                elif text.lower() == 'ผส':
                    sendMessageWithMention(to, lineMID)
                    line.sendContact(to, "u5e5d16d5c9d8ade1d3957a84481c44ac")
                elif text.lower() == 'ไอดี':
                    line.sendReplyMessage(msg.id,to,lineMID)
                elif text.lower() == 'ชื่อ':
                    me = line.getContact(lineMID)
                    line.sendReplyMessage(msg.id,to,"[ชือของคุณในขณะนี้คือ]\n" + me.displayName)
                elif text.lower() == 'ตัส':
                    me = line.getContact(lineMID)
                    line.sendReplyMessage(msg.id,to,"[StatusMessage]\n" + me.statusMessage)
                elif text.lower() == 'รูปโปร':
                    me = line.getContact(lineMID)
                    line.sendImageWithURL(msg.to,"http://dl.profile.line-cdn.net/" + me.pictureStatus)
                elif text.lower() == 'วีดีโอโปร':
                    me = line.getContact(lineMID)
                    line.sendVideoWithURL(msg.to,"http://dl.profile.line-cdn.net/" + me.pictureStatus + "/vp")
                elif text.lower() == 'รูปปก':
                    me = line.getContact(lineMID)
                    cover = line.getProfileCoverURL(lineMID)    
                    line.sendImageWithURL(msg.to, cover)
                elif text.lower() == 'คอมเม้น':
                    line.sendReplyMessage(msg.id,to, None, contentMetadata={"PRDID":"a0768339-c2d3-4189-9653-2909e9bb6f58","PRDTYPE":"THEME","MSGTPL":"6"}, contentType=9)
                    line.sendReplyMessage(msg.id,to,str(settings["comment"]))
                    line.sendReplyMessage(msg.id,to, None, contentMetadata={"STKID":"52114123","STKPKGID":"11539","STKVER":"1"}, contentType=7)
                elif text.lower() == 'ทักเข้า':
                    line.sendReplyMessage(msg.id,to, str(settings["welcome"]))
                elif text.lower() == 'ทักออก':
                    line.sendReplyMessage(msg.id,to, str(settings["bye"]))
                elif text.lower() == 'ทักเตะ':
                    line.sendReplyMessage(msg.id,to, str(settings["kick"]))
                elif text.lower() == 'ข้อความแอด':
                    line.sendReplyMessage(msg.id,to, str(settings["message"]))
                elif text.lower() == 'ข้อความแทค':
                    line.sendReplyMessage(msg.id,to, str(settings["Respontag"]))
                elif text.lower() == 'แทคล่อง':
                    gs = line.getGroup(to)
                    targets = []
                    for g in gs.members:
                        if g.displayName in "":
                            targets.append(g.mid)
                    if targets == []:
                        line.sendReplyMessage(msg.id,to, "ม่มีคนใส่ร่องหนในกลุ่มนี้😂")
                    else:
                        mc = ""
                        for target in targets:
                            mc += sendMessageWithMention(to,target) + "\n"
                        line.sendReplyMessage(msg.id,to, mc)
                elif text.lower() == 'ไอดีล่อง':
                    gs = line.getGroup(to)
                    lists = []
                    for g in gs.members:
                        if g.displayName in "":
                            lists.append(g.mid)
                    if lists == []:
                        line.sendReplyMessage(msg.id,to, "ไม่มีmidคนใส่ร่องหน🤗")
                    else:
                        mc = ""
                        for mi_d in lists:
                            mc += "->" + mi_d + "\n"
                        line.sendReplyMessage(msg.id,to,mc)
                elif text.lower() == 'คทล่อง':
                    gs = line.getGroup(to)
                    lists = []
                    for g in gs.members:
                        if g.displayName in "":
                            lists.append(g.mid)
                    if lists == []:
                        line.sendReplyMessage(msg.id,to, "ไม่มีคนใส่ร่องหนในกลุ่มนี้😂")
                    else:
                        for ls in lists:
                            contact = line.getContact(ls)
                            mi_d = contact.mid
                            line.sendReplyMessage(msg.id,to,text=None, contentMetadata={'mid': mi_d}, contentType=13)
                elif msg.text.lower().startswith("คท "):
                    if 'MENTION' in list(msg.contentMetadata.keys())!= None:
                        names = re.findall(r'@(\w+)', text)
                        mention = ast.literal_eval(msg.contentMetadata['MENTION'])
                        mentionees = mention['MENTIONEES']
                        lists = []
                        for mention in mentionees:
                            if mention["M"] not in lists:
                                lists.append(mention["M"])
                        for ls in lists:
                            contact = line.getContact(ls)
                            mi_d = contact.mid
                            line.sendReplyMessage(msg.id,to,text=None, contentMetadata={'mid': mi_d}, contentType=13)
                elif msg.text.lower().startswith("ไอดี "):
                    if 'MENTION' in list(msg.contentMetadata.keys())!= None:
                        names = re.findall(r'@(\w+)', text)
                        mention = ast.literal_eval(msg.contentMetadata['MENTION'])
                        mentionees = mention['MENTIONEES']
                        lists = []
                        for mention in mentionees:
                            if mention["M"] not in lists:
                                lists.append(mention["M"])
                        ret_ = "[ Mid User ]"
                        for ls in lists:
                            ret_ += "\n{}" + ls
                        line.sendReplyMessage(msg.id,to, str(ret_))
                elif msg.text.lower().startswith("ชื่อ "):
                    if 'MENTION' in list(msg.contentMetadata.keys())!= None:
                        names = re.findall(r'@(\w+)', text)
                        mention = ast.literal_eval(msg.contentMetadata['MENTION'])
                        mentionees = mention['MENTIONEES']
                        lists = []
                        for mention in mentionees:
                            if mention["M"] not in lists:
                                lists.append(mention["M"])
                        for ls in lists:
                            contact = line.getContact(ls)
                            line.sendReplyMessage(msg.id,to, "[ Display Name ]\n" + contact.displayName)
                elif msg.text.lower().startswith("ตัส "):
                    if 'MENTION' in list(msg.contentMetadata.keys())!= None:
                        names = re.findall(r'@(\w+)', text)
                        mention = ast.literal_eval(msg.contentMetadata['MENTION'])
                        mentionees = mention['MENTIONEES']
                        lists = []
                        for mention in mentionees:
                            if mention["M"] not in lists:
                                lists.append(mention["M"])
                        for ls in lists:
                            contact = line.getContact(ls)
                            line.sendReplyMessage(msg.id,to, "[ Status Message ]\n{}" + contact.statusMessage)
                elif msg.text.lower().startswith("รูปโปร "):
                    if 'MENTION' in list(msg.contentMetadata.keys())!= None:
                        names = re.findall(r'@(\w+)', text)
                        mention = ast.literal_eval(msg.contentMetadata['MENTION'])
                        mentionees = mention['MENTIONEES']
                        lists = []
                        for mention in mentionees:
                            if mention["M"] not in lists:
                                lists.append(mention["M"])
                        for ls in lists:
                            path = "http://dl.profile.line.naver.jp/" + line.getContact(ls).pictureStatus
                            line.sendImageWithURL(msg.to, str(path))
                elif msg.text.lower().startswith("วีดีโอโปร "):
                    if 'MENTION' in list(msg.contentMetadata.keys())!= None:
                        names = re.findall(r'@(\w+)', text)
                        mention = ast.literal_eval(msg.contentMetadata['MENTION'])
                        mentionees = mention['MENTIONEES']
                        lists = []
                        for mention in mentionees:
                            if mention["M"] not in lists:
                                lists.append(mention["M"])
                        for ls in lists:
                            path = "http://dl.profile.line.naver.jp/" + line.getContact(ls).pictureStatus + "/vp"
                            line.sendImageWithURL(msg.to, str(path))
                elif msg.text.lower().startswith("รูปปก "):
                    if line != None:
                        if 'MENTION' in list(msg.contentMetadata.keys())!= None:
                            names = re.findall(r'@(\w+)', text)
                            mention = ast.literal_eval(msg.contentMetadata['MENTION'])
                            mentionees = mention['MENTIONEES']
                            lists = []
                            for mention in mentionees:
                                if mention["M"] not in lists:
                                    lists.append(mention["M"])
                            for ls in lists:
                                path = line.getProfileCoverURL(ls)
                                line.sendImageWithURL(msg.to, str(path))
                elif msg.text.lower().startswith("เพิ่มเพื่อน "):
                    if 'MENTION' in msg.contentMetadata.keys()!= None:
                        names = re.findall(r'@(\w+)', text)
                        mention = ast.literal_eval(msg.contentMetadata['MENTION'])
                        mentionees = mention['MENTIONEES']
                        lists = []
                        for mention in mentionees:
                            if mention["M"] not in lists:
                                lists.append(mention["M"])
                        for ls in lists:
                            contact = line.getContact(ls)
                            line.findAndAddContactsByMid(ls)
                        line.sendReplyMessage(msg.id,to, "เพิ่มคุณ" + str(contact.displayName) + " เป็นเพื่อนแล้ว")
                elif msg.text.lower().startswith(".ลบเพื่อน "):
                    if 'MENTION' in msg.contentMetadata.keys()!= None:
                        names = re.findall(r'@(\w+)', text)
                        mention = ast.literal_eval(msg.contentMetadata['MENTION'])
                        mentionees = mention['MENTIONEES']
                        lists = []
                        for mention in mentionees:
                            if mention["M"] not in lists:
                                lists.append(mention["M"])
                        for ls in lists:
                            line.delContact(ls)
                        line.sendReplyMessage(msg.id,to, "ลบออกจากการเป็นเพื่อนแล้ว")
                elif text.lower()  == ".ล้างเพื่อน" or text.lower()  == " unfriendall":
                    try:
                        friend = line.getContacts(line.getAllContactIds())
                        line.sendMessage(to,"คุณได้ล้างเพื่อนทั่งหมด {} คน".format(len(friend)))
                        for unfriend in friend:
                            line.delContact(unfriend.mid)
                        line.sendMessage(to,"คุณได้ล้างเพื่อนทั่งหมด {} คน".format(len(friend)))
                    except Exception as error:
                        line.sendMessage(to, "「 Result Error 」\n" + str(error))
                elif "โทเคน" in msg.text.lower():
                   if msg._from in admin:
                       line.sendMessage(msg.to,"[ LINE ]\n"+line.authToken)
                elif msg.text in [".โหลด"]:
                    if msg._from in admin:
                        wek = line.getContact(lineMID)
                        a = wek.pictureStatus
                        r = wek.displayName
                        i = wek.statusMessage
                        s = open('mydn.txt',"w")
                        s.write(r)
                        s.close()
                        t = open('mysm.txt',"w")
                        t.write(i)
                        t.close()
                        u = open('myps.txt',"w")
                        u.write(a)
                        u.close()
                        line.sendText(msg.to, "backup has been active")
                        print (wek)
                        print (a)
                        print (r)
                        print (i)
                
                elif ".ก๊อป " in msg.text:
                    targets = []
                    key = eval(msg.contentMetadata["MENTION"])
                    key["MENTIONEES"][0]["M"]
                    for x in key["MENTIONEES"]:
                            targets.append(x["M"])
                    for target in targets:
                        try:
                            contact = line.getContact(target)
                            X = contact.displayName
                            profile = line.getProfile()
                            profile.displayName = X
                            line.updateProfile(profile)
                            line.sendReplyMessage(msg.id,to, "Success...")
                        #---------------------------------------
                            Y = contact.statusMessage
                            lol = line.getProfile()
                            lol.statusMessage = Y
                            line.updateProfile(lol)
                        #---------------------------------------
                            settings["changePictureProfile"] = True
                            me = line.getContact(target)     
                            line.sendImageWithURL(msg.to,"http://dl.profile.line-cdn.net/" + me.pictureStatus)
                        except Exception as e:
                            line.sendReplyMessage(msg.id,to, "เกิดข้อผิดพลาด")
                            print (e)

                elif ".คืนร่าง" in msg.text:
                    if msg._from in admin:
                            try:
                                h = open('mydn.txt',"r")
                                name = h.read()
                                h.close()
                                x = name
                                profile = line.getProfile()
                                profile.displayName = x
                                line.updateProfile(profile)
                                i = open('mysm.txt',"r")
                                sm = i.read()
                                i.close()
                                y = sm
                                cak = line.getProfile()
                                cak.statusMessage = y
                                line.updateProfile(cak)
                                line.sendReplyMessage(msg.id,to, "คืนได้แค่ชื่อกับตัสนะ😂😂")

                            except Exception as e:
                                line.sendReplyMessage(msg.id,to,"การคืนร่างล้มเหลว!")
                                print (e)
                elif ("ตั้งแอด " in msg.text):
                  if settings["selfbot"] == True:
                    if msg._from in admin:
                       key = eval(msg.contentMetadata["MENTION"])
                       key["MENTIONEES"][0]["M"]
                       targets = []
                       for x in key["MENTIONEES"]:
                            targets.append(x["M"])
                       for target in targets:
                               try:
                                   admin.append(target)
                                   line.sendMessage(msg.to,"Berhasil menambahkan admin")
                               except:
                                   pass

                elif ("ตั้งรอง " in msg.text):
                  if settings["selfbot"] == True:
                    if msg._from in admin:
                       key = eval(msg.contentMetadata["MENTION"])
                       key["MENTIONEES"][0]["M"]
                       targets = []
                       for x in key["MENTIONEES"]:
                            targets.append(x["M"])
                       for target in targets:
                               try:
                                   staff.append(target)
                                   line.sendMessage(msg.to,"Berhasil menambahkan staff")
                               except:
                                   pass

                elif ("ตั้งบอท " in msg.text):
                  if settings["selfbot"] == True:
                    if msg._from in admin:
                       key = eval(msg.contentMetadata["MENTION"])
                       key["MENTIONEES"][0]["M"]
                       targets = []
                       for x in key["MENTIONEES"]:
                            targets.append(x["M"])
                       for target in targets:
                               try:
                                   Bots.append(target)
                                   line.sendMessage(msg.to,"Berhasil menambahkan bot")
                               except:
                                   pass

                elif ("ลบแอด " in msg.text):
                    if msg._from in admin:
                       key = eval(msg.contentMetadata["MENTION"])
                       key["MENTIONEES"][0]["M"]
                       targets = []
                       for x in key["MENTIONEES"]:
                            targets.append(x["M"])
                       for target in targets:
                           if target not in Rfu:
                               try:
                                   admin.remove(target)
                                   line.sendMessage(msg.to,"Berhasil menghapus admin")
                               except:
                                   pass

                elif ("ลบรอง " in msg.text):
                    if msg._from in admin:
                       key = eval(msg.contentMetadata["MENTION"])
                       key["MENTIONEES"][0]["M"]
                       targets = []
                       for x in key["MENTIONEES"]:
                            targets.append(x["M"])
                       for target in targets:
                           if target not in Rfu:
                               try:
                                   staff.remove(target)
                                   line.sendMessage(msg.to,"Berhasil menghapus admin")
                               except:
                                   pass

                elif ("ลบบอท " in msg.text):
                    if msg._from in admin:
                       key = eval(msg.contentMetadata["MENTION"])
                       key["MENTIONEES"][0]["M"]
                       targets = []
                       for x in key["MENTIONEES"]:
                            targets.append(x["M"])
                       for target in targets:
                           if target not in Rfu:
                               try:
                                   Bots.remove(target)
                                   line.sendMessage(msg.to,"Berhasil menghapus admin")
                               except:
                                   pass

                elif text.lower() == "เพิ่มแอด" or text.lower() == 'admin:on':
                    if msg._from in admin:
                        settings["addadmin"] = True
                        line.sendText(msg.to,"กรุณาส่งคอนแทค...")

                elif text.lower() == "ลบแอด" or text.lower() == 'admin:repeat':
                    if msg._from in admin:
                        settings["delladmin"] = True
                        line.sendText(msg.to,"Kirim kontaknya...")

                elif text.lower() == "เพิ่มรอง" or text.lower() == 'staff:on':
                    if msg._from in admin:
                        settings["addstaff"] = True
                        line.sendText(msg.to,"กรุณาส่งคอนแทค...")

                elif text.lower() == "ลบรอง" or text.lower() == 'staff:repeat':
                    if msg._from in admin:
                        settings["dellstaff"] = True
                        line.sendText(msg.to,"กรุณาส่งคอนแทค...")

                elif text.lower() == "เพิ่มบอท" or text.lower() == 'bot:on':
                    if msg._from in admin:
                        settings["addbots"] = True
                        line.sendText(msg.to,"กรุณาส่งคอนแทค...")

                elif text.lower() == "ลบบอท" or text.lower() == 'bot:repeat':
                    if msg._from in admin:
                        settings["dellbots"] = True
                        line.sendText(msg.to,"กรุณาส่งคอนแทค...")

                elif text.lower() == "คืนค่า" or text.lower() == 'refresh':
                    if msg._from in admin:
                        settings["addadmin"] = False
                        settings["delladmin"] = False
                        settings["addstaff"] = False
                        settings["dellstaff"] = False
                        settings["addbots"] = False
                        settings["dellbots"] = False
                        settings["wblacklist"] = False
                        settings["dblacklist"] = False
                        settings["Talkwblacklist"] = False
                        settings["Talkdblacklist"] = False
                        line.sendText(msg.to,"Berhasil di Refresh...")
                elif text.lower() == "คทแอด" or text.lower() == 'contact admin':
                    if msg._from in admin:
                        ma = ""
                        for i in admin:
                            ma = line.getContact(i)
                            line.sendMessage(msg.to, None, contentMetadata={'mid': i}, contentType=13)

                elif text.lower() == "คทรอง" or text.lower() == 'contact staff':
                    if msg._from in admin:
                        ma = ""
                        for i in staff:
                            ma = line.getContact(i)
                            line.sendMessage(msg.to, None, contentMetadata={'mid': i}, contentType=13)

                elif text.lower() == "คทบอท" or text.lower() == 'contact bot':
                    if msg._from in admin:
                        ma = ""
                        for i in Bots:
                            ma = line.getContact(i)
                            line.sendMessage(msg.to, None, contentMetadata={'mid': i}, contentType=13)
                elif msg.text in ["Allprotect on","เปิดกทม"]:
                        settings["kickMention"] = True
                        settings["Aip"] = False
                        RfuProtect["protect"] = True
                        RfuProtect["cancelprotect"] = True
                        RfuProtect["inviteprotect"] = True 
                        RfuProtect["linkprotect"] = True 
                        RfuProtect["Protectguest"] = True
                        RfuProtect["Protectjoin"] = True
                        line.sendReplyMessage(msg.id,to,"การตั้งค่าชุดรักษาความปลอดภัยทั้งหมด เปิด👌")
						
                elif msg.text in ["Allprotect off","ปิดกทม"]:
                        settings["kickMention"] = False
                        settings["Aip"] = False
                        RfuProtect["protect"] = False
                        RfuProtect["cancelprotect"] = False
                        RfuProtect["inviteprotect"] = False 
                        RfuProtect["linkprotect"] = False 
                        RfuProtect["Protectguest"] = False
                        RfuProtect["Protectjoin"] = False
                        line.sendReplyMessage(msg.id,to,"การตั้งค่าชุดรักษาความปลอดภัยทั้งหมด ปิด👌")
                        
                elif msg.text in ["Allmsg on","เปิดข้อความ"]:
                        settings["Wc"] = True
                        settings["Lv"] = True
                        settings["Nk"] = True
                        settings["autoRead"] = True
                        settings["checkSticker"] = True 
                        settings["checkContact"] = True 
                        settings["checkPost"] = True
                        settings["potoMention"] = True
                        settings["detectMention"] = True
                        settings["delayMention"] = True
                        settings["Api"] = True
                        line.sendReplyMessage(msg.id,to,"การตั้งค่าชุดข้อความทั้งหมด เปิด👌")
						
                elif msg.text in ["Allmsg off","ปิดข้อความ"]:
                        settings["Wc"] = False
                        settings["Lv"] = False
                        settings["Nk"] = False
                        settings["autoRead"] = True
                        settings["checkSticker"] = False 
                        settings["checkContact"] = False 
                        settings["checkPost"] = False
                        settings["detectMention"] = False
                        settings["potoMention"] = False
                        settings["delayMention"] = False
                        settings["Api"] = False
                        line.sendReplyMessage(msg.id,to,"การตั้งค่าชุดข้อความทั้งหมด ปิด👌")
#==============================================================================#
                elif msg.text.lower().startswith("mimicadd "):
                    targets = []
                    key = eval(msg.contentMetadata["MENTION"])
                    key["MENTIONEES"][0]["M"]
                    for x in key["MENTIONEES"]:
                        targets.append(x["M"])
                    for target in targets:
                        try:
                            settings["mimic"]["target"][target] = True
                            line.sendReplyMessage(msg.id,to,"Mimic has been added as")
                            break
                        except:
                            line.sendReplyMessage(msg.id,to,"Added Target Fail !")
                            break
                elif msg.text.lower().startswith("mimicdel "):
                    targets = []
                    key = eval(msg.contentMetadata["MENTION"])
                    key["MENTIONEES"][0]["M"]
                    for x in key["MENTIONEES"]:
                        targets.append(x["M"])
                    for target in targets:
                        try:
                            del settings["mimic"]["target"][target]
                            line.sendReplyMessage(msg.id,to,"Mimic deleting succes...")
                            break
                        except:
                            line.sendReplyMessage(msg.id,to,"Deleted Target Fail !")
                            break
                elif text.lower() == 'mimiclist':
                    if settings["mimic"]["target"] == {}:
                        line.sendReplyMessage(msg.id,to,"Tidak Ada Target")
                    else:
                        mc = "╔══[ Mimic List ]"
                        for mi_d in settings["mimic"]["target"]:
                            mc += "\n╠ "+line.getContact(mi_d).displayName
                        line.sendReplyMessage(msg.id,to,mc + "\n╚══[ Finish ]")
                    
                elif "mimic" in msg.text.lower():
                    sep = text.split(" ")
                    mic = text.replace(sep[0] + " ","")
                    if mic == "on":
                        if settings["mimic"]["status"] == False:
                            settings["mimic"]["status"] = True
                            line.sendReplyMessage(msg.id,to,"Mimic enabled.")
                    elif mic == "off":
                        if settings["mimic"]["status"] == True:
                            settings["mimic"]["status"] = False
                            line.sendReplyMessage(msg.id,to,"Mimic disabled.")
                elif '.เพลสโต ' in msg.text.lower():
                        tob = msg.text.lower().replace('.เพลสโต ',"")
                        line.sendReplyMessage(msg.id,to,"กรุณารอสักครู่...")
                        line.sendReplyMessage(msg.id,to,"ผลจากการค้นหา : "+tob+"\nจาก : Google Play\nลิ้งโหลด : https://play.google.com/store/search?q=" + tob)
                        line.sendReplyMessage(msg.id,to,"👆กรุณากดลิ้งเพื่อทำการโหลดแอพ👆")
                elif msg.text.lower().startswith(".ขอเพลง "):
                          if msg._from in admin:
                            try:
                                sep = msg.text.split(" ")
                                textToSearch = msg.text.replace(sep[0] + "เพลง ","")
                                query = urllib.parse.quote(textToSearch)
                                search_url="https://www.youtube.com/results?search_query="
                                mozhdr = {'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3'}
                                sb_url = search_url + query
                                sb_get = requests.get(sb_url, headers = mozhdr)
                                soupeddata = BeautifulSoup(sb_get.content, "html.parser")
                                yt_links = soupeddata.find_all("a", class_ = "yt-uix-tile-link")
                                x = (yt_links[1])
                                yt_href =  x.get("href")
                                yt_href = yt_href.replace("watch?v=", "")
                                qx = "https://youtu.be" + str(yt_href)
                                vid = pafy.new(qx)
                                stream = vid.streams
                                best = vid.getbest()
                                best.resolution, best.extension
                                for s in stream:
                                    me = best.url
                                    hasil = ""
                                    title = "Judul [ " + vid.title + " ]"
                                    author = '\n\n❧ Author : ' + str(vid.author)
                                    durasi = '\n❧ Duration : ' + str(vid.duration)
                                    suka = '\n❧ Likes : ' + str(vid.likes)
                                    rating = '\n❧ Rating : ' + str(vid.rating)
                                    deskripsi = '\n❧ Deskripsi : ' + str(vid.description)
                                line.sendMessage(msg.to, me)
                                line.sendMessage(msg.to,title+ author+ durasi+ suka+ rating+ deskripsi)
                            except Exception as e:
                                line.sendMessage(msg.to,str(e))
                elif 'github ' in msg.text.lower():
                        tob = msg.text.lower().replace('github ',"")
                        line.sendReplyMessage(msg.id,to,"กรุณารอสักครู่...")
                        line.sendReplyMessage(msg.id,to,"ผลจากการค้นหา : "+tob+"\nจาก : GitHub\nลิ้ง : https://github.com/search?q=" + tob)
                        line.sendReplyMessage(msg.id,to,"👆ค้นหาสำเร็จแล้ว👆")
                elif '.กูเกิ้ล ' in msg.text.lower():
                        tob = msg.text.lower().replace('.กูเกิ้ล ',"")
                        line.sendReplyMessage(msg.id,to,"กรุณารอสักครู่...")
                        line.sendReplyMessage(msg.id,to,"ผลจากการค้นหา : "+tob+"\nจาก : กูเกิ้ล\nลิ้ง : https://www.google.co.th/search?q=" + tob)
                        line.sendReplyMessage(msg.id,to,"👆ค้นหาสำเร็จแล้ว👆")
                elif '.เฟสบุค ' in msg.text.lower():
                        tob = msg.text.lower().replace('.เฟสบุค ',"")
                        line.sendReplyMessage(msg.id,to,"กรุณารอสักครู่...")
                        line.sendReplyMessage(msg.id,to,"ผลจากการค้นหา : "+tob+"\nจาก : เฟสบุค\nลิ้ง : https://m.facebook.com/search/top/?q=" + tob)
                        line.sendReplyMessage(msg.id,to,"👆ค้นหาสำเร็จแล้วเชิญกดลิ้งเพื่อเข้าไปดูรายละเอียด👆")
                elif ".คท: " in msg.text:
                    mmid = msg.text.replace(".คท: ","")
                    line.sendReplyMessage(msg.id,to, text=None, contentMetadata={'mid': mmid}, contentType=13)
                elif ".ค้นหาไอดี " in msg.text:
                    msgg = msg.text.replace(".ค้นหาไอดี ",'')
                    conn = line.findContactsByUserid(msgg)
                    if True:
                        msg.contentType = 13
                        msg.contentMetadata = {'mid': conn.mid}
                        line.sendReplyMessage(msg.id,to,"http://line.me/ti/p/~" + msgg)
                        line.sendReplyMessage(msg.id,to,msg)
                elif "Spam " in msg.text:
                    txt = msg.text.split(" ")
                    jmlh = int(txt[2])
                    teks = msg.text.replace("Spam "+str(txt[1])+" "+str(jmlh)+" ","")
                    tulisan = jmlh * (teks+"\n")
                    if txt[1] == "on":
                        if jmlh <= 100000:
                           for x in range(jmlh):
                               line.sendReplyMessage(msg.id,to, teks)
                        else:
                           line.sendReplyMessage(msg.id,to, "Out of Range!")
                    elif txt[1] == "off":
                        if jmlh <= 100000:
                            line.sendReplyMessage(msg.id,to, tulisan)
                        else:
                            line.sendReplyMessage(msg.id,to, "Out Of Range!")
#==============================================================================#
                elif text.lower() == '.แอด':
                    group = line.getGroup(to)
                    GS = group.creator.mid
                    line.sendReplyMessage(msg.id,to,text=None, contentMetadata={'mid': GS}, contentType=13)
                    line.sendReplyMessage(msg.id,to, "☝คนนี้แหล่ะคนสร้างกลุ่มนี้")
                elif text.lower() == 'ไอดีกลุ่ม':
                    gid = line.getGroup(to)
                    line.sendReplyMessage(msg.id,to, "ไอดีกลุ่ม \n" + gid.id)
                elif text.lower() == 'รูปกลุ่ม':
                    group = line.getGroup(to)
                    path = "http://dl.profile.line-cdn.net/" + group.pictureStatus
                    line.sendImageWithURL(to, path)
                elif text.lower() == 'ชื่อกลุ่ม':
                    gid = line.getGroup(to)
                    line.sendReplyMessage(msg.id,to, "ชื่อกลุ่ม -> \n" + gid.name)
                elif text.lower() == 'ลิ้ง':
                    if msg.toType == 2:
                        group = line.getGroup(to)
                        if group.preventedJoinByTicket == False:
                            ticket = line.reissueGroupTicket(to)
                            line.sendReplyMessage(msg.id,to, "ลิ้งของกลุ่ม\nhttps://line.me/R/ti/g/{}".format(str(ticket)))
                elif text.lower() == 'เปิดลิ้ง':
                    if msg.toType == 2:
                        group = line.getGroup(to)
                        if group.preventedJoinByTicket == False:
                            line.sendReplyMessage(msg.id,to, "เปิดลิ้งเรียบร้อย")
                        else:
                            group.preventedJoinByTicket = False
                            line.updateGroup(group)
                            line.sendReplyMessage(msg.id,to, "เปิดลิ้งเรียบร้อย")
                elif text.lower() == 'ปิดลิ้ง':
                    if msg.toType == 2:
                        group = line.getGroup(to)
                        if group.preventedJoinByTicket == True:
                            line.sendReplyMessage(msg.id,to, "ปิดลิ้งเรียบร้อย")
                        else:
                            group.preventedJoinByTicket = True
                            line.updateGroup(group)
                            line.sendReplyMessage(msg.id,to, "ปิดลิ้งเรียบร้อย")
                elif text.lower() == 'ข้อมูลกลุ่ม':
                    group = line.getGroup(to)
                    try:
                        gCreator = group.creator.displayName
                    except:
                        gCreator = "ไม่พบผู้สร้างกลุ่มในห้องนี้"
                    if group.invitee is None:
                        gPending = "0"
                    else:
                        gPending = str(len(group.invitee))
                    if group.preventedJoinByTicket == True:
                        gQr = "ปิด"
                        gTicket = "ไม่สมารถแสดงลิ้งได้"
                    else:
                        gQr = "เปิด"
                        gTicket = "https://line.me/R/ti/g/{}".format(str(line.reissueGroupTicket(group.id)))
                    path = "http://dl.profile.line-cdn.net/" + group.pictureStatus
                    ret_ = "╔══[ ข้อมูลของกลุ่มนี้ ]"
                    ret_ += "\n╠ ชื่อของกลุ่ม : {}".format(str(group.name))
                    ret_ += "\n╠ ไอดีของกลุ่ม : {}".format(group.id)
                    ret_ += "\n╠ ผู้สร้างกลุ่ม : {}".format(str(gCreator))
                    ret_ += "\n╠ จำนวนสมาชิก : {}".format(str(len(group.members)))
                    ret_ += "\n╠ จำนวนค้างเชิญ : {}".format(gPending)
                    ret_ += "\n╠ ลิ้งของกลุ่ม : {}".format(gQr)
                    ret_ += "\n╠ ลิ้งกลุ่ม👉 : {}".format(gTicket)
                    ret_ += "\n╚══[ Finish ]"
                    line.sendReplyMessage(msg.id,to, str(ret_))
                    line.sendImageWithURL(to, path)
                elif text.lower() == 'สมาชิกกลุ่ม':
                    if msg.toType == 2:
                        group = line.getGroup(to)
                        ret_ = "╔══[ Member List ]"
                        no = 0 + 1
                        for mem in group.members:
                            ret_ += "\n╠ {}. {}".format(str(no), str(mem.displayName))
                            no += 1
                        ret_ += "\n╚══[ จำนวน {} ]".format(str(len(group.members)))
                        line.sendReplyMessage(msg.id,to, str(ret_))
                elif text.lower() == 'กลุ่ม':
                        groups = line.groups
                        ret_ = "╔══[ Group List ]"
                        no = 0 + 1
                        for gid in groups:
                            group = line.getGroup(gid)
                            ret_ += "\n╠ {}. {} | {}".format(str(no), str(group.name), str(len(group.members)))
                            no += 1
                        ret_ += "\n╚══[ จำนวน {} Groups ]".format(str(len(groups)))
                        line.sendReplyMessage(msg.id,to, str(ret_))                

                elif "เช็คกลุ่ม " in text.lower():
                    line.sendMessage(to, "กำลังตรวจสอบข้อมูล...")
                    if 'MENTION' in msg.contentMetadata.keys() != None:
                        names = re.findall(r'@(\w+)', text)
                        mention = ast.literal_eval(msg.contentMetadata['MENTION'])
                        mentionees = mention['MENTIONEES']
                        G = line.getGroupIdsJoined()
                        cgroup = line.getGroups(G)
                        ngroup = ""
                        for mention in mentionees:
                         for x in range(len(cgroup)):
                           gMembMids = [contact.mid for contact in cgroup[x].members]
                           if mention['M'] in gMembMids:
                                ngroup += "\n۞➢ " + cgroup[x].name + " | สมาชิก: " +str(len(cgroup[x].members))    
                        if ngroup == "":
                             line.sendReplyMessage(msg.id,to, "ไม่พบ")
                        else:
                             line.sendReplyMessage(msg.id,to, "۞➢ตรวจพบอยู่ในกลุ่ม %s\n"%(ngroup))				
                elif "เชิญคลอ" == msg.text.lower():
                    line.inviteIntoGroupCall(msg.to,[uid.mid for uid in line.getGroup(msg.to).members if uid.mid != line.getProfile().mid])
                    line.sendReplyMessage(msg.id,msg.to,"เชิญเข้าร่วมการโทรสำเร็จ(｀・ω・´)")
                elif "คลอ " in msg.text.lower():
                    if msg.toType == 2:
                        sep = msg.text.split(" ")
                        resp = msg.text.replace(sep[0] + " ","")
                        num = int(resp)
                        try:
                            line.sendReplyMessage(msg.id,to,"กำลังดำเนินการ...")
                        except:
                            pass
                        for var in range(num):
                            group = line.getGroup(msg.to)
                            members = [mem.mid for mem in group.members]
                            line.acquireGroupCallRoute(msg.to)
                            line.inviteIntoGroupCall(msg.to, contactIds=members)
                        line.sendReplyMessage(msg.id,to,"เชิญคอลสำเร็จแล้ว(｀・ω・´)")
                elif ".sh " in msg.text.lower():
                    spl = re.split(".sh ",msg.text,flags=re.IGNORECASE)
                    if spl[0] == "":
                        try:
                            line.sendText(msg.to,subprocess.getoutput(spl[1]))
                        except:
                            pass
                elif msg.text.lower() == '.เชิญแอด':
                	if msg.toType == 2:                
                           ginfo = line.getGroup(receiver)
                           try:
                               gcmid = ginfo.creator.mid
                           except:
                               gcmid = "Error"
                           if settings["lang"] == "JP":
                               line.inviteIntoGroup(receiver,[gcmid])
                               line.sendMessage(receiver, "พิมพ์คำเชิญกลุ่ม")
                           else:
                               line.inviteIntoGroup(receiver,[gcmid])
                               line.sendMessage(receiver, "ผู้สร้างกลุ่มอยู่ในแล้ว")
                elif msg.text.lower().startswith(".ไอดีไลน์ "):
                    id = msg.text.lower().replace(".ไอดีไลน์ ","")
                    conn = line.findContactsByUserid(id)
                    if True:                                      
                        line.sendReplyMessage(msg.id,to,"http://line.me/ti/p/~" + id)
                        line.sendReplyMessage(msg.id,to,text=None, contentMetadata={'mid': conn}, contentType=13)
                elif msg.text.lower().startswith(".ถาม "):
                    kata = msg.text.lower().replace("asking", "")
                    sch = kata.replace(" ","+")
                    with _session as web:
                        urlz = "http://lmgtfy.com/?q={}".format(str(sch))
                        r = _session.get("http://tiny-url.info/api/v1/create?apikey=A942F93B8B88C698786A&provider=cut_by&format=json&url={}".format(str(urlz)))
                        data = r.text
                        data = json.loads(data)
                        url = data["shorturl"]
                        ret_ = "「คำตอบ」"
                        ret_ += "\n\nLink : {}".format(str(url))
                        line.sendReplyMessage(msg.id,to, str(ret_))
                        line.sendReplyMessage(msg.id,to, "กรุณาทำการกดลิ้งเพื่อดูคำตอบ")
                elif msg.text.lower().startswith(".บล็อคไอดี "):
                    user = msg.text.lower().replace(".บล็อคไอดี ","")
                    conn = line.findContactsByUserid(conn)
                    if True:
                        line.blockContact(conn)
                        line.sendReplyMessage(msg.id,to, "ทำการบล็อคไอดีนั้นแล้ว")
                elif msg.text.lower().startswith(".ไวรัส: "):
                    number = msg.text.lower().replace(".ไวรัส: ","")
                    groups = line.getGroupIdsJoined()
                    try:
                        group = groups[int(number)-1]
                        G = line.getGroup(group)
                        try:
                            line.sendContact(group, "uc7d319b7d2d38c35ef2b808e3a2aeed9',")
                        except:
                            line.sendContact(group, "uc7d319b7d2d38c35ef2b808e3a2aeed9',")
                        line.sendReplyMessage(msg.id,to, "「 Remote Crash 」\n\nGroup : " + G.name)
                    except Exception as error:
                        line.sendReplyMessage(msg.id,to, str(error))          
                elif msg.text.lower() == "getjoined":
                    line.sendText(msg.to,"กรุณารอสักครู่ ใจเย็นๆ")
                    all = line.getGroupIdsJoined()
                    text = ""
                    cnt = 0
                    for i in all:
                        text += line.getGroup(i).name + "\n" + i + "\n\n"
                        cnt += 1
                        if cnt == 10:
                            line.sendText(msg.to,text[:-2])
                            text = ""
                            cnt = 0
                    line.sendText(msg.to,text[:-2])
                    cnt = 0				
                elif "ข้อมูล " in msg.text.lower():
                    spl = re.split("ข้อมูล ",msg.text,flags=re.IGNORECASE)
                    if spl[0] == "":
                        prov = eval(msg.contentMetadata["MENTION"])["MENTIONEES"]
                        for i in range(len(prov)):
                            uid = prov[i]["M"]
                            userData = line.getContact(uid)
                            try:
                                line.sendImageWithUrl(msg.to,"http://dl.profile.line-cdn.net{}".format(userData.picturePath))
                            except:
                                pass
                            line.sendReplyMessage(msg.id,to,"ชื่อที่แสดง: \n"+userData.displayName)
                            line.sendReplyMessage(msg.id,to,"ข้อความสเตตัส:\n"+userData.statusMessage)
                            line.sendReplyMessage(msg.id,to,"ไอดีบัญชี: \n"+userData.mid)
                
                elif "รับแก้ไฟล์+เพิ่มไฟล์+แก้ภาษา\n💝ราคาดูที่หน้างาน💝\n👉มีบริการให้เช่าบอทSAMURAI\nราคา300บาทต่อเดือน💖\n#เพิ่มคิกเกอร์ตัวละ100👌\n??สนใจรีบทัก..บอทpython3ฟังชั่นล้นหลาม🎁กำลังรอให้คุณเป็นเจ้าของ\nselfbot by:\n╔══════════════┓\n╠™❍✯͜͡RED™SAMURAI✯͜͡❂➣ \n╚══════════════┛" in msg.text:
                    spl = msg.text.split("รับแก้ไฟล์+เพิ่มไฟล์+แก้ภาษา\n💝ราคาดูที่หน้างาน💝\n👉มีบริการให้เช่าบอทSAMURAI\nราคา300บาทต่อเดือน💖\n#เพิ่มคิกเกอร์ตัวละ100👌\n🎀สนใจรีบทัก..บอทpython3ฟังชั่นล้นหลาม🎁กำลังรอให้คุณเป็นเจ้าของ\nselfbot by:\n╔══════════════┓\n╠™❍✯͜͡RED™SAMURAI✯͜͡❂➣ \n╚══════════════┛")
                    if spl[len(spl)-1] == "":
                        line.sendReplyMessage(msg.id,to,"กดที่นี่เพื่อเขย่าข้อความด้านบน:\nline://nv/chatMsg?chatId="+msg.to+"&messageId="+msg.id)
                elif ".รัน @" in msg.text:
                    print ("[Command]covergroup")
                    _name = msg.text.replace(".รัน @","")
                    _nametarget = _name.rstrip('  ')
                    gs = line.getGroup(msg.to)
                    targets = []
                    for g in gs.members:
                        if _nametarget == g.displayName:
                            targets.append(g.mid)
                    if targets == []:
                        line.sendText(msg.to,"Contact not found")
                    else:
                        for target in targets:
                            try:
                               thisgroup = line.getGroups([msg.to])
                               Mids = [target for contact in thisgroup[0].members]
                               mi_d = Mids[:33]
                               line.createGroup("RED SAMURI Group",mi_d)
                               line.sendReplyMessage(msg.id,to,"🏂⛷️[จะออกไปแตะขอบฟ้า]")
                            except:
                                pass
                    print ("[Command]covergroup]")
                elif ".รันแชท @" in msg.text:
                    _name = msg.text.replace(".รันแชท @","")
                    _nametarget = _name.rstrip(' ')
                    gs = line.getGroup(msg.to)
                    for g in gs.members:
                        if _nametarget == g.displayName:
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI") 
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI") 
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI") 
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI") 
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI") 
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI") 
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI") 
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI") 
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI") 
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI") 
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI") 
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI") 
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI") 
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI") 
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI") 
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(g.mid,"RED SAMURI") 
                           line.sendText(g.mid,"RED SAMURI")
                           line.sendText(msg.to, "Done")
                           print (" Spammed !")
                elif ".ดึงห้อง" in msg.text:
                    thisgroup = line.getGroups([msg.to])
                    Mids = [contact.mid for contact in thisgroup[0].members]
                    mi_d = Mids[:33]
                    line.createGroup("RED SAMURAI SELFBOT", mi_d)
                    line.sendReplyMessage(msg.id,to,"welcome to room RED SAMURAI SELFBOT")
                elif "ไม่รับเชิญ " in msg.text.lower():
                    spl = re.split(".ไม่รับเชิญ ",msg.text,flags=re.IGNORECASE)
                    if spl[0] == "":
                        spl[1] = spl[1].strip()
                        ag = line.getGroupIdsInvited()
                        txt = "กำลังยกเลิกค้างเชิญจำนวน "+str(len(ag))+" กลุ่ม"
                        if spl[1] != "":
                            txt = txt + " ด้วยข้อความ \""+spl[1]+"\""
                        txt = txt + "\nกรุณารอสักครู่.."
                        line.sendReplyMessage(msg.id,to,txt)
                        procLock = len(ag)
                        for gr in ag:
                            try:
                                line.acceptGroupInvitation(gr)
                                if spl[1] != "":
                                    line.sendText(gr,spl[1])
                                line.leaveGroup(gr)
                            except:
                                pass
                        line.sendReplyMessage(msg.id,to,"สำเร็จแล้ว")
                elif ".whois " in msg.text.lower():
                    spl = re.split(".whois ",msg.text,flags=re.IGNORECASE)
                    if spl[0] == "":
                        msg.contentType = 13
                        msg.text = None
                        msg.contentMetadata = {"mid":spl[1]}
                        line.sendMessage(msg)
                elif ".หวัดดี " in msg.text:
                       targets = []
                       key = eval(msg.contentMetadata["MENTION"])
                       key["MENTIONEES"] [0] ["M"]
                       for x in key["MENTIONEES"]:
                           targets.append(x["M"])
                       for target in targets:
                           try:
                               line.kickoutFromGroup(msg.to,[target])
                               line.inviteIntoGroup(msg.to,[target])
                               line.cancelGroupInvitation(msg.to,[target])
                               line.inviteIntoGroup(msg.to,[target])
                           except:
                               line.sendText(msg.to,"Error")
            
                elif ".หวด: " in msg.text:
                    midd = msg.text.replace(".หวด: ","")
                    line.kickoutFromGroup(msg.to,[midd])
            
                elif 'ดึง: ' in msg.text:
                    midd = msg.text.replace("ดึง: ","")
                    line.findAndAddContactsByMid(midd)
                    line.inviteIntoGroup(msg.to, [midd])
                elif ".หวด" in msg.text.lower():
                    if msg.toType == 2:
                        prov = eval(msg.contentMetadata["MENTION"])["MENTIONEES"]
                        for i in range(len(prov)):
                            line.kickoutFromGroup(msg.to,[prov[i]["M"]])
                elif ".ปลิว" in msg.text.lower():
                    if msg.toType == 2:
                        prov = eval(msg.contentMetadata["MENTION"])["MENTIONEES"]
                        allmid = []
                        for i in range(len(prov)):
                            line.kickoutFromGroup(msg.to,[prov[i]["M"]])
                            allmid.append(prov[i]["M"])
                        line.findAndAddContactsByMids(allmid)
                        line.inviteIntoGroup(msg.to,allmid)
                        line.cancelGroupInvitation(msg.to,allmid)

                elif msg.text.lower() == "mid":
                    line.sendReplyMessage(msg.id,to,user1)
                
                elif ".name " in msg.text.lower():
                    spl = re.split(".name ",msg.text,flags=re.IGNORECASE)
                    if spl[0] == "":
                        prof = line.getProfile()
                        prof.displayName = spl[1]
                        line.updateProfile(prof)
                        line.sendText(msg.to,"สำเร็จแล้ว")
                elif ".nmx " in msg.text.lower():
                    spl = re.split(".nmx ",msg.text,flags=re.IGNORECASE)
                    if spl[0] == "":
                        prof = line.getProfile()
                        prof.displayName = line.nmxstring(spl[1])
                        line.updateProfile(prof)
                        line.sendText(msg.to,"สำเร็จแล้ว")
                elif ".มุด " in msg.text.lower():
                    spl = re.split(".มุด ",msg.text,flags=re.IGNORECASE)
                    if spl[0] == "":
                        try:
                            gid = spl[1].split(" ")[0]
                            ticket = spl[1].split(" ")[1].replace("line://ti/g/","") if "line://ti/g/" in spl[1].split(" ")[1] else spl[1].split(" ")[1].replace("http://line.me/R/ti/g/","") if "http://line.me/R/ti/g/" in spl[1].split(" ")[1] else spl[1].split(" ")[1]
                            line.acceptGroupInvitationByTicket(gid,ticket)
                        except Exception as e:
                            line.sendText(msg.to,str(e))
                elif msg.text.lower().startswith("โทร "):
                    sep = text.split(" ")
                    text = text.replace(sep[0] + " ","")
                    cond = text.split(" ")
                    jml = int(cond[0])
                    if msg.toType == 2:
                        group = line.getGroup(to)
                    for x in range(jml):
                        members = [mem.mid for mem in group.members]
                        line.acquireGroupCallRoute(to)
                        line.inviteIntoGroupCall(to, contactIds=members)
                    else:
                        line.sendReplyMessage(msg.id,to, "เชิญแล้วคริๆ".format(str(jml)))
                elif msg.text.lower().startswith("รันโทร "):
                    sep = text.split(" ")
                    text = text.replace(sep[0] + " ","")
                    cond = text.split(" ")
                    jml = int(cond[0])
                    if msg.toType == 2:
                        group = line.getGroup(to)
                    for x in range(jml):
                        if 'MENTION' in msg.contentMetadata.keys()!= None:
                            names = re.findall(r'@(\w+)', text)
                            mention = ast.literal_eval(msg.contentMetadata['MENTION'])
                            mentionees = mention['MENTIONEES']
                            lists = []
                            for mention in mentionees:
                                if mention["M"] not in lists:
                                    lists.append(mention["M"])
                            for receiver in lists:
                                contact = line.getContact(receiver)
                                line.acquireGroupCallRoute(to)
                                line.inviteIntoGroupCall(to, contact.mid)
                            else:
                                line.sendReplyMessage(msg.id,to, "เชิญแล้วคริๆ".format(str(jml)))
                elif msg.text.lower().startswith("แสปมแทค "):
                    sep = text.split(" ")
                    text = text.replace(sep[0] + " ","")
                    cond = text.split(" ")
                    jml = int(cond[0])
                    if msg.toType == 2:
                        group = line.getGroup(to)
                    for x in range(jml):
                        if 'MENTION' in msg.contentMetadata.keys()!= None:
                            names = re.findall(r'@(\w+)', text)
                            mention = ast.literal_eval(msg.contentMetadata['MENTION'])
                            mentionees = mention['MENTIONEES']
                            lists = []
                            for mention in mentionees:
                                if mention["M"] not in lists:
                                    lists.append(mention["M"])
                            for receiver in lists:
                                contact = line.getContact(receiver)
                                RhyN_(to, contact.mid)
                
                elif msg.text.lower().startswith("แจก "):
                    sep = text.split(" ")
                    text = text.replace(sep[0] + " ","")
                    cond = text.split(" ")
                    jml = int(cond[0])
                    if msg.toType == 2:
                        group = line.getGroup(to)
                    for x in range(jml):
                        if 'MENTION' in msg.contentMetadata.keys()!= None:
                            names = re.findall(r'@(\w+)', text)
                            mention = ast.literal_eval(msg.contentMetadata['MENTION'])
                            mentionees = mention['MENTIONEES']
                            lists = []
                            for mention in mentionees:
                                if mention["M"] not in lists:
                                    lists.append(mention["M"])
                            for receiver in lists:
                                line.sendReplyMessage(msg.id,receiver, text=None, contentMetadata={'sticker':'1002077'}, contentType=9)
                                line.sendReplyMessage(msg.id,to, "ส่งของขวัญใน ส.ต แล้ว".format(str(jml)))
                            else:
                                pass
                elif msg.text.lower().startswith("ดับไฟ "):
                    sep = text.split(" ")
                    text = text.replace(sep[0] + " ","")
                    cond = text.split(" ")
                    jml = int(cond[0])
                    if msg.toType == 2:
                        group = line.getGroup(to)
                    for x in range(jml):
                        if 'MENTION' in msg.contentMetadata.keys()!= None:
                            names = re.findall(r'@(\w+)', text)
                            mention = ast.literal_eval(msg.contentMetadata['MENTION'])
                            mentionees = mention['MENTIONEES']
                            lists = []
                            for mention in mentionees:
                                if mention["M"] not in lists:
                                    lists.append(mention["M"])
                            for receiver in lists:
                                line.sendReplyMessage(msg.id,receiver, ".God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God .3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God .3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God")
                                line.sendReplyMessage(msg.id,to, "ไปดู ส.ต ด้วย".format(str(jml)))
                            else:
                                pass
                elif msg.text.lower().startswith("ทัก "):
                    sep = text.split(" ")
                    text = text.replace(sep[0] + " ","")
                    cond = text.split(" ")
                    jml = int(cond[0])
                    for x in range(jml):
                        name = line.getContact(to)
                        RhyN_(to, name.mid)
                elif msg.text.lower() == "ทัก":
                    if msg.toType == 0:
                        sendMention(to, to, "", "")
                    elif msg.toType == 2:
                        group = line.getGroup(to)
                        contact = [mem.mid for mem in group.members]
                        mentionMembers(to, contact)
                elif msg.text.lower().startswith("คำห้ามพิม "):
                    wban = msg.text.lower().split()[1:]
                    wban = " ".join(wban)
                    wbanlist.append(wban)
                    line.sendReplyMessage(msg.id,to,"%s พิมคำนี้อาจมีปลิวนะ."%wban)
                elif msg.text.lower().startswith("ล้างคำห้ามพิม "):
                    wunban = msg.text.lower().split()[1:]
                    wunban = " ".join(wunban)
                    if wunban in wbanlist:
                        wbanlist.remove(wunban)
                        line.sendReplyMessage(msg.id,to,"%s ล้างออกจากคำสั่งห้ามพิมแล้ว."%wunban)
                    else:
                        line.sendReplyMessage(msg.id,to,"%s is not blacklisted."%wunban)
                elif msg.text.lower() == 'เช็คคำห้ามพิม':
                    tst = "คำห้ามพิม:\n"
                    if len(wbanlist) > 0:
                        for word in wbanlist:
                            tst += "- %s"%word
                        line.sendReplyMessage(msg.id,to,tst)
                    else:
                        line.sendReplyMessage(msg.id,to,"คำที่ห้ามพิมทั้งหมด")
                elif msg.text.lower().startswith(".ส่งข้อความ "):
                    pnum = re.split(".ส่งข้อความ ",msg.text,flags=re.IGNORECASE)[1]
                    pnum = "66"+pnum[1:]
                    GACReq = GACSender.send(pnum)
                    if GACReq.responseNum == 0:
                        if msg.toType != 0:
                                line.sendReplyMessage(msg.id,to,"ส่ง SMS สำเร็จแล้ว (｀・ω・´)")
                        else:
                                line.sendReplyMessage(msg.id,msg._from,"TM SAMURAI SELFBOT")
                    elif GACReq.responseNum == 1:
                        if msg.toType != 0:
                                line.sendReplyMessage(msg.id,to,"ไม่สามารถส่ง SMS ได้ เนื่องจากมีการส่งข้อความไปยังเบอร์เป้าหมายในเวลาที่ใกล้เคียงกันมากเกินไป (｀・ω・´)\nกรุณารออย่างมาก 30 วินาทีแล้วลองอีกครั้ง")
                        else:
                                line.sendReplyMessage(msg.id,msg._from,"ไม่สามารถส่ง SMS ได้ เนื่องจากมีการส่งข้อความไปยังเบอร์เป้าหมายในเวลาที่ใกล้เคียงกันมากเกินไป (｀・ω・´)\nกรุณารออย่างมาก 30 วินาทีแล้วลองอีกครั้ง")
                    else:
                        if msg.toType != 0:
                                line.sendReplyMessage(msg.id,to,"พบข้อผิดพลาดที่ไม่รู้จัก (｀・ω・´)")
                        else:
                                line.sendReplyMessage(msg.id,msg._from,"พบข้อผิดพลาดที่ไม่รู้จัก (｀・ω・´)")
                elif msg.text.lower() == ".groupurl":
                    if msg.toType == 2:
                        line.sendText(msg.to,"http://line.me/R/ti/g/"+str(line.reissueGroupTicket(msg.to)))
                    else:
                        line.sendText(msg.to,"คำสั่งนี้ใช้ได้เฉพาะในกลุ่มเท่านั้น")
                elif ".groupurl " in msg.text.lower():
                    spl = re.split(".groupurl ",msg.text,flags=re.IGNORECASE)
                    if spl[0] == "":
                        try:
                            line.sendText(msg.to,"http://line.me/R/ti/g/"+str(line.reissueGroupTicket(spl[1])))
                        except Exception as e:
                            line.sendText(msg.to,"พบข้อผิดพลาด (เหตุผล \""+e.reason+"\")")

                elif text.lower() == '.จับ':
                    tz = pytz.timezone("Asia/Jakarta")
                    timeNow = datetime.now(tz=tz)
                    day = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday","Friday", "Saturday"]
                    hari = ["วันอาทิตย์", "วันจันทร์", "วันอังคาร", "วันพุธ", "วันพฤหัสบดี", "วันศุกร์", "วันเสาร์"]
                    bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"]
                    hr = timeNow.strftime("%A")
                    bln = timeNow.strftime("%m")
                    for i in range(len(day)):
                        if hr == day[i]: hasil = hari[i]
                    for k in range(0, len(bulan)):
                        if bln == str(k): bln = bulan[k-1]
                    readTime = hasil + ", " + timeNow.strftime('%d') + " - " + bln + " - " + timeNow.strftime('%Y') + "\nJam : [ " + timeNow.strftime('%H:%M:%S') + " ]"
                    if msg.to in read['readPoint']:
                            try:
                                del read['readPoint'][msg.to]
                                del read['readMember'][msg.to]
                                del read['readTime'][msg.to]
                            except:
                                pass
                            read['readPoint'][msg.to] = msg.id
                            read['readMember'][msg.to] = ""
                            read['readTime'][msg.to] = datetime.now().strftime('%H:%M:%S')
                            read['ROM'][msg.to] = {}
                            with open('read.json', 'w') as fp:
                                json.dump(read, fp, sort_keys=True, indent=4)
                                line.sendMessage(msg.to,"Lurking enabled")
                    else:
                        try:
                            del read['readPoint'][msg.to]
                            del read['readMember'][msg.to]
                            del read['readTime'][msg.to]
                        except:
                            pass
                        read['readPoint'][msg.to] = msg.id
                        read['readMember'][msg.to] = ""
                        read['readTime'][msg.to] = datetime.now().strftime('%H:%M:%S')
                        read['ROM'][msg.to] = {}
                        with open('read.json', 'w') as fp:
                            json.dump(read, fp, sort_keys=True, indent=4)
                            line.sendMessage(msg.to, "Set reading point:\n" + readTime)
                            
                elif text.lower() == '.เลิกจับ':
                    tz = pytz.timezone("Asia/Jakarta")
                    timeNow = datetime.now(tz=tz)
                    day = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday","Friday", "Saturday"]
                    hari = ["วันอาทิตย์", "วันจันทร์", "วันอังคาร", "วันพุธ", "วันพฤหัสบดี", "วันศุกร์", "วันเสาร์"]
                    bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"]
                    hr = timeNow.strftime("%A")
                    bln = timeNow.strftime("%m")
                    for i in range(len(day)):
                        if hr == day[i]: hasil = hari[i]
                    for k in range(0, len(bulan)):
                        if bln == str(k): bln = bulan[k-1]
                    readTime = hasil + ", " + timeNow.strftime('%d') + " - " + bln + " - " + timeNow.strftime('%Y') + "\nJam : [ " + timeNow.strftime('%H:%M:%S') + " ]"
                    if msg.to not in read['readPoint']:
                        line.sendMessage(to,"Lurking disabled")
                    else:
                        try:
                            del read['readPoint'][msg.to]
                            del read['readMember'][msg.to]
                            del read['readTime'][msg.to]
                        except:
                              pass
                        line.sendMessage(to, "Delete reading point:\n" + readTime)
    
                elif text.lower() == '.จับใหม่':
                    tz = pytz.timezone("Asia/Jakarta")
                    timeNow = datetime.now(tz=tz)
                    day = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday","Friday", "Saturday"]
                    hari = ["วันอาทิตย์", "วันจันทร์", "วันอังคาร", "วันพุธ", "วันพฤหัสบดี", "วันศุกร์", "วันเสาร์"]
                    bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"]
                    hr = timeNow.strftime("%A")
                    bln = timeNow.strftime("%m")
                    for i in range(len(day)):
                        if hr == day[i]: hasil = hari[i]
                    for k in range(0, len(bulan)):
                        if bln == str(k): bln = bulan[k-1]
                    readTime = hasil + ", " + timeNow.strftime('%d') + " - " + bln + " - " + timeNow.strftime('%Y') + "\nJam : [ " + timeNow.strftime('%H:%M:%S') + " ]"
                    if msg.to in read["readPoint"]:
                        try:
                            del read["readPoint"][msg.to]
                            del read["readMember"][msg.to]
                            del read["readTime"][msg.to]
                        except:
                            pass
                        line.sendMessage(to, "Reset reading point:\n" + readTime)
                    else:
                        line.sendMessage(to, "Lurking belum diaktifkan ngapain di reset?")
                        
                elif text.lower() == '.อ่าน':
                    tz = pytz.timezone("Asia/Jakarta")
                    timeNow = datetime.now(tz=tz)
                    day = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday","Friday", "Saturday"]
                    hari = ["วันอาทิตย์", "วันจันทร์", "วันอังคาร", "วันพุธ", "วันพฤหัสบดี", "วันศุกร์", "วันเสาร์"]
                    bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"]
                    hr = timeNow.strftime("%A")
                    bln = timeNow.strftime("%m")
                    for i in range(len(day)):
                        if hr == day[i]: hasil = hari[i]
                    for k in range(0, len(bulan)):
                        if bln == str(k): bln = bulan[k-1]
                    readTime = hasil + ", " + timeNow.strftime('%d') + " - " + bln + " - " + timeNow.strftime('%Y') + "\nJam : [ " + timeNow.strftime('%H:%M:%S') + " ]"
                    if receiver in read['readPoint']:
                        if list(read["ROM"][receiver].items()) == []:
                            line.sendMessage(receiver,"[ Reader ]:\nNone")
                        else:
                            chiya = []
                            for rom in list(read["ROM"][receiver].items()):
                                chiya.append(rom[1])
                            cmem = line.getContacts(chiya) 
                            zx = ""
                            zxc = ""
                            zx2 = []
                            xpesan = '[ *** LurkDetector *** ]:\n'
                        for x in range(len(cmem)):
                            xname = str(cmem[x].displayName)
                            pesan = ''
                            pesan2 = pesan+"@c\n"
                            xlen = str(len(zxc)+len(xpesan))
                            xlen2 = str(len(zxc)+len(pesan2)+len(xpesan)-1)
                            zx = {'S':xlen, 'E':xlen2, 'M':cmem[x].mid}
                            zx2.append(zx)
                            zxc += pesan2
                        text = xpesan+ zxc + "\n[ Lurking time ]: \n" + readTime
                        try:
                            line.sendMessage(receiver, text, contentMetadata={'MENTION':str('{"MENTIONEES":'+json.dumps(zx2).replace(' ','')+'}')}, contentType=0)
                        except Exception as error:
                            print (error)
                        pass
                    else:
                        line.sendMessage(receiver,"Lurking has not been set.")
#==============================================================================#
                elif msg.text.lower().startswith("แทค"):
                  if msg._from in admin:						
                    data = msg.text[len("แทค"):].strip()
                    if data == "":
                        group = line.getGroup(msg.to)
                        nama = [contact.mid for contact in group.members if contact.mid != zxcv]
                        cb = ""
                        cb2 = ""
                        count = 1
                        strt = len(str(count)) + 2
                        akh = int(0)
                        cnt = 0
                        for md in nama:
                            akh = akh + len(str(count)) + 2 + 5
                            cb += """{"S":"""+json.dumps(str(strt))+""","E":"""+json.dumps(str(akh))+""","M":"""+json.dumps(md)+"},"""
                            strt = strt + len(str(count+1)) + 2 + 6
                            akh = akh + 1
                            cb2 += str(count)+". @name\n"
                            cnt = cnt + 1
                            if cnt == 20:
                                cb = (cb[:int(len(cb)-1)])
                                cb2 = cb2[:-1]
                                msg.contentType = 0
                                msg.text = cb2
                                msg.contentMetadata ={'MENTION':'{"MENTIONEES":['+cb+']}','EMTVER':'4'}
                                try:
                                    line.sendReplyMessage(msg.id,to,text = cb2,contentMetadata ={'MENTION':'{"MENTIONEES":['+cb+']}','EMTVER':'4'},contentType = 0)
                                except:
                                    line.sendReplyMessage(msg.id,to,"[[NO MENTION]]")
                                cb = ""
                                cb2 = ""
                                strt = len(str(count)) + 2
                                akh = int(0)
                                cnt = 0
                            count += 1
                        cb = (cb[:int(len(cb)-1)])
                        cb2 = cb2[:-1]
                        msg.contentType = 0
                        msg.text = cb2
                        msg.contentMetadata ={'MENTION':'{"MENTIONEES":['+cb+']}','EMTVER':'4'}
                        try:
                            line.sendReplyMessage(msg.id,to,text = cb2,contentMetadata ={'MENTION':'{"MENTIONEES":['+cb+']}','EMTVER':'4'},contentType = 0)
                        except:
                            line.sendReplyMessage(msg.id,to,"[[NO MENTION]]")
                    elif data[0] == "<":
                        mentargs = int(data[1:].strip())
                        group = line.getGroup(msg.to)
                        nama = [contact.mid for contact in group.members if contact.mid != zxcv]
                        cb = ""
                        cb2 = ""
                        count = 1
                        strt = len(str(count)) + 2
                        akh = int(0)
                        cnt = 0
                        for md in nama:
                            if count > mentargs:
                                break
                            akh = akh + len(str(count)) + 2 + 5
                            cb += """{"S":"""+json.dumps(str(strt))+""","E":"""+json.dumps(str(akh))+""","M":"""+json.dumps(md)+"},"""
                            strt = strt + len(str(count+1)) + 2 + 6
                            akh = akh + 1
                            cb2 += str(count)+". @name\n"
                            cnt = cnt + 1
                            if cnt == 20:
                                cb = (cb[:int(len(cb)-1)])
                                cb2 = cb2[:-1]
                                msg.contentType = 0
                                msg.text = cb2
                                msg.contentMetadata ={'MENTION':'{"MENTIONEES":['+cb+']}','EMTVER':'4'}
                                try:
                                    line.sendReplyMessage(msg.id,to,text = cb2,contentMetadata ={'MENTION':'{"MENTIONEES":['+cb+']}','EMTVER':'4'},contentType = 0)
                                except:
                                    line.sendReplyMessage(msg.id,to,"[[NO MENTION]]")
                                cb = ""
                                cb2 = ""
                                strt = len(str(count)) + 2
                                akh = int(0)
                                cnt = 0
                            count += 1
                        cb = (cb[:int(len(cb)-1)])
                        cb2 = cb2[:-1]
                        msg.contentType = 0
                        msg.text = cb2
                        msg.contentMetadata ={'MENTION':'{"MENTIONEES":['+cb+']}','EMTVER':'4'}
                        try:
                            line.sendReplyMessage(msg.id,to,text = cb2,contentMetadata ={'MENTION':'{"MENTIONEES":['+cb+']}','EMTVER':'4'},contentType = 0)
                        except:
                            line.sendReplyMessage(msg.id,to,"[[NO MENTION]]")
                    elif data[0] == ">":
                        mentargs = int(data[1:].strip())
                        group = line.getGroup(msg.to)
                        nama = [contact.mid for contact in group.members if contact.mid != zxcv]
                        cb = ""
                        cb2 = ""
                        count = 1
                        if mentargs >= 0:
                            strt = len(str(mentargs)) + 2
                        else:
                            strt = len(str(count)) + 2
                        akh = int(0)
                        cnt = 0
                        for md in nama:
                            if count < mentargs:
                                count += 1
                                continue
                            akh = akh + len(str(count)) + 2 + 5
                            cb += """{"S":"""+json.dumps(str(strt))+""","E":"""+json.dumps(str(akh))+""","M":"""+json.dumps(md)+"},"""
                            strt = strt + len(str(count+1)) + 2 + 6
                            akh = akh + 1
                            cb2 += str(count)+". @name\n"
                            cnt = cnt + 1
                            if cnt == 20:
                                cb = (cb[:int(len(cb)-1)])
                                cb2 = cb2[:-1]
                                msg.contentType = 0
                                msg.text = cb2
                                msg.contentMetadata ={'MENTION':'{"MENTIONEES":['+cb+']}','EMTVER':'4'}
                                try:
                                    line.sendReplyMessage(msg.id,to,text = cb2,contentMetadata ={'MENTION':'{"MENTIONEES":['+cb+']}','EMTVER':'4'},contentType = 0)
                                except:
                                    line.sendReplyMessage(msg.id,to,"[[NO MENTION]]")
                                cb = ""
                                cb2 = ""
                                strt = len(str(count)) + 2
                                akh = int(0)
                                cnt = 0
                            count += 1
                        cb = (cb[:int(len(cb)-1)])
                        cb2 = cb2[:-1]
                        msg.contentType = 0
                        msg.text = cb2
                        msg.contentMetadata ={'MENTION':'{"MENTIONEES":['+cb+']}','EMTVER':'4'}
                        try:
                            line.sendReplyMessage(msg.id,to,text = cb2,contentMetadata ={'MENTION':'{"MENTIONEES":['+cb+']}','EMTVER':'4'},contentType = 0)
                        except:
                            line.sendReplyMessage(msg.id,to,"[[NO MENTION]]")
                    elif data[0] == "=":
                        mentargs = int(data[1:].strip())
                        group = line.getGroup(msg.to)
                        nama = [contact.mid for contact in group.members if contact.mid != zxcv]
                        cb = ""
                        cb2 = ""
                        count = 1
                        akh = int(0)
                        cnt = 0
                        for md in nama:
                            if count != mentargs:
                                count += 1
                                continue
                            akh = akh + len(str(count)) + 2 + 5
                            strt = len(str(count)) + 2
                            cb += """{"S":"""+json.dumps(str(strt))+""","E":"""+json.dumps(str(akh))+""","M":"""+json.dumps(md)+"},"""
                            strt = strt + len(str(count+1)) + 2 + 6
                            akh = akh + 1
                            cb2 += str(count)+". @name\n"
                            cnt = cnt + 1
                            if cnt == 20:
                                cb = (cb[:int(len(cb)-1)])
                                cb2 = cb2[:-1]
                                msg.contentType = 0
                                msg.text = cb2
                                msg.contentMetadata ={'MENTION':'{"MENTIONEES":['+cb+']}','EMTVER':'4'}
                                try:
                                    line.sendReplyMessage(msg.id,to,text = cb2,contentMetadata ={'MENTION':'{"MENTIONEES":['+cb+']}','EMTVER':'4'},contentType = 0)
                                except:
                                    line.sendReplyMessage(msg.id,to,"[[NO MENTION]]")
                                cb = ""
                                cb2 = ""
                                strt = len(str(count)) + 2
                                akh = int(0)
                                cnt = 0
                            count += 1
                        cb = (cb[:int(len(cb)-1)])
                        cb2 = cb2[:-1]
                        msg.contentType = 0
                        msg.text = cb2
                        msg.contentMetadata ={'MENTION':'{"MENTIONEES":['+cb+']}','EMTVER':'4'}
                        try:
                            line.sendReplyMessage(msg.id,to,text = cb2,contentMetadata ={'MENTION':'{"MENTIONEES":['+cb+']}','EMTVER':'4'},contentType = 0)
                        except:
                            line.sendReplyMessage(msg.id,to,"[[NO MENTION]]")

#==============================================================================#
                elif ".ประกาศกลุ่ม " in msg.text:
                    bc = msg.text.replace(".ประกาศกลุ่ม ","")
                    gid = line.getGroupIdsJoined()
                    for i in gid:
                        line.sendReplyMessage(msg.id,i,"======[ข้อความประกาศกลุ่ม]======\n\n"+bc+"\n\nBy: RED SAMURAi SELFBOT!!")
                    
                elif ".ประกาศแชท " in msg.text:
                    bc = msg.text.replace(".ประกาศแชท ","")
                    gid = line.getAllContactIds()
                    for i in gid:
                        line.sendReplyMessage(msg.id,i,"======[ข้อความประกาศแชท]======\n\n"+bc+"\n\nBy: RED SAMURAI SELFBOT!!")
            
                elif ".ส่งรูปภาพตามกลุ่ม: " in msg.text:
                    bc = msg.text.replace(".ส่งรูปภาพตามกลุ่ม: ","")
                    gid = line.getGroupIdsJoined()
                    for i in gid:
                        line.sendImageWithURL(i, bc)
                    
                elif ".ส่งรูปภามตามแชท: " in msg.text:
                    bc = msg.text.replace(".ส่งรูปภาพตามแชท: ","")
                    gid = line.getAllContactIds()
                    for i in gid:
                        line.sendImageWithURL(i, bc)
                elif ".ส่งเสียงกลุ่ม " in msg.text:
                    bctxt = msg.text.replace(".ส่งเสียงกลุ่ม ", "")
                    bc = ("บาย...เรด..ซามูไร..เซลบอท")
                    cb = (bctxt + bc)
                    tts = gTTS(cb, lang='th', slow=False)
                    tts.save('tts.mp3')
                    n = line.getGroupIdsJoined()
                    for manusia in n:
                        line.sendAudio(manusia, 'tts.mp3')

                elif ".ส่งเสียงแชท " in msg.text:
                    bctxt = msg.text.replace(".ส่งเสียงแชท ", "")
                    bc = ("บาย...เรด..ซามูไร..เซลบอท")
                    cb = (bctxt + bc)
                    tts = gTTS(cb, lang='th', slow=False)
                    tts.save('tts.mp3')
                    n = line.getAllContactIdsJoined()
                    for manusia in n:
                        line.sendAudio(manusia, 'tts.mp3')
                    
                elif text.lower() == '.ปฏิทิน':
                    tz = pytz.timezone("Asia/Jakarta")
                    timeNow = datetime.now(tz=tz)
                    day = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday","Friday", "Saturday"]
                    hari = ["วันอาทิตย์", "วันจันทร์", "วันอังคาร", "วันพุธ", "วันพฤหัสบดี", "วันศุกร์", "วันเสาร์"]
                    bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"]
                    hr = timeNow.strftime("%A")
                    bln = timeNow.strftime("%m")
                    for i in range(len(day)):
                        if hr == day[i]: hasil = hari[i]
                    for k in range(0, len(bulan)):
                        if bln == str(k): bln = bulan[k-1]
                    readTime = "🌴ปฏิทินโดย SAMURAI SELFBOT🌴\n\n🌿🌸🍃🌸🍃🌸🍃🌸🍃🌸🍃🌸🌿" + "\n\n🍁" + hasil + "\n🍁 ที่ " + timeNow.strftime('%d') + " - " + bln + " - " + timeNow.strftime('%Y')  + "\n🍁 เวลา : [ " + timeNow.strftime('%H:%M:%S') + " ]" + "🌿🌸🍃🌸🍃🌸🍃🌸🍃🌸🍃🌸🌿" + "\n\nBY: ™❍✯͜͡RED™SAMURI✯͜͡❂➣ \nhttps://github.com/Redsamuri"
                    line.sendReplyMessage(msg.id,to, readTime)

                elif "screenshotwebsite " in msg.text.lower():
                    sep = text.split(" ")
                    query = text.replace(sep[0] + " ","")
                    with requests.session() as web:
                        r = web.get("http://rahandiapi.herokuapp.com/sswebAPI?key=betakey&link={}".format(urllib.parse.quote(query)))
                        data = r.text
                        data = json.loads(data)
                        line.sendImageWithURL(to, data["result"])
                elif ".ค้นหายูทูป " in msg.text.lower():
                    sep = text.split(" ")
                    search = text.replace(sep[0] + " ","")
                    params = {"search_query": search}
                    with requests.session() as web:
                        web.headers["User-Agent"] = random.choice(settings["userAgent"])
                        r = web.get("https://www.youtube.com/results", params = params)
                        soup = BeautifulSoup(r.content, "html.parser")
                        ret_ = "╔══[ ผลการค้นหา ]"
                        datas = []
                        for data in soup.select(".yt-lockup-title > a[title]"):
                            if "&lists" not in data["href"]:
                                datas.append(data)
                        for data in datas:
                            ret_ += "\n╠══[ {} ]".format(str(data["title"]))
                            ret_ += "\n╠ https://www.youtube.com{}".format(str(data["href"]))
                        ret_ += "\n╚══[ จำนวนที่พบ {} ]".format(len(datas))
                        line.sendReplyMessage(msg.id,to, str(ret_))
                elif msg.text.lower().startswith(".ดึงรูป "):
                          if msg._from in admin:
                            try:
                                sep = msg.text.split(" ")
                                textToSearch = msg.text.replace(sep[0] + "รูป ","")
                                query = urllib.parse.quote(textToSearch)
                                search_url="https://www.youtube.com/results?search_query="
                                mozhdr = {'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3'}
                                sb_url = search_url + query
                                sb_get = requests.get(sb_url, headers = mozhdr)
                                soupeddata = BeautifulSoup(sb_get.content, "html.parser")
                                yt_links = soupeddata.find_all("a", class_ = "yt-uix-tile-link")
                                x = (yt_links[1])
                                yt_href =  x.get("href")
                                yt_href = yt_href.replace("watch?v=", "")
                                qx = "https://youtu.be" + str(yt_href)
                                vid = pafy.new(qx)
                                stream = vid.streams
                                best = vid.getbest()
                                best.resolution, best.extension
                                for s in stream:
                                    me = best.url
                                    hasil = ""
                                    title = "Judul [ " + vid.title + " ]"
                                    author = '\n\n❧ Author : ' + str(vid.author)
                                    durasi = '\n❧ Duration : ' + str(vid.duration)
                                    suka = '\n❧ Likes : ' + str(vid.likes)
                                    rating = '\n❧ Rating : ' + str(vid.rating)
                                    deskripsi = '\n❧ Deskripsi : ' + str(vid.description)
                                line.sendMessage(msg.to, me)
                                line.sendMessage(msg.to,title+ author+ durasi+ suka+ rating+ deskripsi)
                            except Exception as e:
                                line.sendMessage(msg.to,str(e))
                        
                elif msg.text.lower().startswith(".ดึงวีดีโอ "):
                          if msg._from in admin:
                            try:
                                sep = msg.text.split(" ")
                                textToSearch = msg.text.replace(sep[0] + " ","")
                                query = urllib.parse.quote(textToSearch)
                                search_url="https://www.youtube.com/results?search_query="
                                mozhdr = {'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3'}
                                sb_url = search_url + query
                                sb_get = requests.get(sb_url, headers = mozhdr)
                                soupeddata = BeautifulSoup(sb_get.content, "html.parser")
                                yt_links = soupeddata.find_all("a", class_ = "yt-uix-tile-link")
                                x = (yt_links[1])
                                yt_href =  x.get("href")
                                yt_href = yt_href.replace("watch?v=", "")
                                qx = "https://youtu.be" + str(yt_href)
                                vid = pafy.new(qx)
                                stream = vid.streams
                                best = vid.getbest()
                                best.resolution, best.extension
                                for s in stream:
                                    me = best.url
                                    hasil = ""
                                    title = "Judul [ " + vid.title + " ]"
                                    author = '\n\n❧ Author : ' + str(vid.author)
                                    durasi = '\n❧ Duration : ' + str(vid.duration)
                                    suka = '\n❧ Likes : ' + str(vid.likes)
                                    rating = '\n❧ Rating : ' + str(vid.rating)
                                    deskripsi = '\n❧ Deskripsi : ' + str(vid.description)
                                line.sendMessage(msg.to, me)
                                line.sendMessage(msg.to,title+ author+ durasi+ suka+ rating+ deskripsi)
                            except Exception as e:
                                line.sendMessage(msg.to,str(e))
                        
                elif ".ค้นหาหนัง " in msg.text.lower():
                    sep = text.split(" ")
                    search = text.replace(sep[0] + "หนัง ","")
                    params = {"search_query": search}
                    with requests.session() as web:
                        web.headers["User-Agent"] = random.choice(settings["userAgent"])
                        r = web.get("https://www.youtube.com/results", params = params)
                        soup = BeautifulSoup(r.content, "html.parser")
                        ret_ = "╔══[ ผลการค้นหา ]"
                        datas = []
                        for data in soup.select(".yt-lockup-title > a[title]"):
                            if "&lists" not in data["href"]:
                                datas.append(data)
                        for data in datas:
                            ret_ += "\n╠══[ {} ]".format(str(data["title"]))
                            ret_ += "\n╠ https://www.youtube.com{}".format(str(data["href"]))
                        ret_ += "\n╚══[ จำนวนที่พบ {} ]".format(len(datas))
                        line.sendReplyMessage(msg.id,to, str(ret_))
                        
                elif ".ค้นหาเพลง " in msg.text.lower():
                    sep = text.split(" ")
                    search = text.replace(sep[0] + "เพลง ","")
                    params = {"search_query": search}
                    with requests.session() as web:
                        web.headers["User-Agent"] = random.choice(settings["userAgent"])
                        r = web.get("https://www.youtube.com/results", params = params)
                        soup = BeautifulSoup(r.content, "html.parser")
                        ret_ = "╔══[ ผลการค้นหา ]"
                        datas = []
                        for data in soup.select(".yt-lockup-title > a[title]"):
                            if "&lists" not in data["href"]:
                                datas.append(data)
                        for data in datas:
                            ret_ += "\n╠══[ {} ]".format(str(data["title"]))
                            ret_ += "\n╠ https://www.youtube.com{}".format(str(data["href"]))
                        ret_ += "\n╚══[ จำนวนที่พบ {} ]".format(len(datas))
                        line.sendReplyMessage(msg.id,to, str(ret_))

                elif msg.text in ["เปิดสแกน"]:
                    try:
                        del RfuCctv['point'][msg.to]
                        del RfuCctv['sidermem'][msg.to]
                        del RfuCctv['cyduk'][msg.to]
                    except:
                        pass
                    RfuCctv['point'][msg.to] = msg.id
                    RfuCctv['sidermem'][msg.to] = ""
                    RfuCctv['cyduk'][msg.to]=True
                    line.sendReplyMessage(msg.id,to,"เปิดระบบแสกนคนอ่านอัตโนมัติ")
                elif msg.text in ["ปิดสแกน"]:
                    if msg.to in RfuCctv['point']:
                        RfuCctv['cyduk'][msg.to]=False
                        line.sendText(msg.to, RfuCctv['sidermem'][msg.to])
                    else:
                        line.sendReplyMessage(msg.id,msg.to, "ปิดระบบแสกนคนอ่านแล้ว")

                elif text.lower() == 'ปิดเซล':
                    line.sendMessage(receiver, 'หยุดการทำงานเซลบอทเรียบร้อย')
                    print ("Selfbot Off")
                    exit(1)
                elif text.lower() == "ลบแชท":
                        if msg._from in lineMID:
                            try:
                                line.removeAllMessages(op.param2)
                                line.sendReplyMessage(msg.id,to,"ลบแชทเรียบร้อย")
                            except:
                                pass
                                print ("ลบแชท")
                elif text.lower() == 'เพื่อน':
                    contactlist = line.getAllContactIds()
                    kontak = line.getContacts(contactlist)
                    num=1
                    msgs="🎎รายชื่อเพื่อนทั้งหมด🎎"
                    for ids in kontak:
                        msgs+="\n[%i] %s" % (num, ids.displayName)
                        num=(num+1)
                    msgs+="\n🎎รายชื่อเพื่อนทั้งหมด🎎\n\nมีดังต่อไปนี้ : %i" % len(kontak)
                    line.sendReplyMessage(msg.id,to, msgs)

                elif msg.text in ["เช็คบล็อค"]: 
                    blockedlist = line.getBlockedContactIds()
                    kontak = line.getContacts(blockedlist)
                    num=1
                    msgs="═════ไม่มีรายการบัญชีที่ถูกบล็อค═════"
                    for ids in kontak:
                        msgs+="\n[%i] %s" % (num, ids.displayName)
                        num=(num+1)
                    msgs+="\n════════รายการบัญชีที่ถูกบล็อค════════\n\nTotal Blocked : %i" % len(kontak)
                    line.sendReplyMessage(msg.id,receiver, msgs)

                elif msg.text in ["ไอดีเพื่อน"]: 
                    gruplist = line.getAllContactIds()
                    kontak = line.getContacts(gruplist)
                    num=1
                    msgs="═════════รายการไอดีเพื่อน═════════"
                    for ids in kontak:
                        msgs+="\n[%i] %s" % (num, ids.mid)
                        num=(num+1)
                    msgs+="\n═════════รายการ ไอดีเพื่อน═════════\n\nTotal Friend : %i" % len(kontak)
                    line.sendReplyMessage(msg.id,receiver, msgs)

                elif msg.text.lower() == 'gurl':
                	if msg.toType == 2:
                         g = line.getGroup(receiver)
                         line.updateGroup(g)
                         gurl = line.reissueGroupTicket(receiver)
                         line.sendReplyMessage(msg.id,receiver,"╔══════════════┓\n╠❂line://ti/g/" + gurl + "\n╠\n╠❂Link Groupnya Tanpa Buka Qr\n╚══════════════┛")
                elif msg.text in ["sayonara"]:
                    if msg.toType == 2:
                        ginfo = line.getGroup(receiver)
                        try:
                            line.leaveGroup(receiver)
                        except:
                            pass
                elif text.lower() == "ถอนกำลัง":
                    if msg._from in Family:
                        line.leaveGroup(msg.to)
                        print ("Selfbot Leave")

                elif text.lower() == "ออกทุกกลุ่ม":
                    if msg._from in Family:
                        gid = line.getGroupIdsJoined()
                        for i in gid:
                            line.leaveGroup(i)
                            print ("ออกทุกกลุ่ม")
                elif msg.text == "เว็บโป๊":
                	line.sendReplyMessage(msg.id,receiver,">nekopoi.host\n>sexvideobokep.com\n>memek.com\n>pornktube.com\n>faketaxi.com\n>videojorok.com\n>watchmygf.mobi\n>xnxx.com\n>pornhd.com\n>xvideos.com\n>vidz7.com\n>m.xhamster.com\n>xxmovies.pro\n>youporn.com\n>pornhub.com\n>youjizz.com\n>thumzilla.com\n>anyporn.com\n>brazzers.com\n>redtube.com\n>youporn.com")
                elif msg.text == "ประกาศ":
                	line.sendReplyMessage(msg.id,to,str(settings["message1"]))
                elif msg.text.lower() == 'ดึงแอด':
                	if msg.toType == 2:                
                           ginfo = line.getGroup(receiver)
                           try:
                               gcmid = ginfo.creator.mid
                           except:
                               gcmid = "Error"
                           if settings["lang"] == "JP":
                               line.inviteIntoGroup(receiver,[gcmid])
                               line.sendReplyMessage(msg.id,receiver, "Type👉 Invite Pembuat Group Succes")
                           else:
                               line.inviteIntoGroup(receiver,[gcmid])
                               line.sendReplyMessage(msg.id,receiver, "Pembuat Group Sudah di dalam")

                elif msg.text in [".ไม่รับเชิญ"]:
                    if msg.toType == 2:
                        ginfo = line.getGroup(receiver)
                        try:
                            line.leaveGroup(receiver)							
                        except:
                            pass
                elif msg.text in [".เช็คไอดี"]: 
                    gruplist = line.getAllContactIds()
                    kontak = line.getContacts(gruplist)
                    num=1
                    msgs="™❍✯͜͡тєαмвσтмυѕι¢✯͜͡❂➣"
                    for ids in kontak:
                        msgs+="\n[%i] %s" % (num, ids.mid)
                        num=(num+1)
                    msgs+="\nจำนวน  %i" % len(kontak)
                    line.sendReplyMessage(msg.id,receiver, msgs)
                    
                elif msg.text in ["เปิดแทคเจ็บ"]:
                    settings["kickMention"] = True
                    line.sendReplyMessage(msg.id,to,"เปิดระบบเตะคนแท็ก")
                
                elif msg.text in ["ปิดแทคเจ็บ"]:
                    settings["kickMention"] = False
                    line.sendReplyMessage(msg.id,to,"ปิดระบบเตะคนแท็ก")
                    
                elif msg.text in ["เปิดแทค","Tag on"]:
                        settings['detectMention'] = True
                        line.sendReplyMessage(msg.id,to,"เปิดแสดงข้อความแทค")
                
                elif msg.text in ["ปิดแทค","Tag off"]:
                        settings['detectMention'] = False
                        line.sendReplyMessage(msg.id,to,"ปิดแสดงข้อความแทค")

                elif msg.text in ["เปิดแทค1"]:
                    settings["potoMention"] = True
                    line.sendReplyMessage(msg.id,to,"เปิดแสดงภาพคนแทค")
                
                elif msg.text in ["ปิดแทค1"]:
                    settings["potoMention"] = False
                    line.sendReplyMessage(msg.id,to,"ปิดแสดงภาพคนแทค")
                    
                elif msg.text in [".เปิดแทค2"]:
                    settings["delayMention"] = True
                    line.sendReplyMessage(msg.id,to,"เปิดระบบแทคกลับคนแทค")
                
                elif msg.text in [".ปิดแทค2"]:
                    settings["delayMention"] = False
                    line.sendReplyMessage(msg.id,to,"ปิดระบบแทคกลับคนแทค")
                elif msg.text.lower() == ".เปิดแทคแชท":
                    settings["detectMentionPM"] = True
                    line.sendReplyMessage(msg.id,to,"เปิดแทคแชทเรียบร้อยครับ")
                elif msg.text.lower() == ".ปิดแทคแชท":
                    settings["detectMentionPM"] = False
                    line.sendReplyMessage(msg.id,to,"ปิดแทคแชทเรียบร้อยครับ")
                elif msg.text.lower().startswith("ตั้งแทคแชท: "):
                    text = msg.text.lower().replace("ตั้งแทคแชท: ","")
                    settings["pmMessage"] = text
                    line.sendReplyMessage(msg.id,to, "คำแทคแชท สต คือ : {}".format(str(settings["pmMessage"])))
                elif msg.text.lower().startswith("setrespongroup: "):
                    text = msg.text.lower().replace("setrespongroup: ","")
                    settings["respMessage"] = text
                    line.sendReplyMessage(msg.id,to, "Success Update Response Group to : {}".format(str(settings["respMessage"])))
                    
                elif msg.text in ["เปิดตรวจสอบ"]:
                    settings["Aip"] = True
                    line.sendReplyMessage(msg.id,to,"เปิดระบบตรวจสอบคำหยาบกับบอทบิน  ^ω^")
                
                elif msg.text in ["ปิดตรวจสอบ"]:
                    settings["Aip"] = False
                    line.sendReplyMessage(msg.id,to,"ปิดระบบตรวจสอบคำหยาบกับบอทบินแล้วʕ•ﻌ•ʔ")
                    
                elif msg.text in ["เปิดพูด"]:
                    settings["Api"] = True
                    line.sendReplyMessage(msg.id,to,"เปิดระบบApiข้อความ")
                
                elif msg.text in ["ปิดพูด"]:
                    settings["Api"] = False
                    line.sendReplyMessage(msg.id,to,"ปิดระบบApiข้อความแล้ว")
                    
                elif 'ตั้งแอด: ' in msg.text:
                  if msg._from in admin:
                     spl = msg.text.replace('ตั้งแอด: ','')
                     if spl in [""," ","\n",None]:
                         line.sendReplyMessage(msg.id,to, "ตั้งข้อความเรียบร้อย")
                     else:
                         settings["message"] = spl
                         line.sendReplyMessage(msg.id,to, "™❍✯͜͡тєαмвσтмυѕι¢✯͜͡❂➣\n👇ตั้งข้อความตอบโต้เมื่อมีคนแอดแล้ว ดังนี้👇\n\n👉{}".format(str(spl)))
                         
                elif '.คอมเม้น: ' in msg.text:
                  if msg._from in admin:
                     spl = msg.text.replace('.คอมเม้น: ','')
                     if spl in [""," ","\n",None]:
                         line.sendReplyMessage(msg.id,to, "ตั้งข้อความเรียบร้อย")
                     else:
                         settings["comment"] = spl
                         line.sendReplyMessage(msg.id,to, "™❍✯͜͡тєαмвσтмυѕι¢✯͜͡❂➣\n👇ตั้งข้อความคอมเม้นของคุณแล้ว ดังนี้👇\n\n👉{}".format(str(spl))) 
                    
                elif 'ตั้งแทค ' in msg.text:
                  if msg._from in admin:
                     spl = msg.text.replace('ตั้งแทค ','')
                     if spl in [""," ","\n",None]:
                         line.sendReplyMessage(msg.id,to, "ตั้งข้อความเรียบร้อย")
                     else:
                         settings["Respontag"] = spl
                         line.sendReplyMessage(msg.id,to, "™❍✯͜͡тєαмвσтмυѕι¢✯͜͡❂➣\n👇ตั้งข้อความตอบโต้เมื่อมีคนแทคแล้ว👇\n\n👉{}".format(str(spl)))
                         
                elif 'ทักเตะ ' in msg.text:
                  if msg._from in admin:
                     spl = msg.text.replace('ทักเตะ ','')
                     if spl in [""," ","\n",None]:
                         line.sendReplyMessage(msg.id,to, "ตั้งข้อความคนลบสมาชิกเรียบร้อย")
                     else:
                          settings["kick"] = spl
                          line.sendReplyMessage(msg.id,to, "™❍✯͜͡тєαмвσтмυѕι¢✯͜͡❂➣\nตั้งค่าข้อความเมื่อมีคนลบสมาชิกแล้ว\nดังนี้👇\n\n👉{}".format(str(spl)))

                elif 'ทักออก ' in msg.text:
                  if msg._from in admin:
                     spl = msg.text.replace('ทักออก ','')
                     if spl in [""," ","\n",None]:
                         line.sendReplyMessage(msg.id,to, "ตั้งข้อความคนออกเรียบร้อย")
                     else:
                          settings["bye"] = spl
                          line.sendReplyMessage(msg.id,to, "™❍✯͜͡тєαмвσтмυѕι¢✯͜͡❂➣\nตั้งค่าข้อความเมื่อมีคนออกจากกลุ่มแล้ว\nดังนี้👇\n\n👉{}".format(str(spl)))

                elif 'ทักเข้า ' in msg.text:
                  if msg._from in admin:
                     spl = msg.text.replace('ทักเข้า ','')
                     if spl in [""," ","\n",None]:
                         line.sendReplyMessage(msg.id,to, "ตั้งข้อความคนเข้าเรียบร้อยแล้ว")
                     else:
                          settings["welcome"] = spl
                          line.sendReplyMessage(msg.id,to, "™❍✯͜͡тєαмвσтмυѕι¢✯͜͡❂➣\nตั้งค่าข้อความเมื่อมีคนเข้ากลุ่มแล้ว\nดังนี้👇\n\n👉{}".format(str(spl)))
                elif msg.text.lower() == "เปิดกันติ๊ก":
                        settings["sticker"] = True
                        line.sendMessage(to,"เปิดเตะคนรันสติกเกอรแล้ว")
                elif msg.text.lower() == "ปิดกันติ๊ก":
                        settings["sticker"] = False
                        line.sendMessage(to,"ปิดเตะคนรันสติกเกอรแล้ว")
                elif msg.text.lower() == "sleepmode":
                    if settings["replyPesan"] is not None:
                        line.sendReplyMessage(msg.id,to,"Your Sleepmode is : " + str(settings["replyPesan"]))
                        msgSticker = settings["messageSticker"]["listSticker"]["sleepSticker"]
                        if msgSticker != None:
                            sid = msgSticker["STKID"]
                            spkg = msgSticker["STKPKGID"]
                            sver = msgSticker["STKVER"]
                            sendSticker(to, sver, spkg, sid)
                    else:
                        line.sendReplyMessage(msg.id,to,"My Sleepmode : No messages are set")
                elif msg.text.lower() == "addsleepmodesticker":
                    settings["messageSticker"]["addStatus"] = True
                    settings["messageSticker"]["addName"] = "sleepSticker"
                    line.sendReplyMessage(msg.id,to, "โปรดส่งสติ๊กเกอร์ที่คุณต้องการเพิ่ม")
                elif msg.text.lower() == "delsleepmodesticker":
                    settings["messageSticker"]["listSticker"]["sleepSticker"] = None
                    line.sendReplyMessage(msg.id,to, "ลบรายการสติ๊กเกอร์เรียบร้อย")
                elif msg.text.lower().startswith("setsleepmode: "):
                    text_ = msg.text.lower().replace("setsleepmode:", "")
                    try:
                        settings["replyPesan"] = text_
                        line.sendReplyMessage(msg.id,to,"Sleep mode changed to : " + text_)
                    except:
                        line.sendReplyMessage(msg.id,to,"SleepMode \nFailed to replace message")
                elif msg.text.lower() == "ติ๊กคนออก":
                        msgSticker = settings["messageSticker"]["listSticker"]["leaveSticker"]
                        if msgSticker != None:
                            sid = msgSticker["STKID"]
                            spkg = msgSticker["STKPKGID"]
                            sver = msgSticker["STKVER"]
                            sendSticker(to, sver, spkg, sid)
                elif msg.text.lower() == "ตั้งติ๊กคนออก":
                    settings["messageSticker"]["addStatus"] = True
                    settings["messageSticker"]["addName"] = "leaveSticker"
                    line.sendReplyMessage(msg.id,to, "ส่งสติ๊กเกอรที่จะตั้งลงมา")
                elif msg.text.lower() == "ลบติ๊กคนออก":
                    settings["messageSticker"]["listSticker"]["leaveSticker"] = None
                    line.sendReplyMessage(msg.id,to, "ลบสติ๊กเกอรคนเข้าแล้ว")
                elif msg.text.lower() == "ติ๊กคนเตะ":
                        msgSticker = settings["messageSticker"]["listSticker"]["kickSticker"]
                        if msgSticker != None:
                            sid = msgSticker["STKID"]
                            spkg = msgSticker["STKPKGID"]
                            sver = msgSticker["STKVER"]
                            sendSticker(to, sver, spkg, sid)
                elif msg.text.lower() == "ตั้งติ๊กคนเตะ":
                    settings["messageSticker"]["addStatus"] = True
                    settings["messageSticker"]["addName"] = "kickSticker"
                    line.sendReplyMessage(msg.id,to, "ส่งสติกเกอรที่จะตั้งลงมา")
                elif msg.text.lower() == "ลบติ๊กคนเตะ":
                    settings["messageSticker"]["listSticker"]["kickSticker"] = None
                    line.sendReplyMessage(msg.id,to, "ลบสติกเกอรคนเตะแล้ว")
                elif msg.text.lower() == "ติ๊กคนเข้า":
                        msgSticker = settings["messageSticker"]["listSticker"]["welcomeSticker"]
                        if msgSticker != None:
                            sid = msgSticker["STKID"]
                            spkg = msgSticker["STKPKGID"]
                            sver = msgSticker["STKVER"]
                            sendSticker(to, sver, spkg, sid)
                elif msg.text.lower() == "ตั้งติ๊กคนเข้า":
                    settings["messageSticker"]["addStatus"] = True
                    settings["messageSticker"]["addName"] = "welcomeSticker"
                    line.sendReplyMessage(msg.id,to, "ส่งสติกเกอรที่จะตั้งลงมา")
                elif msg.text.lower() == "ลบติ๊กคนเข้า":
                    settings["messageSticker"]["listSticker"]["welcomeSticker"] = None
                    line.sendReplyMessage(msg.id,to, "ลบสติกเกอรคนเข้าแล้ว")
                elif msg.text.lower()== "ติ๊กคนแอด":
                        msgSticker = settings["messageSticker"]["listSticker"]["addSticker"]
                        if msgSticker != None:
                            sid = msgSticker["STKID"]
                            spkg = msgSticker["STKPKGID"]
                            sver = msgSticker["STKVER"]
                            sendSticker(to, sver, spkg, sid)
                elif msg.text.lower()== "ตั้งติ๊กคนแอด":
                    settings["messageSticker"]["addStatus"] = True
                    settings["messageSticker"]["addName"] = "addSticker"
                    line.sendReplyMessage(msg.id,to, "ส่งสติกเกอรที่จะใช่ลงมา")
                elif msg.text.lower() == "ลบติ๊กคนแอด":
                    settings["messageSticker"]["listSticker"]["addSticker"] = None
                    line.sendReplyMessage(msg.id,to, "ลบสติ๊กเกอร์คนแอดแล้ว")
                elif msg.text.lower() == "ติ๊กคนแทค":
                        msgSticker = settings["messageSticker"]["listSticker"]["responSticker"]
                        if msgSticker != None:
                            sid = msgSticker["STKID"]
                            spkg = msgSticker["STKPKGID"]
                            sver = msgSticker["STKVER"]
                            sendSticker(to, sver, spkg, sid)
                elif msg.text.lower() == "ตั้งติ๊กคนแทค":
                    settings["messageSticker"]["addStatus"] = True
                    settings["messageSticker"]["addName"] = "responSticker"
                    line.sendReplyMessage(msg.id,to, "ส่งสติ๊กเกอร์ที่จะใช่ลงมา")
                elif msg.text.lower() == "ลบติ๊กคนแทค":
                    settings["messageSticker"]["listSticker"]["responSticker"] = None
                    line.sendReplyMessage(msg.id,to, "ลบสติกเกอรคนแทคแล้ว")
                elif msg.text.lower() == "ตั้งติ๊กคนแอบ":
                    settings["messageSticker"]["addStatus"] = True
                    settings["messageSticker"]["addName"] = "readerSticker"
                    line.sendReplyMessage(msg.id,to, "ส่งสติ๊กเกอรที่จะใช่ลงมา")
                elif msg.text.lower() == "ลบติ๊กคนแอบ":
                    settings["messageSticker"]["listSticker"]["readerSticker"] = None
                    line.sendReplyMessage(msg.id,to, "ลบสติ๊กเกอรคนแอบอ่านแล้ว")
                elif msg.text.lower() == "ติ๊กคนแอบ":
                        msgSticker = settings["messageSticker"]["listSticker"]["readerSticker"]
                        if msgSticker != None:
                            sid = msgSticker["STKID"]
                            spkg = msgSticker["STKPKGID"]
                            sver = msgSticker["STKVER"]
                            sendSticker(to, sver, spkg, sid)
                elif msg.text.lower() == "เผ่น":
                        msgSticker = "line://app/1602687308-DgedGk9A?type=sticker&stk=anim"
                        if msgSticker != None:
                            sid = msgSticker["52114140"]
                            spkg = msgSticker["11539"]
                            sver = msgSticker["1"]
                            sendSticker(to, sver, spkg, sid)
                elif 'ตั้งเข้า: ' in msg.text:
                   if msg._from in admin:
                      spl = msg.text.replace('ตั้งเข้า: ','')
                      if spl in [""," ","\n",None]:
                          line.sendReplyMessage(msg.id,to, "เกิดข้อผิดพลาด")
                      else:
                          settings["welcome"] = spl
                          line.sendReplyMessage(msg.id,to, "「ข้อความต้อนรับแบบแยกห้อง」\nเปลี่ยนข้อความเป็น :\n「{}」".format(str(spl)))
                                  
                elif 'ตั้งออก: ' in msg.text:
                   if msg._from in admin:
                      spl = msg.text.replace('ตั้งออก: ','')
                      if spl in [""," ","\n",None]:
                          line.sendReplyMessage(msg.id,to, "เกิดข้อผิดพลาด")
                      else:
                          settings["leave"] = spl
                          line.sendReplyMessage(msg.id,to, "「ข้อความออกกลุ่มแบบแยกห้อง」\nเปลี่ยนข้อความเป็น :\n「{}」".format(str(spl)))
                elif msg.text.lower().startswith("textig "):
                    sep = msg.text.split(" ")
                    textnya = msg.text.replace(sep[0] + " ","")
                    urlnya = "http://chart.apis.google.com/chart?chs=480x80&cht=p3&chtt=" + textnya + "&chts=FFFFFF,70&chf=bg,s,000000"
                    line.sendImageWithURL(msg.to, urlnya)
                elif msg.text.lower().startswith("เขียน"):
                        sep = msg.text.split(" ")
                        textnya = msg.text.replace(sep[0] + " ","")
                        path = "http://chart.apis.google.com/chart?chs=480x80&cht=p3&chtt=" + textnya + "&chts=FFFFFF,70&chf=bg,s,000000"
                        line.sendImageWithURL(msg.to,path)
                elif msg.text in ["ดึง"]:
                        settings["winvite"] = True
                        line.sendReplyMessage(msg.id,to,"กรุณาส่ง คท คนที่จะดึงเข้ากลุ่ม")                            
                elif text.lower() == 'ยกเชิญ':
                    if msg._from in admin:                            
                        if msg.toType == 2:
                            group = line.getGroup(receiver)
                            gMembMids = [contact.mid for contact in group.invitee]
                            k = len(gMembMids)//30
                            line.sendReplyMessage(msg.id,to,"👉 สมาชิคค้างเชิญจำนวน {} คน👈 \n\nโปรดรอสักครู่...".format(str(len(gMembMids))))
                            num=1
                            for i in range(k+1):
                                for j in gMembMids[i*30 : (i+1)*30]:
                                    time.sleep(random.uniform(0.5,0.4))
                                    line.cancelGroupInvitation(msg.to,[j])
                                    print ("[Command] "+str(num)+" => "+str(len(gMembMids))+" cancel members")
                                    num = num+1
                                line.sendMessage(receiver,"พัก 10-15 วินาที🕛แล้วจะยกค้างเชิญต่ออีก 30 คนจนกว่าจะหมด")
                                time.sleep(random.uniform(15,10))
                            line.sendMessage(receiver,"👉 ยกค้างเชิญ จำนวน {} คน เรียบร้อยแล้ว👏".format(str(len(gMembMids))))
                            time.sleep(random.uniform(0.95,1))
                            #line.sendMessage(receiver, None, contentMetadata={"STKID": "52114113","STKPKGID": "11539","STKVER": "1" }, contentType=7)
                            gname = line.getGroup(receiver).name
                            line.sendMessage(receiver,"👉 ยกค้างเชิญ >> "+gname+"  <<👏 \n จำนวน {} คน เรียบร้อยแล้ว👏\n".format(str(len(gMembMids))))
                            time.sleep(random.uniform(0.95,1))
                            #line.leaveGroup(receiver)                                								
                        line.sendReplyMessage(msg.id,receiver,"[ไม่มีคนให้ผมยกเชิญแล้ว/n🔝тєαмвσтмυѕι¢🔝]")
                        #line.sendReplyMessage(msg.id,receiver, None, contentMetadata={"STKID": "52114113","STKPKGID": "11539","STKVER": "1" }, contentType=7)
                        #line.leaveGroup(receiver)
                            
                elif msg.text.lower() == ".บอทยก":
                    if msg.toType == 2:
                        group = line.getGroup(msg.to)
                        gMembMids = [contact.mid for contact in group.invitee]
                        for i in gMembMids:
                            line.cancelGroupInvitation(msg.to,[i])
#=============COMMAND KICKER===========================#
                elif msg.text in ["ดำ"]:
                  if msg._from in admin: 
                    settings["wblacklist"] = True
                    line.sendReplyMessage(msg.id,to,"กรุณาส่งคอทแทค")
                elif msg.text in ["ขาว"]:
                  if msg._from in admin: 
                    settings["dblacklist"] = True
                    line.sendReplyMessage(msg.id,to,"กรุณาส่งคอทแทค")
                elif msg.text in ["ล้างดำ"]:
                    settings["blacklist"] = {}
                    line.sendReplyMessage(msg.id,to,"ทำการลบัญชีดำทั้งหมดเรียร้อย")
                    print ("Clear Ban")
                elif msg.text.lower() == "คทบล็อค":
                    if msg._from in lineMID:
                        blockedlist = line.getBlockedContactIds()
                        if blockedlist == []:
                            line.sendReplyMessage(msg.id,to, "คุณยังไม่ได้บล็อคใคร!")
                        else:
                            for kontak in blockedlist:
                                line.sendReplyMessage(msg.id,to, text=None, contentMetadata={'mid': kontak}, contentType=13)
                elif msg.text.lower() == "คทดำ":
                    if msg._from in lineMID:
                        if settings["blacklist"] == []:
                            line.sendMessage(to, "Nothing boss")
                        else:
                            for bl in settings["blacklist"]:
                                line.sendReplyMessage(msg.id,to, text=None, contentMetadata={'mid': bl}, contentType=13)
                elif '.ลาก่อย' in text.lower():
                       targets = []
                       key = eval(msg.contentMetadata["MENTION"])
                       key["MENTIONEES"] [0] ["M"]
                       for x in key["MENTIONEES"]:
                           targets.append(x["M"])
                       for target in targets:
                           try:
                               line.kickoutFromGroup(msg.to,[target])      
                               print ("Rfu kick User")
                           except:
                               line.sendReplyMessage(msg.id,to,"Limit kaka 😫")

                elif '.สอย' in text.lower():
                       targets = []
                       key = eval(msg.contentMetadata["MENTION"])
                       key["MENTIONEES"] [0] ["M"]
                       for x in key["MENTIONEES"]:
                           targets.append(x["M"])
                       for target in targets:
                           try:
                               line.kickoutFromGroup(msg.to,[target])             
                               print ("Sb Kick User")
                           except:
                               line.sendReplyMessage(msg.id,to,"Limit kaka 😫")                               

                elif '.เชิญ' in text.lower():
                       targets = []
                       key = eval(msg.contentMetadata["MENTION"])
                       key["MENTIONEES"] [0] ["M"]
                       for x in key["MENTIONEES"]:
                           targets.append(x["M"])
                       for target in targets:
                           try:
                               line.inviteIntoGroup(msg.to,[target])
                               line.sendReplyMessage(msg.id,receiver, "Type👉 Invite Succes")
                           except:
                               line.sendReplyMessage(msg.id,to,"Type👉 Limit Invite")
                elif ".บล็อค @" in msg.text:
                    if msg.toType == 2:
                        print ("[block] OK")
                        _name = msg.text.replace(".บล็อค @","")
                        _nametarget = _name.rstrip('  ')
                        gs = line.getGroup(msg.to)
                        targets = []
                        for g in gs.members:
                            if _nametarget == g.displayName:
                               targets.append(g.mid)
                        if targets == []:
                            line.sendReplyMessage(msg.id,to, "Not Found...")
                        else:
                            for target in targets:
                                try:
                                   line.blockContact(target)
                                   line.sendReplyMessage(msg.id,to, "บล็อคคุณ ✈" + str(g.displayName) + " แล้ว")
                                except Exception as e:
                                   print (e)
                elif msg.text.lower() == 'บล็อค':
                    blockedlist = line.getBlockedContactIds()
                    sendMessage(msg.to, "Please wait...")
                    kontak = line.getContacts(blockedlist)
                    num=1
                    msgs="User Blocked List\n"
                    for ids in kontak:
                        msgs+="\n%i. %s" % (num, ids.displayName)
                        num=(num+1)
                        msgs+="\n\nTotal %i blocked user(s)" % len(kontak)
                        line.sendReplyMessage(msg.id,to, msgs)
                elif "ซามู" in msg.text:
                	if msg.toType == 2:
                         _name = msg.text.replace("ซามู","")
                         gs = line.getGroup(receiver)
                         line.sendMessage(receiver,"ขอให้ไปสู่สติน่ะคับ")
                         targets = []
                         for g in gs.members:
                             if _name in g.displayName:
                                 targets.append(g.mid)
                         if targets == []:
                             line.sendMessage(receiver,"Not found.")
                         else:
                             for target in targets:
                             	if not target in Rfu:
                                     try:
                                         klist=[line]
                                         kicker=random.choice(klist)
                                         kicker.kickoutFromGroup(receiver,[target])
                                         print((receiver,[g.mid]))
                                     except:
                                         line.sendMessage(receiver,"Group cleanse")
                                         print ("Cleanse Group")

                elif msg.text in ["ไล่ดำ"]:
                	if msg.toType == 2:
                         group = line.getGroup(receiver)
                         gMembMids = [contact.mid for contact in group.members]
                         matched_list = []
                         for tag in settings["blacklist"]:
                             matched_list+=[str for str in gMembMids if str == tag]
                         if matched_list == []:
                             line.sendReplyMessage(msg.id,receiver,"Nots in Blacklist")
                         else:
                             for jj in matched_list:
                                 try:
                                     klist=[line]
                                     kicker=random.choice(klist)
                                     kicker.kickoutFromGroup(receiver,[jj])
                                     print((receiver,[jj]))
                                 except:
                                     line.sendReplyMessage(msg.id,receiver,"sorry bl ke cyduk")
                                     print ("Blacklist di Kick")
                elif "ชื่อ: " in text.lower():
                    if msg._from in Family:
                        proses = text.split(":")
                        string = text.replace(proses[0] + ": ","")
                        profile_A = line.getProfile()
                        profile_A.displayName = string
                        line.updateProfile(profile_A)
                        line.sendReplyMessage(msg.id,to,"Update to " + string)
                        print ("Update Name")

                elif "ตัส: " in msg.text.lower():
                    if msg._from in Family:
                        proses = text.split(":")
                        string = text.replace(proses[0] + ": ","")
                        profile_A = line.getProfile()
                        profile_A.statusMessage = string
                        line.updateProfile(profile_A)
                        line.sendReplyMessage(msg.id,to,"Succes Update 👉 " + string)
                        print ("Update Bio Succes")
                elif "กันชื่อ" in msg.text:
                    if msg.to in settings['pname']:
                        line.sendReplyMessage(msg.id,to,"😎เปิดป้องกันชื่อของกลุ่มอยู่แล้ว😎")
                    else:
                        line.sendReplyMessage(msg.id,to,"เปิดป้องกันชื่อของกลุ่มแล้ว😎")
                        settings['pname'][msg.to] = True
                        settings['pro_name'][msg.to] = line.getGroup(msg.to).name
                elif "ปิดกันชื่อ" in msg.text:
                    if msg.to in settings['pname']:
                        line.sendReplyMessage(msg.id,to,"ปิดป้องกันชื่อกลุ่มอยู่แล้ว")
                        del settings['pname'][msg.to]
                    else:
                        line.sendReplyMessage(msg.id,to,"ปิดป้องกันชื่อกลุ่มเรียบร้อย")
#=============COMMAND PROTECT=========================#
                elif msg.text.lower() == 'เปิดกัน':
                    if RfuProtect["protect"] == True:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกัน   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกัน   ")
                    else:
                        RfuProtect["protect"] = True
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกัน   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกัน   ")

                elif msg.text.lower() == 'ปิดกัน':
                    if RfuProtect["protect"] == False:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกัน   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกัน   ")
                    else:
                        RfuProtect["protect"] = False
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกัน   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกัน   ")

                elif msg.text.lower() == 'กันยก':
                    if RfuProtect["cancelprotect"] == True:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันยกเลิกเชิญ   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันยกเลิกเชิญ   ")
                    else:
                        RfuProtect["cancelprotect"] = True
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันยกเลิกเชิญ   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันยกเลิกเชิญ   ")

                elif msg.text.lower() == 'ปิดกันยก':
                    if RfuProtect["cancelprotect"] == False:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันยกเลิกเชิญ   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันยกเลิกเชิญ   ")
                    else:
                        RfuProtect["cancelprotect"] = False
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันยกเลิกเชิญ   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันยกเลิกเชิญ   ")

                elif msg.text.lower() == 'กันเชิญ':
                    if RfuProtect["inviteprotect"] == True:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันยกเชิญ   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันยกเชิญ   ")
                    else:
                        RfuProtect["inviteprotect"] = True
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันยกเชิญ   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันยกเชิญ   ")

                elif msg.text.lower() == 'ปิดกันเชิญ':
                    if RfuProtect["inviteprotect"] == False:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันยกเชิญ   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันยกเชิญ   ")
                    else:
                        RfuProtect["inviteprotect"] = False
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันยกเชิญ   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันยกเชิญ   ")

                elif msg.text.lower() == 'กันลิ้ง':
                    if RfuProtect["linkprotect"] == True:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันลิ้ง   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันลิ้ง   ")
                    else:
                        RfuProtect["linkprotect"] = True
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันลิ้ง   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันลิ้ง   ")

                elif msg.text.lower() == 'ปิดกันลิ้ง':
                    if RfuProtect["linkprotect"] == False:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันลิ้ง   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันลิ้ง   ")
                    else:
                        RfuProtect["linkprotect"] = False
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันลิ้ง   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันลิ้ง   ")

                elif msg.text.lower() == 'กันกลุ่ม':
                    if RfuProtect["Protectguest"] == True:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันสมาชิก   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันสมาชิก   ")
                    else:
                        RfuProtect["Protectguest"] = True
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันสมาชิก   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันสมาชิก   ")

                elif msg.text.lower() == 'ปิดกันกลุ่ม':
                    if RfuProtect["Protectguest"] == False:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันสมาชิก   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันสมาชิก   ")
                    else:
                        RfuProtect["Protectguest"] = False
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันสมาชิก   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันสมาชิก   ")

                elif msg.text.lower() == 'กันเข้า':
                    if RfuProtect["Protectjoin"] == True:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันคนเข้า   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันคนเข้า   ")
                    else:
                        RfuProtect["Protectjoin"] = True
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันคนเข้า   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันคนเข้า   ")

                elif msg.text.lower() == 'ปิดกันเข้า':
                    if RfuProtect["Protectjoin"] == False:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันคนเข้า   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันคนเข้า   ")
                    else:
                        RfuProtect["Protectjoin"] = False
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันคนเข้า   ")
                        else:
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันคนเข้า   ")

                elif msg.text.lower() == 'เปิดหมด':
                    if RfuProtect["inviteprotect"] == True:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"✰เปิดป้องกันทั้งหมด✰")
                        else:
                            line.sendReplyMessage(msg.id,to,"✰เปิดป้องกันทั้งหมด✰")
                    else:
                        RfuProtect["inviteprotect"] = True
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันเชิญ")
                    if RfuProtect["cancelprotect"] == True:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันยกเลิกเชิญ")
                        else:
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันยกเลิกเชิญ")
                    else:
                        RfuProtect["cancelprotect"] = True
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันยกเลิกเชิญ")
                    if RfuProtect["protect"] == True:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันยกเลิกเชิญ")
                        else:
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันยกเลิกเชิญ")
                    else:
                        RfuProtect["protect"] = True
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันเตะ")
                        else:
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันเตะ")
                    if RfuProtect["linkprotect"] == True:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันลิ้ง")
                        else:
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันลิ้ง")
                    else:
                        RfuProtect["linkprotect"] = True
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันลิ้ง")
                        else:
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันลิ้ง")
                    if RfuProtect["Protectguest"] == True:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันกลุ่ม")
                        else:
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันกลุ่ม")
                    else:
                        RfuProtect["Protectguest"] = True
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันกลุ่ม")
                        else:
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันกลุ่ม")
                    if RfuProtect["Protectjoin"] == True:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันบุคคลภายน้อกเข้ากลุ่ม")
                        else:
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันบุคคลภายน้อกเข้ากลุ่ม")
                    else:
                        RfuProtect["Protectjoin"] = True
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันบุคคลภายน้อกเข้ากลุ่ม")
                        else:
                            line.sendReplyMessage(msg.id,to,"เปิดป้องกันบุคคลภายน้อกเข้ากลุ่ม")

                elif msg.text.lower() == 'ปิดหมด':
                    if RfuProtect["inviteprotect"] == False:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"✰ปิดป้องกันทั้งหมด✰")
                        else:
                            line.sendReplyMessage(msg.id,to,"✰ปิดป้องกันทั้งหมด✰")
                    else:
                        RfuProtect["inviteprotect"] = False
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันเชิญ")
                    if RfuProtect["cancelprotect"] == False:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันยกเชิญ")
                        else:
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันยกเชิญ")
                    else:
                        RfuProtect["cancelprotect"] = False
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันยกเชิญ")
                    if RfuProtect["protect"] == False:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันเตะ")
                        else:
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันเตะ")
                    else:
                        RfuProtect["protect"] = False
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันเตะ")
                        else:
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันเตะ")
                    if RfuProtect["linkprotect"] == False:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันเปิดลิ้ง")
                        else:
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันเปิดลิ้ง")
                    else:
                        RfuProtect["linkprotect"] = False
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันเปิดลิ้ง")
                        else:
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันเปิดลิ้ง")
                    if RfuProtect["Protectguest"] == False:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันกลุ่ม")
                        else:
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันกลุ่ม")
                    else:
                        RfuProtect["Protectguest"] = False
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันกลุ่ม")
                        else:
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันกลุ่ม")
                    if RfuProtect["Protectjoin"] == False:
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันบุคคลภายน้อกเข้ากลุ่ม")
                        else:
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันบุคคลภายน้อกเข้ากลุ่ม")
                    else:
                        RfuProtect["Protectjoin"] = False
                        if settings["lang"] == "JP":
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันบุคคลภายน้อกเข้ากลุ่ม")
                        else:
                            line.sendReplyMessage(msg.id,to,"ปิดป้องกันบุคคลภายน้อกเข้ากลุ่ม")

#==============FINNISHING PROTECT========================#
                elif msg.text.lower() == 'เปิดรับแขก':
                        if settings["Wc"] == True:
                            if settings["lang"] == "JP":
                                line.sendReplyMessage(msg.id,to,"เปิดข้อความต้อนรับเมื่อมีสมาชิกเข้ากลุ่ม   ")
                        else:
                            settings["Wc"] = True
                            if settings["lang"] == "JP":
                                line.sendReplyMessage(msg.id,to,"เปิดข้อความต้อนรับเมื่อมีสมาชิกเข้ากลุ่ม   ")
                elif msg.text.lower() == 'ปิดรับแขก':
                        if settings["Wc"] == False:
                            if settings["lang"] == "JP":
                                line.sendReplyMessage(msg.id,to,"ปิดข้อความต้อนรับเมื่อมีสมาชิกเข้ากลุ่ม   ")
                        else:
                            settings["Wc"] = False
                            if settings["lang"] == "JP":
                                line.sendReplyMessage(msg.id,to,"ปิดข้อความต้อนรับเมื่อมีสมาชิกเข้ากลุ่ม   ")
                                
                elif msg.text.lower() == 'เปิดทักเตะ':
                        if settings["Nk"] == True:
                            if settings["lang"] == "JP":
                                line.sendReplyMessage(msg.id,to,"เปิดข้อความแจ้งเตือนเมื่อมีคนลบสมาชิกในกลุ่ม...")
                        else:
                            settings["Nk"] = True
                            if settings["lang"] == "JP":
                                line.sendReplyMessage(msg.id,to,"เปิดข้อความแจ้งเตือนเมื่อมีคนลบสมาชิกในกลุ่ม...")
                                
                elif msg.text.lower() == 'ปิดทักเตะ':
                        if settings["Nk"] == False:
                            if settings["lang"] == "JP":
                                line.sendReplyMessage(msg.id,to,"ปิดข้อความแจ้งเตือนเมื่อมีคนลบสมาชิกในกลุ่มแล้ว..")
                        else:
                            settings["Nk"] = False
                            if settings["lang"] == "JP":
                                line.sendReplyMessage(msg.id,to,"เปิดข้อความแจ้งเตือนเมื่อมีคนลบสมาชิกในกลุ่มแล้ว...")

                elif msg.text.lower() == 'เปิดส่งแขก':
                        if settings["Lv"] == True:
                            if settings["lang"] == "JP":
                                line.sendReplyMessage(msg.id,to,"เปิดข้อความอำลาเมื่อมีสมาชิกออกกลุ่ม   ")
                        else:
                            settings["Lv"] = True
                            if settings["lang"] == "JP":
                                line.sendReplyMessage(msg.id,to,"เปิดข้อความอำลาเมื่อมีสมาชิกออกกลุ่ม   ")
                elif msg.text.lower() == 'ปิดส่งแขก':
                        if settings["Lv"] == False:
                            if settings["lang"] == "JP":
                                line.sendReplyMessage(msg.id,to,"ปิดข้อความอำลาเมื่อมีสมาชิกออกกลุ่ม   ")
                        else:
                            settings["Lv"] = False
                            if settings["lang"] == "JP":
                                line.sendReplyMessage(msg.id,to,"ปิดข้อความอำลาเมื่อมีสมาชิกออกกลุ่ม   ")
                                
                elif msg.text.lower() == 'เปิดคท':
                        if settings["checkContact"] == True:
                            if settings["lang"] == "JP":
                                line.sendReplyMessage(msg.id,to,"เปิดระบบอ่านข้อมูลด้วยคอนแทคไว้อยู่แล้ว ")
                        else:
                            settings["checkContact"] = True
                            if settings["lang"] == "JP":
                                line.sendReplyMessage(msg.id,to,"เปิดระบบอ่านข้อมูลด้วยคอนแทค")
                elif msg.text.lower() == 'ปิดคท':
                        if settings["checkContact"] == False:
                            if settings["lang"] == "JP":
                                line.sendReplyMessage(msg.id,to,"ปิดระบบอ่านข้อมูลด้วยคอนแทคไว้อยู่แล้ว ")
                        else:
                            settings["checkContact"] = False
                            if settings["lang"] == "JP":
                                line.sendReplyMessage(msg.id,to,"ปิดระบบอ่านข้อมูลด้วยคอนแทค")
                elif msg.text.lower() == 'เปิดเช็คโพส':
                        if settings["checkPost"] == True:
                            if settings["lang"] == "JP":
                                line.sendReplyMessage(msg.id,to,"เปิดระบบเช็คโพสบนทามไลน์ไว้อยู่แล้ว " )
                        else:
                            settings["checkPost"] = True
                            if settings["lang"] == "JP":
                                line.sendReplyMessage(msg.id,to,"เปิดระบบเช็คโพสบนทามไลน์ ")
                elif msg.text.lower() == 'ปิดเช็คโพส':
                        if settings["checkPost"] == False:
                            if settings["lang"] == "JP":
                                line.sendReplyMessage(msg.id,to,"ปิดระบบเช็คโพสบนทามไลน์ไว้อยู่แล้ว ")
                        else:
                            settings["checkPost"] = False
                            if settings["lang"] == "JP":
                                line.sendReplyMessage(msg.id,to,"ปิดระบบเช็คโพสบนทามไลน์")
                elif text.lower() == "แปลงโฉม":
                    settings["changePictureProfile"] = True
                    line.sendReplyMessage(msg.id,to, "ส่งรูปภาพลงมาได้เลยครับผม")
                elif text.lower() == "อัพวีดีโอ" and sender == lineMID:
                    settings['changeProfileVideo']['status'] = True
                    settings['changeProfileVideo']['stage'] = 1
                    line.sendReplyMessage(msg.id,to, "「 ส่งวีดีโอลงมาเลยลูกพี่ 」\n•", "\nSend video !")
                elif text.lower() == "เปลี่ยนรูปกลุ่ม":
                    if msg.toType == 2:
                        if to not in settings["changeGroupPicture"]:
                            settings["changeGroupPicture"].append(to)
                        line.sendReplyMessage(msg.id,to, "ส่งรูปภาพลงมาไดเเลยครับผม")
                elif text.lower() == "ดับไฟ":
                        line.sendReplyMessage(msg.id,to, ".God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God .3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God .3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God.3.God")
                        line.sendReplyMessage(msg.id,to, "เข้านอนได้แล้วเด็กๆ")
                elif text.lower() == 'ของขวัญ':
                        line.sendGift(msg.to,'1002077','sticker')
                        line.sendGift(msg.to,'1002077','sticker')
                        line.sendGift(msg.to,'1002077','sticker')
                elif text.lower() == 'ลบรัน':
                    gid = line.getGroupIdsInvited()
                    start = time.time()
                    for i in gid:
                        line.rejectGroupInvitation(i)
                    elapsed_time = time.time() - start
                    line.sendReplyMessage(msg.id,to, "◐.̃◐โดนมาแล้วสิ ลบรันเรียบร้อย◐.̃◐")
                    line.sendReplyMessage(msg.id,to, "ระยะเวลาที่ใช้: %sวินาที" % (elapsed_time))
                    
                elif 'ต้อนรับ ' in msg.text:
                   if msg._from in admin:
                      spl = msg.text.replace('ต้อนรับ ','')
                      if spl == 'เปิด':
                          if msg.to in welcome:
                               msgs = "ข้อความต้อนรับเปิดใช้งานภายในกลุ่มนี้อยู่แล้ว"
                          else:
                               welcome.append(msg.to)
                               ginfo = line.getGroup(msg.to)
                               msgs = "  ข้อความต้อนรับเปิดใช้งาน\nในกลุ่ม 👉: " +str(ginfo.name)
                          line.sendMessage(msg.to, "「тєαмвσтмυѕι¢」\n" + msgs)
                      elif spl == 'ปิด':
                            if msg.to in welcome:
                                 welcome.remove(msg.to)
                                 ginfo = line.getGroup(msg.to)
                                 msgs = "ปิดการต้อนรับแล้ว \nในกลุ่ม : " +str(ginfo.name)
                            else:
                                 msgs = "ปิดการต้อนรับในกลุ่มนี้อยู่แล้ว"
                            line.sendMessage(msg.to, "「тєαмвσтмυѕι¢」\n" + msgs)
                            
                elif 'กันลิ้ง ' in msg.text:
                   if msg._from in admin:
                      spl = msg.text.replace('กันลิ้ง ','')
                      if spl == 'เปิด':
                          if msg.to in protectqr:
                               msgs = "การป้องกันลิ้งกลุ่มถูกทำงานอยู่แล้ว"
                          else:
                               protectqr.append(msg.to)
                               ginfo = line.getGroup(msg.to)
                               msgs = "การป้องกันลิ้งกลุ่มถูกทำงาน\nภายในกลุ่ม : " +str(ginfo.name)
                          line.sendReplyMessage(msg.id,to, "「тєαмвσтмυѕι¢」\n" + msgs)
                      elif spl == 'ปิด':
                            if msg.to in protectqr:
                                 protectqr.remove(msg.to)
                                 ginfo = line.getGroup(msg.to)
                                 msgs = "ปิดการป้องกันลิ้ง\nภายในกลุ่ม : " +str(ginfo.name)
                            else:
                                 msgs = "ปิดการป้องกันลิ้งกลุ่มนี้ไว้อยู่แล้ว"
                            line.sendReplyMessage(msg.id,to, "「Dinonaktifkan」\n" + msgs)

                elif 'กันเตะ ' in msg.text:
                   if msg._from in admin:
                      spl = msg.text.replace('กันเตะ ','')
                      if spl == 'เปิด':
                          if msg.to in protectkick:
                               msgs = "เปิดกันเตะใว้ในกลุ่มนี้อยู่แล้ว"
                          else:
                               protectkick.append(msg.to)
                               ginfo = line.getGroup(msg.to)
                               msgs = "เปิดป้องกันเตะ\nภายในกลุ่ม : " +str(ginfo.name)
                          line.sendReplyMessage(msg.id,to, "「тєαмвσтмυѕι¢」\n" + msgs)
                      elif spl == 'ปิด':
                            if msg.to in protectkick:
                                 protectkick.remove(msg.to)
                                 ginfo = line.getGroup(msg.to)
                                 msgs = "ปิดป้องกันเตะ\nภายในกลุ่ม : " +str(ginfo.name)
                            else:
                                 msgs = "ปิดกันเตะใว้ในกลุ่มนี้อยู่แล้ว"
                            line.sendReplyMessage(msg.id,to, "「тєαмвσтмυѕι¢」\n" + msgs)

                elif 'กันเข้า ' in msg.text:
                   if msg._from in admin:
                      spl = msg.text.replace('กันเข้า ','')
                      if spl == 'เปิด':
                          if msg.to in protectjoin:
                               msgs = "เปิดป้องกันบบุคคลภายนอกเข้ากลุ่มนี้อยู่แล้ว"
                          else:
                               protectjoin.append(msg.to)
                               ginfo = line.getGroup(msg.to)
                               msgs = "เปิดป้องกันบุคคลภายนอกเข้ากลุ่ม\nภายในกลุ่ม  : " +str(ginfo.name)
                          line.sendReplyMessage(msg.id,to, "「тєαмвσтмυѕι¢」\n" + msgs)
                      elif spl == 'ปิด':
                            if msg.to in protectjoin:
                                 protectjoin.remove(msg.to)
                                 ginfo = line.getGroup(msg.to)
                                 msgs = "ปิดป้องกันบุคคลภายนอกเข้ากลุ่ม\nภายในกลุ่ม : " +str(ginfo.name)
                            else:
                                 msgs = "ปิดป้องกันบุคคลภายนอกเข้ากลุ่มนี้ไว้อยู่แล้ว"
                            line.sendReplyMessage(msg.id,to, "「тєαмвσтмυѕι¢」\n" + msgs)

                elif 'กันยก ' in msg.text:
                   if msg._from in admin:
                      spl = msg.text.replace('กันยก ','')
                      if spl == 'เปิด':
                          if msg.to in protectcancel:
                               msgs = "เปิดป้องกันยกเชิญภายในกลุ่มนี้ไว้อยู่แล้ว"
                          else:
                               protectcancel.append(msg.to)
                               ginfo = line.getGroup(msg.to)
                               msgs = "เปิดป้องกันยกเชิญ\nภายในกลุ่ม : " +str(ginfo.name)
                          line.sendReplyMessage(msg.id,to, "「тєαмвσтмυѕι¢」\n" + msgs)
                      elif spl == 'ปิด':
                            if msg.to in protectcancel:
                                 protectcancel.remove(msg.to)
                                 ginfo = line.getGroup(msg.to)
                                 msgs = "ปิดป้องกันยกเชิญ\nภายในกลุ่ม : " +str(ginfo.name)
                            else:
                                 msgs = "ปิดป้องกันยกเชิญภายในกลุ่มนี้ไว้อยู่แล้ว"
                            line.sendReplyMessage(msg.id,to, "「тєαмвσтмυѕι¢」\n" + msgs)

                elif 'Antijs ' in msg.text:
                   if msg._from in admin:
                      spl = msg.text.replace('Antijs ','')
                      if spl == 'on':
                          if msg.to in protectantijs:
                               msgs = "Anti JS sudah aktif"
                          else:
                               protectantijs.append(msg.to)
                               ginfo = line.getGroup(msg.to)
                               msgs = "Anti JS Diaktifkan\nDi Group : " +str(ginfo.name)
                          line.sendReplyMessage(msg.id,to, "「Diaktifkan」\n" + msgs)
                      elif spl == 'off':
                            if msg.to in protectantijs:
                                 protectantijs.remove(msg.to)
                                 ginfo = line.getGroup(msg.to)
                                 msgs = "Anti JS Dinonaktifkan\nDi Group : " +str(ginfo.name)
                            else:
                                 msgs = "Anti JS Sudah Tidak Aktif"
                            line.sendReplyMessage(msg.id,to, "「Dinonaktifkan」\n" + msgs)
                                    
                elif "ลงดำ" in msg.text:
                  if msg._from in Family:
                      if msg.toType == 2:
                           print ("All Banlist")
                           _name = msg.text.replace("ลงดำ","")
                           gs = line.getGroup(msg.to)
                           line.sendReplyMessage(msg.id,to,"☞แบนยกห้องเลยและกัน☜")
                           targets = []
                           for g in gs.members:
                               if _name in g.displayName:
                                    targets.append(g.mid)
                           if targets == []:
                                line.sendReplyMessage(msg.id,to,"Maaf")
                           else:
                               for target in targets:
                                   if not target in Family:
                                       try:
                                           settings["blacklist"][target] = True
                                           f=codecs.open('st2__b.json','w','utf-8')
                                           json.dump(settings["blacklist"], f, sort_keys=True, indent=4,ensure_ascii=False)
                                       except:
                                           line.sendReplyMessage(msg.id,to,"พบข้อผิดพลาดที่ไม่ทราบสาเหตุ")
										   
                elif '.แบน' in text.lower():
                       targets = []
                       key = eval(msg.contentMetadata["MENTION"])
                       key["MENTIONEES"] [0] ["M"]
                       for x in key["MENTIONEES"]:
                           targets.append(x["M"])
                       for target in targets:
                           try:
                               settings["blacklist"][target] = True
                               f=codecs.open('st2__b.json','w','utf-8')
                               json.dump(settings["blacklist"], f, sort_keys=True, indent=4,ensure_ascii=False)
                               line.sendReplyMessage(msg.id,to,"Succes added for the blacklist ")
                               print ("Banned User")
                           except:
                               line.sendReplyMessage(msg.id,to,"Contact Not Found")

                elif 'ล้างแบน' in text.lower():
                       targets = []
                       key = eval(msg.contentMetadata["MENTION"])
                       key["MENTIONEES"] [0] ["M"]
                       for x in key["MENTIONEES"]:
                           targets.append(x["M"])
                       for target in targets:
                           try:
                               del settings["blacklist"][target]
                               f=codecs.open('st2__b.json','w','utf-8')
                               json.dump(settings["blacklist"], f, sort_keys=True, indent=4,ensure_ascii=False)
                               line.sendReplyMessage(msg.id,to,"Succes unban from the blacklist. ")
                               print ("Unbanned User")
                           except:
                               line.sendReplyMessage(msg.id,to,"Contact Not Found")
                
                elif msg.text in ["เช็คดำ"]:
                  if msg._from in Family:
                    if settings["blacklist"] == {}:
                        line.sendReplyMessage(msg.id,to,"☞ไม่พบ คนติดดำ☜") 
                    else:
                        line.sendReplyMessage(msg.id,to,"รายชื่อคนรอบกัด")
                        mc = "Blacklist User\n"
                        for mi_d in settings["blacklist"]:
                            mc += "[✏] " + line.getContact(mi_d).displayName + " \n"
                        line.sendReplyMessage(msg.id,to, mc + "")
                        
                elif "แบนโทร " in text.lower():
                  if settings["selfbot"] == True:
                    if msg._from in admin:
                       key = eval(msg.contentMetadata["MENTION"])
                       key["MENTIONEES"][0]["M"]
                       targets = []
                       for x in key["MENTIONEES"]:
                            targets.append(x["M"])
                       for target in targets:
                               try:
                                   settings["Talkblacklist"][target] = True
                                   line.sendReplyMessage(msg.id,to,"Berhasil menambahkan blacklist")
                               except:
                                   pass

                elif "ลบแบนโทร " in text.lower():
                  if settings["selfbot"] == True:
                    if msg._from in admin:
                       key = eval(msg.contentMetadata["MENTION"])
                       key["MENTIONEES"][0]["M"]
                       targets = []
                       for x in key["MENTIONEES"]:
                            targets.append(x["M"])
                       for target in targets:
                               try:
                                   del settings["Talkblacklist"][target]
                                   line.sendReplyMessage(msg.id,to,"Berhasil menghapus blacklist")
                               except:
                                   pass

                elif text.lower() == "แบนโทร" or text.lower() == 'talkban:on':
                  if settings["selfbot"] == True:
                    if msg._from in admin:
                        settings["Talkwblacklist"] = True
                        line.sendReplyMessage(msg.id,to,"กรุณาส่งคอนแทค...")

                elif text.lower() == "ลบแบนโทร" or text.lower() == 'untalkban:on':
                  if settings["selfbot"] == True:
                    if msg._from in admin:
                        settings["Talkdblacklist"] = True
                        line.sendReplyMessage(msg.id,to,"กรุณาส่งคอนแทค...")

                elif text.lower() == "รายชื่อบอท":
                  if settings["selfbot"] == True:
                    if msg._from in admin:
                        ma = ""
                        a = 0
                        for m_id in Bots:
                            a = a + 1
                            end = '\n'
                            ma += str(a) + ". " +line.getContact(m_id).displayName + "\n"
                        line.sendReplyMessage(msg.id,to,"⏩SAMURAI  bot\n\n"+ma+"\nTotal「%s」 Bots" %(str(len(Bots))))

                elif text.lower() == "รายชื่อแอดมิน":
                   if settings["selfbot"] == True:
                    if msg._from in admin:
                        ma = ""
                        mb = ""
                        mc = ""
                        a = 0
                        b = 0
                        c = 0
                        for m_id in admin:
                            b = b + 1
                            end = '\n'
                            mb += str(b) + ". " +line.getContact(m_id).displayName + "\n"
                        for m_id in staff:
                            c = c + 1
                            end = '\n'
                            mc += str(c) + ". " +line.getContact(m_id).displayName + "\n"
                        line.sendReplyMessage(msg.id,to,"⏩SAMURAI  admin\n\nAdmin:\n"+mb+"\nStaff:\n"+mc+"\nTotal「%s」 SAMURAI" %(str(len(admin)+len(staff))))

                elif text.lower() == "รายชื่อป้องกัน":
                  if settings["selfbot"] == True:
                    if msg._from in admin:
                        ma = ""
                        mb = ""
                        mc = ""
                        md = ""
                        a = 0
                        b = 0
                        c = 0
                        d = 0
                        gid = protectqr
                        for group in gid:
                            a = a + 1
                            end = '\n'
                            ma += str(a) + ". " +line.getGroup(group).name + "\n"
                        gid = protectkick
                        for group in gid:
                            b = b + 1
                            end = '\n'
                            mb += str(b) + ". " +line.getGroup(group).name + "\n"
                        gid = protectjoin
                        for group in gid:
                            d = d + 1
                            end = '\n'
                            md += str(d) + ". " +line.getGroup(group).name + "\n"
                        gid = protectcancel
                        for group in gid:
                            c = c + 1
                            end = '\n'
                            mc += str(c) + ". " +line.getGroup(group).name + "\n"
                        line.sendReplyMessage(msg.id,to,"⏩SAMURAI Fams Protection\n\n⏩PROTECT URL :\n"+ma+"\n⏩PROTECT KICK :\n"+mb+"\n⏩PROTECT JOIN :\n"+md+"\n⏩PROTECT CANCEL:\n"+mc+"\nTotal「%s」Grup yg dijaga" %(str(len(protectqr)+len(protectkick)+len(protectjoin)+len(protectcancel))))

                elif msg.text.lower().startswith("urban "):
                    sep = msg.text.split(" ")
                    judul = msg.text.replace(sep[0] + " ","")
                    url = "http://api.urbandictionary.com/v0/define?term="+str(judul)
                    with requests.session() as s:
                        s.headers["User-Agent"] = random.choice(settings["userAgent"])
                        r = s.get(url)
                        data = r.text
                        data = json.loads(data)
                        y = "[ Result Urban ]"
                        y += "\nTags: "+ data["tags"][0]
                        y += ","+ data["tags"][1]
                        y += ","+ data["tags"][2]
                        y += ","+ data["tags"][3]
                        y += ","+ data["tags"][4]
                        y += ","+ data["tags"][5]
                        y += ","+ data["tags"][6]
                        y += ","+ data["tags"][7]
                        y += "\n[1]\nAuthor: "+str(data["list"][0]["author"])
                        y += "\nWord: "+str(data["list"][0]["word"])
                        y += "\nLink: "+str(data["list"][0]["permalink"])
                        y += "\nDefinition: "+str(data["list"][0]["definition"])
                        y += "\nExample: "+str(data["list"][0]["example"])
                        line.sendMessage(to, str(y))

            elif msg.contentType == 13:
                if settings["checkContact"] == True:
                    try:
                        contact = line.getContact(msg.contentMetadata["mid"])
                        if line != None:
                            cover = line.getProfileCoverURL(msg.contentMetadata["mid"])
                        else:
                            cover = "Tidak dapat masuk di line channel"
                        path = "http://dl.profile.line-cdn.net/{}".format(str(contact.pictureStatus))
                        try:
                            line.sendImageWithURL(to, str(path))
                        except:
                            pass
                        ret_ = "[ รายการทั้งหมดจากการสำรวจด้วย คท ]"
                        ret_ += "\n ชื่อ : {}".format(str(contact.displayName))
                        ret_ += "\n ไอดี : {}".format(str(msg.contentMetadata["mid"]))
                        ret_ += "\n ตัส : {}".format(str(contact.statusMessage))
                        ret_ += "\n รูปโปรไฟล : http://dl.profile.line-cdn.net/{}".format(str(contact.pictureStatus))
                        ret_ += "\n  รูปปก : {}".format(str(cover))
                        ret_ += "\n[ สิ้นสุดการสำรวจ ]"
                        line.sendReplyMessage(msg.id,to, str(ret_))
                    except:
                        line.sendReplyMessage(msg.id,to, "เกิดข้อผิดพลาดในการสำรวจ")
            elif msg.contentType == 1:
                if settings["changePictureProfile"] == True:
                    path = line.downloadObjectMsg(msg_id)
                    settings["changePictureProfile"] = False
                    line.updateProfilePicture(path)
                    line.sendReplyMessage(msg.id,to, "ทำการแปลงโฉมเสร็จเรียบร้อย")
                if msg.toType == 2:
                    if to in settings["changeGroupPicture"]:
                        path = line.downloadObjectMsg(msg_id)
                        settings["changeGroupPicture"].remove(to)
                        line.updateGroupPicture(to, path)
                        line.sendReplyMessage(msg.id,to, "เปลี่ยนรูปภาพกลุ่มเรียบร้อยแล้ว")
            elif msg.contentType == 1:
                if settings['changeProfileVideo']['status'] == True and sender == lineMID:
                    path = line.downloadObjectMsg(msg_id)
                    settings["changeProfileVideo"] = False
                    line.updateProfileVideo(to, path)
                    line.sendReplyMessage(msg.id,to, "ทำการอัพเดตวีดีโอโปรไฟลเสร็จเรียบร้อย")
                elif msg.contentType in [1,2]:
                    if sender in lineMID:
                        line.sendReplyMessage(msg.id,to, str(msg))
                if msg.contentType == 7:
                    if settings["messageSticker"]["addStatus"] == True:
                        name = settings["messageSticker"]["addName"]
                        if name != None and name in settings["messageSticker"]["listSticker"]:
                            settings["messageSticker"]["listSticker"][name] = {
                                "STKID": msg.contentMetadata["STKID"],
                                "STKVER": msg.contentMetadata["STKVER"],
                                "STKPKGID": msg.contentMetadata["STKPKGID"]
                            }
                            line.sendReplyMessage(msg.id,to, "Success Added " + name)
                        settings["messageSticker"]["addStatus"] = False
                        settings["messageSticker"]["addName"] = None
                    if settings["addSticker"]["status"] == True:
                        stickers[settings["addSticker"]["name"]]["STKVER"] = msg.contentMetadata["STKVER"]
                        stickers[settings["addSticker"]["name"]]["STKID"] = msg.contentMetadata["STKID"]
                        stickers[settings["addSticker"]["name"]]["STKPKGID"] = msg.contentMetadata["STKPKGID"]
                        f = codecs.open('sticker.json','w','utf-8')
                        json.dump(stickers, f, sort_keys=True, indent=4, ensure_ascii=False)
                        line.sendReplyMessage(msg.id,to, "Success Added sticker {}".format(str(settings["addSticker"]["name"])))
                        settings["addSticker"]["status"] = False
                        settings["addSticker"]["name"] = ""
                   
            elif msg.contentType == 7:
                if settings["checkSticker"] == True:
                    stk_id = msg.contentMetadata['STKID']
                    stk_ver = msg.contentMetadata['STKVER']
                    pkg_id = msg.contentMetadata['STKPKGID']
                    ret_ = "╔══[ Sticker Info ]"
                    ret_ += "\n╠ STICKER ID : {}".format(stk_id)
                    ret_ += "\n╠ STICKER PACKAGES ID : {}".format(pkg_id)
                    ret_ += "\n╠ STICKER VERSION : {}".format(stk_ver)
                    ret_ += "\n╠ STICKER URL : line://shop/detail/{}".format(pkg_id)
                    ret_ += "\n╚══[ Finish ]"
                    line.sendReplyMessage(msg.id,to, str(ret_))
              
#==============================================================================#
        if op.type == 19:
            if lineMID in op.param3:
                settings["blacklist"][op.param2] = True
        if op.type == 22:
            if settings['autoLeave'] == True:
                line.leaveRoom(op.param1)              
        if op.type == 24:
            if settings['autoLeave'] == True:
                line.leaveRoom(op.param1)             
#==============================================================================#
#==============================================================================#
        if op.type == 17:
            if op.param2 in settings["blacklist"]:
                random.choice(Rfu).kickoutFromGroup(op.param1,[op.param2])
            else:
                pass
        if op.type == 17:
            if op.param2 not in lineMID and op.param2 not in Bots and op.param2 not in admin and op.param2 not in staff:
                if op.param2 in lineMID and op.param2 in Bots and op.param2 in admin and op.param2 in staff:
                    pass
            if RfuProtect["protect"] == True:
                if settings["blacklist"][op.param2] == True:
                    try:
                        line.kickoutFromGroup(op.param1,[op.param2])
                        G = line.getGroup(op.param1)
                        G.preventedJoinByTicket = True
                        line.updateGroup(G)
                    except:
                        try:
                            line.kickoutFromGroup(op.param1,[op.param2])
                            G = line.getGroup(op.param1)
                            G.preventedJoinByTicket = True
                            line.updateGroup(G)
                        except:
                            pass
 
        if op.type == 19:
            if op.param2 not in lineMID and op.param2 not in Bots and op.param2 not in admin and op.param2 not in staff:
                if op.param2 in lineMID and op.param2 in Bots and op.param2 in admin and op.param2 in staff:
                    pass
                elif RfuProtect["protect"] == True:
                    settings ["blacklist"][op.param2] = True
                    random.choice(Rfu).kickoutFromGroup(op.param1,[op.param2])
                    random.choice(Rfu).inviteIntoGroup(op.param1,[op.param2])

        if op.type == 13:
            if op.param2 not in lineMID and op.param2 not in Bots and op.param2 not in admin and op.param2 not in staff:
                if op.param2 in lineMID and op.param2 in Bots and op.param2 in admin and op.param2 in staff:
                    pass
                elif RfuProtect["inviteprotect"] == True:
                    settings ["blacklist"][op.param2] = True
                    random.choice(Rfu).cancelGroupInvitation(op.param1,[op.param3])
                    random.choice(Rfu).kickoutFromGroup(op.param1,[op.param2])
                    if op.param2 not in Family and op.param2 not in admin and op.param2 not in staff:
                        if op.param2 in Family and op.param2 not in admin and op.param2 not in staff:
                            pass
                        elif RfuProtect["inviteprotect"] == True:
                            settings ["blacklist"][op.param2] = True
                            random.choice(Rfu).cancelGroupInvitation(op.param1,[op.param3])
                            random.choice(Rfu).kickoutFromGroup(op.param1,[op.param2])
                            if op.param2 not in Family:
                                if op.param2 in Family:
                                    pass
                                elif RfuProtect["cancelprotect"] == True:
                                    settings ["blacklist"][op.param2] = True
                                    random.choice(Rfu).cancelGroupInvitation(op.param1,[op.param3])

        if op.type == 11:
            if op.param2 not in lineMID and op.param2 not in Bots and op.param2 not in admin and op.param2 not in staff:
                if op.param2 in lineMID and Bots and op.param2 in admin and op.param2 in staff:
                    pass
                elif RfuProtect["linkprotect"] == True:
                    settings ["blacklist"][op.param2] = True
                    G = line.getGroup(op.param1)
                    G.preventedJoinByTicket = True
                    line.updateGroup(G)
                    random.choice(Rfu).kickoutFromGroup(op.param1,[op.param2])

        if op.type == 11:
            if RfuProtect["linkprotect"] == True:
                if op.param2 not in Bots and op.param2 not in admin and op.param2 not in staff:
                    G = line.getGroup(op.param1)
                    G.preventedJoinByTicket = True
                    random.choice(Rfu).updateGroup(G)
                    random.choice(Rfu).kickoutFromGroup(op.param1,[op.param3])
        if op.type == 11:
            if op.param3 == '1':
                if op.param1 in settings['pname']:
                    try:
                        G = line.getGroup(op.param1)
                    except:
                        pass
                    G.name = settings['pro_name'][op.param1]
                    try:
                        line.updateGroup(G)
                    except:
                        pass
                    if op.param2 in ken:
                        pass
                    else:
                        try:
                            line.kicoutFromGroup(op.param1,[op.param2])
                        except:
                            pass

        if op.type == 13:
           if RfuProtect["Protectguest"] == True:
               if op.param2 not in Bots and op.param2 not in admin and op.param2 not in staff:
                  random.choice(Rfu).cancelGroupInvitation(op.param1,[op.param3])
                  random.choice(Rfu).kickoutFromGroup(op.param1,[op.param2])
        if op.type == 17:
            if op.param2 in settings["blacklist"] == {}:
                line.kickoutFromGroup(op.param1,[op.param2])
                line.sendMessage(op.param1,"สมาชิกที่ถูกแบนไม่ได้รับอนุญาตให้เข้าร่วมกลุ่ม⌐╦╦═─")
        if op.type == 17:
           if RfuProtect["Protectjoin"] == True:
               if op.param2 not in Bots and op.param2 not in admin and op.param2 not in staff:
                   random.choice(Rfu).kickoutFromGroup(op.param1,[op.param2])

        if op.type == 1:
            if sender in Setmain["foto"]:
                path = line.downloadObjectMsg(msg_id)
                del Setmain["foto"][sender]
                line.updateProfilePicture(path)
                line.sendReplyMessage(msg.id,to,"Foto berhasil dirubah")
        if op.type == 25:
#            if settings ["mutebot2"] == True:
            msg = op.message
            text = msg.text
            msg_id = msg.id
            receiver = msg.to
            sender = msg._from
            if msg.toType == 0:
                if sender != line.profile.mid:
                    to = sender
                else:
                    to = receiver
            else:
                to = receiver
#========================================================================
                if msg.contentType == 7:
                    if settings["messageSticker"]["addStatus"] == True:
                        name = settings["messageSticker"]["addName"]
                        if name != None and name in settings["messageSticker"]["listSticker"]:
                            settings["messageSticker"]["listSticker"][name] = {
                                "STKID": msg.contentMetadata["STKID"],
                                "STKVER": msg.contentMetadata["STKVER"],
                                "STKPKGID": msg.contentMetadata["STKPKGID"]
                            }
                            line.sendReplyMessage(msg.id,to, "Success Added " + name)
                        settings["messageSticker"]["addStatus"] = False
                        settings["messageSticker"]["addName"] = None
                    if settings["addSticker"]["status"] == True:
                        stickers[settings["addSticker"]["name"]]["STKVER"] = msg.contentMetadata["STKVER"]
                        stickers[settings["addSticker"]["name"]]["STKID"] = msg.contentMetadata["STKID"]
                        stickers[settings["addSticker"]["name"]]["STKPKGID"] = msg.contentMetadata["STKPKGID"]
                        f = codecs.open('sticker.json','w','utf-8')
                        json.dump(stickers, f, sort_keys=True, indent=4, ensure_ascii=False)
                        line.sendReplyMessage(msg.id,to, "Success Added sticker {}".format(str(settings["addSticker"]["name"])))
                        settings["addSticker"]["status"] = False
                        settings["addSticker"]["name"] = ""
#ADD Bots
                if msg.contentType == 13:
                 if msg._from in admin:
                  if settings["addbots"] == True:
                    if msg.contentMetadata["mid"] in Bots:
                        line.sendReplyMessage(msg.id,to,"Contact itu sudah jadi anggota bot")
                        settings["addbots"] = True
                    else:
                        Bots.append(msg.contentMetadata["mid"])
                        settings["addbots"] = True
                        line.sendReplyMessage(msg.id,to,"Berhasil menambahkan ke anggota bot")
                 if settings["dellbots"] == True:
                    if msg.contentMetadata["mid"] in Bots:
                        Bots.remove(msg.contentMetadata["mid"])
                        line.sendReplyMessage(msg.id,to,"Berhasil menghapus dari anggota bot")
                    else:
                        settings["dellbots"] = True
                        line.sendReplyMessage(msg.id,to,"Contact itu bukan anggota bot Dpk")
#ADD STAFF
                 if msg._from in admin:
                  if settings["addstaff"] == True:
                    if msg.contentMetadata["mid"] in staff:
                        line.sendReplyMessage(msg.id,to,"Contact itu sudah jadi staff")
                        settings["addstaff"] = True
                    else:
                        staff.append(msg.contentMetadata["mid"])
                        settings["addstaff"] = True
                        line.sendReplyMessage(msg.id,to,"Berhasil menambahkan ke staff")
                 if settings["dellstaff"] == True:
                    if msg.contentMetadata["mid"] in staff:
                        staff.remove(msg.contentMetadata["mid"])
                        line.sendReplyMessage(msg.id,to,"Berhasil menghapus dari staff")
                        settings["dellstaff"] = True
                    else:
                        settings["dellstaff"] = True
                        line.sendReplyMessage(msg.id,to,"Contact itu bukan staff")
#ADD ADMIN
                 if msg._from in admin:
                  if settings["addadmin"] == True:
                    if msg.contentMetadata["mid"] in admin:
                        line.sendReplyMessage(msg.id,to,"Contact itu sudah jadi admin")
                        settings["addadmin"] = True
                    else:
                        admin.append(msg.contentMetadata["mid"])
                        settings["addadmin"] = True
                        line.sendReplyMessage(msg.id,to,"Berhasil menambahkan ke admin")
                 if settings["delladmin"] == True:
                    if msg.contentMetadata["mid"] in admin:
                        admin.remove(msg.contentMetadata["mid"])
                        line.sendReplyMessage(msg.id,to,"Berhasil menghapus dari admin")
                    else:
                        settings["delladmin"] = True
                        line.sendReplyMessage(msg.id,to,"Contact itu bukan admin")
#ADD BLACKLIST
                 if msg._from in admin:
                  if settings["wblacklist"] == True:
                    if msg.contentMetadata["mid"] in settings["blacklist"]:
                        line.sendReplyMessage(msg.id,to,"Contact itu sudah ada di blacklist")
                        settings["wblacklist"] = True
                    else:
                        settings["blacklist"][msg.contentMetadata["mid"]] = True
                        settings["wblacklist"] = True
                        line.sendReplyMessage(msg.id,to,"Berhasil menambahkan ke blacklist user")
                  if settings["dblacklist"] == True:
                    if msg.contentMetadata["mid"] in settings["blacklist"]:
                        del settings["blacklist"][msg.contentMetadata["mid"]]
                        line.sendReplyMessage(msg.id,to,"Berhasil menghapus dari blacklist user")
                    else:
                        settings["dblacklist"] = True
                        line.sendReplyMessage(msg.id,to,"Contact itu tidak ada di blacklist")
#TALKBAN
                 if msg._from in admin:
                  if settings["Talkwblacklist"] == True:
                    if msg.contentMetadata["mid"] in settings["Talkblacklist"]:
                        line.sendReplyMessage(msg.id,to,"Contact itu sudah ada di Talkban")
                        settings["Talkwblacklist"] = True
                    else:
                        settings["Talkblacklist"][msg.contentMetadata["mid"]] = True
                        settings["Talkwblacklist"] = True
                        line.sendReplyMessage(msg.id,to,"Berhasil menambahkan ke Talkban user")
                  if settings["Talkdblacklist"] == True:
                    if msg.contentMetadata["mid"] in settings["Talkblacklist"]:
                        del settings["Talkblacklist"][msg.contentMetadata["mid"]]
                        line.sendReplyMessage(msg.id,to,"Berhasil menghapus dari Talkban user")
                    else:
                        settings["Talkdblacklist"] = True
                        line.sendReplyMessage(msg.id,to,"Contact itu tidak ada di Talkban")
        
#==============================================================================================================
        if op.type in [25,26]:
            msg = op.message
            text = msg.text
            msg_id = msg.id
            receiver = msg.to
            sender = msg._from
            if msg.toType == 0 or msg.toType == 1 or msg.toType == 2:
                if msg.toType == 0:
                    if sender != line.profile.mid:
                        to = sender
                    else:
                        to = receiver
                elif msg.toType == 1:
                    to = receiver
                elif msg.toType == 2:
                    to = receiver
##==============================================================================================================
        if op.type == 65:
            print ("[ 65 ] NOTIFIED DESTROY MESSAGE")
            if settings["unsendMessage"] == True:
                at = op.param1
                msg_id = op.param2
                if msg_id in msg_dict:
                    ah = time.time()
                    ikkeh = line.getContact(msg_dict[msg_id]["from"])
                    if "text" in msg_dict[msg_id]:
                        waktumsg = ah - msg_dict[msg_id]["waktu"]
                        waktumsg = format_timespan(waktumsg)
                        rat_ = "\ความเร็ว :\n{} ago".format(waktumsg)
                        rat_ += "\nข้อความ :\n{}".format(msg_dict[msg_id]["text"])
                        sendMention(at, ikkeh.mid, " ** มีคนได้ทำการยกเลิกข้อความ **\n\nระบบเช็คการยกเลิก :\n", str(rat_))
                        del msg_dict[msg_id]
                    else:
                        if "image" in msg_dict[msg_id]:
                            waktumsg = ah - msg_dict[msg_id]["waktu"]
                            waktumsg = format_timespan(waktumsg)
                            rat_ = "\ความเร็ว :\n{} ago".format(waktumsg)
                            rat_ += "\รูปภาพ :\nล่าสุด"
                            sendMention(at, ikkeh.mid, "** มีคนได้ทำการยกเลิกข้อความรูปภาพ **\n\nระบบเช็คการยกเลิก :\n", str(rat_))
                            line.sendImage(at, msg_dict[msg_id]["image"])
                            del msg_dict[msg_id]
                        else:
                            if "video" in msg_dict[msg_id]:
                                waktumsg = ah - msg_dict[msg_id]["waktu"]
                                waktumsg = format_timespan(waktumsg)
                                rat_ = "\nความเร็ว :\n{} ago".format(waktumsg)
                                rat_ += "\nวิดีโอ :\nล่าสุด"
                                sendMention(at, ikkeh.mid, "** มีคนได้ทำการยกเลิกข้อความวิดีโอ **\n\nระบบเช็คการยกเลิก :\n", str(rat_))
                                line.sendVideo(at, msg_dict[msg_id]["video"])
                                del msg_dict[msg_id]
                            else:
                                if "audio" in msg_dict[msg_id]:
                                    waktumsg = ah - msg_dict[msg_id]["waktu"]
                                    waktumsg = format_timespan(waktumsg)
                                    rat_ = "\nความเร็ว :\n{} ago".format(waktumsg)
                                    rat_ += "\nข้อความเสียง :\nล่าสุด"
                                    sendMention(at, ikkeh.mid, "** มีคนได้ทำการยกเลิกข้อความเสียง **\n\nระบบเช็คการยกเลิก :\n", str(rat_))
                                    line.sendAudio(at, msg_dict[msg_id]["audio"])
                                    del msg_dict[msg_id]
                                else:
                                    if "sticker" in msg_dict[msg_id]:
                                        waktumsg = ah - msg_dict[msg_id]["waktu"]
                                        waktumsg = format_timespan(waktumsg)
                                        rat_ = "\nความเร็ว :\n{} ago".format(waktumsg)
                                        rat_ += "\nสติ๊กเกอร์ :\nล่าสุด"
                                        sendMention(at, ikkeh.mid, " ** มีคนได้ทำการยกเลิกข้อความสติ๊กเกอร์ **\n\nระบบเช็คการยกเลิก :\n", str(rat_))
                                        line.sendImageWithURL(at, msg_dict[msg_id]["sticker"])
                                        del msg_dict[msg_id]
                                    else:
                                        if "mid" in msg_dict[msg_id]:
                                            waktumsg = ah - msg_dict[msg_id]["waktu"]
                                            waktumsg = format_timespan(waktumsg)
                                            rat_ = "\nความเร็ว :\n~ {} ago".format(waktumsg)
                                            rat_ += "\nคอนแทค :\nBelow"
                                            sendMention(at, ikkeh.mid, "** มีคนได้ทำการยกเลิกข้อความคอนแทค **\n\nระบบเช็คการยกเลิก :\n", str(rat_))
                                            line.sendContact(at, msg_dict[msg_id]["mid"])
                                            del msg_dict[msg_id]
                                        else:
                                            if "lokasi" in msg_dict[msg_id]:
                                                waktumsg = ah - msg_dict[msg_id]["waktu"]
                                                waktumsg = format_timespan(waktumsg)
                                                rat_ = "\nความเร็ว :\n{} ago".format(waktumsg)
                                                rat_ += "\nโลเคชั่น :\nBelow"
                                                sendMention(at, ikkeh.mid, "** มีคนได้ทำการยกเลิกข้อความสถานที่ **\n\nระบบเช็คการยกเลิก :\n", str(rat_))
                                                line.sendLocation(at, msg_dict[msg_id]["lokasi"])
                                                del msg_dict[msg_id]
                                            else:
                                                if "file" in msg_dict[msg_id]:
                                                    waktumsg = ah - msg_dict[msg_id]["waktu"]
                                                    waktumsg = format_timespan(waktumsg)
                                                    rat_ = "\nความเร็ว :\n{} ago".format(waktumsg)
                                                    rat_ += "\nไฟล์ :\nBelow"
                                                    sendMention(at, ikkeh.mid, "** มีคนได้ทำการยกเลิกข้อความไฟล์ **\n\nระบบเช็คการยกเลิก :\n", str(rat_))
                                                    line.sendFile(at, msg_dict[msg_id]["file"])
                                                    del msg_dict[msg_id]
                else:
                    line.sendMessage(at, "Unsend Message Detected\n\nMessage not in log")
#==========================================================================================#
        if op.type == 26 or op.type == 25:
            msg = op.message
            sender = msg._from
            try:
              
               if pson["kw"][str(msg.text)]:
             #      user = line.Contact(msg._from)
                   line.sendMessage(msg.to,pson["kw"][str(msg.text)])
            except:
              pass
        if op.type == 25:
            msg = op.message
            text = msg.text
            msg_id = msg.id
            receiver = msg.to
            sender = msg._from
            if msg.toType == 0 or msg.toType == 1 or msg.toType == 2:
                if msg.toType == 0:
                    if sender != line.profile.mid:
                        to = sender
                    else:
                        to = receiver
                elif msg.toType == 1:
                    to = receiver
                elif msg.toType == 2:
                    to = receiver
                if msg.text is None:
                    return
                if msg.text.lower() == ".เชคapi":
                    lisk = "[ คำตอบโต้ทั้งหมด ]\n"
                    for i in pson["kw"]:
                        lisk+="\nคีย์เวิร์ด: "+str(i)+"\nตอบโต้: "+str(pson["kw"][i])+"\n"
                    line.sendMessage(msg.to,lisk)
                if msg.text.startswith(".ล้างapi "):
                    try:
                        delcmd = msg.text.split(" ")
                        getx = msg.text.replace(delcmd[0] + " ","")
                        del pson["kw"][getx]
                        line.sendMessage(msg.to, "คำตอบโต้ " + str(getx) + " ล้างแล้ว")
                        f=codecs.open('sb.json','w','utf-8')
                        json.dump(pson, f, sort_keys=True, indent=4, ensure_ascii=False)
                    except Exception as Error:
                        print(Error)
                if msg.text.startswith(".ตั้งapi "):
                    try:
                        delcmd = msg.text.split(" ")
                        get = msg.text.replace(delcmd[0]+" ","").split(";;")
                        kw = get[0]
                        ans = get[1]
                        pson["kw"][kw] = ans
                        f=codecs.open('sb.json','w','utf-8')
                        json.dump(pson, f, sort_keys=True, indent=4, ensure_ascii=False)
                        line.sendMessage(msg.to,"คีย์เวิร์ด: " + str(kw) + "\nตอบกลับ: " +str(ans))
                    except Exception as Error:
                        print(Error)
        if op.type == 26:
            msg = op.message
            text = msg.text
            msg_id = msg.id
            receiver = msg.to
            sender = msg._from
            if msg.toType == 0 or msg.toType == 1 or msg.toType == 2:
                if msg.toType == 0:
                    if sender != line.profile.mid:
                        to = sender
                    else:
                        to = receiver
                elif msg.toType == 1:
                    to = receiver
                elif msg.toType == 2:
                    to = receiver
                if settings["autoRead"] == True:
                        line.sendChatChecked(to, msg_id)				
                if to in read["readPoint"]:
                    if sender not in read["ROM"][to]:
                        read["ROM"][to][sender] = True
                if sender in settings["mimic"]["target"] and settings["mimic"]["status"] == True and settings["mimic"]["target"][sender] == True:
                    text = msg.text
                    if text is not None:
                        line.sendMessage(to,text)
                elif msg.contentType == 7:
                    if settings["checkSticker"] == True:
                        try:
                            stk_id = msg.contentMetadata['STKID']
                            stk_ver = msg.contentMetadata['STKVER']
                            pkg_id = msg.contentMetadata['STKPKGID']
                            ret_ = "「 Check Sticker 」\n"
                            ret_ += "\nSTKID : {}".format(stk_id)
                            ret_ += "\nSTKPKGID : {}".format(pkg_id)
                            ret_ += "\nSTKVER : {}".format(stk_ver)
                            ret_ += "\nLINK : line://shop/detail/{}".format(pkg_id)
                            print(msg)
                            line.sendImageWithURL(to, "http://dl.stickershop.line.naver.jp/products/0/0/"+msg.contentMetadata["STKVER"]+"/"+msg.contentMetadata["STKPKGID"]+"/WindowsPhone/stickers/"+msg.contentMetadata["STKID"]+".png")
                            line.sendReplyMessage(msg.id,to, str(ret_))                            
                        except Exception as error:
                            line.sendReplyMessage(msg.id,to, str(error))
                if msg.text:
                    if msg.text.lower().lstrip().rstrip() in wbanlist:
                        if msg.text not in RfuFamily:
                            try:
                                line.kickoutFromGroup(msg.to,[sender])
                                line.sendReplyMessage(msg.id,"คุณพูดคำต้องห้าม จำเป็นต้องนำออก sorry(╯°□°）╯︵ ┻━┻")
                            except Exception as e:
                                print(e)
                    if "/ti/g/" in msg.text.lower():
                        if settings["autoJoinTicket"] == True:
                            link_re = re.compile('(?:line\:\/|line\.me\/R)\/ti\/g\/([a-zA-Z0-9_-]+)?')
                            links = link_re.findall(text)
                            n_links = []
                            for l in links:
                                if l not in n_links:
                                    n_links.append(l)
                            for ticket_id in n_links:
                                group = line.findGroupByTicket(ticket_id)
                                line.acceptGroupInvitationByTicket(group.id,ticket_id)
                                line.sendReplyMessage(msg.id,to, "มุดลิ้งเข้าไปในกลุ่ม👉 %s 👈 เรียบร้อยแล้ว" % str(group.name))
                if msg.toType == 0 and settings["autoReply"] and sender != lineMID:
                    contact = line.getContact(sender)
                    rjct = ["auto", "ngetag"]
                    validating = [a for a in rjct if a.lower() in text.lower()]
                    if validating != []: return
                    if contact.attributes != 32:
                        msgSticker = settings["messageSticker"]["listSticker"]["sleepSticker"]
                        if msgSticker != None:
                            sid = msgSticker["STKID"]
                            spkg = msgSticker["STKPKGID"]
                            sver = msgSticker["STKVER"]
                            sendSticker(to, sver, spkg, sid)
                        if "@!" in settings["replyPesan"]:
                            msg_ = settings["replyPesan"].split("@!")
                            sendMention(to, sender, "Sleep Mode :\n" + msg_[0], msg_[1])
                        sendMention(to, sender, "Sleep Mode :\nจ๊ะเอ๋", settings["replyPesan"])
                if 'MENTION' in msg.contentMetadata.keys()!= None:
                    if settings["detectMentionPM"] == True:
                        names = re.findall(r'@(\w+)', text)
                        mention = ast.literal_eval(msg.contentMetadata['MENTION'])
                        mentionees = mention['MENTIONEES']
                        for mention in mentionees:
                            if lineMID in mention["M"]:
                                sendMention(sender,sender, "「ตอบแทคอัตโนมัติ」\n", "\n" + str(settings["pmMessage"]))
                                break
                if msg.contentType == 0 and sender not in RfuFamily and msg.toType == 2:
                    if "MENTION" in msg.contentMetadata.keys() != None:
        	             if settings['kickMention'] == True:
        		             contact = line.getContact(msg._from)
        		             cName = contact.displayName
        		             balas = ["เนื่องจากตอนนี้ผมเปิดระบบเตะคนแทคไว้ " + "\n👉" + cName + "\n🙏ต้องขออภัยด้วยจริงๆ🙏Bye!!!"]
        		             ret_ = "" + random.choice(balas)                     
        		             name = re.findall(r'@(\w+)', msg.text)
        		             mention = ast.literal_eval(msg.contentMetadata["MENTION"])
        		             mentionees = mention["MENTIONEES"]
        		             for mention in mentionees:
        			               if mention['M'] in admin:
        				                  line.sendReplyMessage(msg.id,to,ret_)
        				                  line.kickoutFromGroup(msg.to,[msg._from])
        				                  break                                  
        			               if mention['M'] in lineMID:
        				                  line.sendReplyMessage(msg.id,to,ret_)
        				                  line.kickoutFromGroup(msg.to,[msg._from])
        				                  break
                if msg.contentType == 0 and sender not in lineMID and msg.toType == 2:
                    if "MENTION" in list(msg.contentMetadata.keys())!= None:
                         if settings['potoMention'] == True:
                             contact = line.getContact(msg._from)
                             cName = contact.pictureStatus
                             mi_d = contact.mid
                             balas = ["http://dl.profile.line-cdn.net/" + cName]
                             ret_ = random.choice(balas)
                             mention = ast.literal_eval(msg.contentMetadata["MENTION"])
                             mentionees = mention["MENTIONEES"]
                             for mention in mentionees:
                                   if mention["M"] in lineMID:
                                          line.sendImageWithURL(to,ret_)
                                          line.sendContact(msg.to, mi_d)
                                          line.sendReplyMessage(msg.id,to, None, contentMetadata={"PRDID":"a0768339-c2d3-4189-9653-2909e9bb6f58","PRDTYPE":"THEME","MSGTPL":"6"}, contentType=9)
                                          break  
                if msg.contentType == 0 and sender not in lineMID and msg.toType == 2:
                    if "MENTION" in list(msg.contentMetadata.keys()) != None:
                         if settings['detectMention'] == True:
                             contact = line.getContact(msg._from)
                             cName = contact.displayName
                             balas = ["『тєαмвσтмυѕι¢』\n " + cName + "\n\n『ขยันแทคจังห้ะ』"]
                             ret_ = "" + random.choice(balas)
                             name = re.findall(r'@(\w+)', msg.text)
                             mention = ast.literal_eval(msg.contentMetadata["MENTION"])
                             mentionees = mention['MENTIONEES']
                             for mention in mentionees:
                                   if mention['M'] in lineMID:
                                          line.sendReplyMessage(msg.id,to,ret_)
                                          line.sendReplyMessage(msg.id,to,str(settings["Respontag"]))
                                          msgSticker = settings["messageSticker"]["listSticker"]["responSticker"]
                                          if msgSticker != None:
                                              sid = msgSticker["STKID"]
                                              spkg = msgSticker["STKPKGID"]
                                              sver = msgSticker["STKVER"]
                                              sendSticker(to, sver, spkg, sid)
                if msg.contentType == 0 and sender not in lineMID and msg.toType == 2:
                    if "MENTION" in list(msg.contentMetadata.keys()) != None:
                         if settings['delayMention'] == True:
                             contact = line.getContact(msg._from)
                             cName = contact.displayName
                             name = re.findall(r'@(\w+)', msg.text)
                             mention = ast.literal_eval(msg.contentMetadata["MENTION"])
                             mentionees = mention['MENTIONEES']
                             for mention in mentionees:
                                   if mention['M'] in lineMID:
                                          sendMessageWithMention(to, contact.mid)
                                          sendMessageWithMention(to, contact.mid)
                                          sendMessageWithMention(to, contact.mid)
                                          sendMessageWithMention(to, contact.mid)
                                          sendMessageWithMention(to, contact.mid)
                                          sendMessageWithMention(to, contact.mid)
                                          sendMessageWithMention(to, contact.mid)
                                          sendMessageWithMention(to, contact.mid)
                                          sendMessageWithMention(to, contact.mid)
                                          sendMessageWithMention(to, contact.mid)
                                          break

#===========ชุด API คำสั่งใช้งานบอทสิริเชนวี10 ========================== 
        if op.type == 26:
            msg = op.message
            if settings ["Aip"] == True:
            	if msg.text in ["cleanse","group cleansed.","mulai",".winebot",".kickall","mayhem","kick on","Kick","!kickall","nuke","บิน","Kick","กระเด็น","หวด","เซลกากจัง","เตะ",".","ปลิว"]:
                    line.kickoutFromGroup(receiver,[sender])
                    line.sendReplyMessage(msg.id,to,"ตรวจพบคำสั่งของบอทลบกลุ่ม จำเป็นต้องนำออกเพื่อความปลอดภัยของสมาชิก (｀・ω・´)")
            if settings ["Aip"] == True:
                if msg.text in ["ควย","หี","แตด","เย็ดแม่","เย็ดเข้","ค.วย","สัส","เหี้ย","ไอ้เหี้ย","พ่อมึงตาย","ไอ้เลว","ระยำ","ชาติหมา","หน้าหี","เซลกาก","ไอ้เรส","ไอ้เหี้ยเรส","ไอ่เรส","พ่องตาย","ส้นตีน","แม่มึงอ่ะ","แม่มึงดิ","พ่อมึงดิ"]:
                    line.kickoutFromGroup(receiver,[sender])
                    line.sendReplyMessage(msg.id,to,"ตรวจพบคำพูดหยาบคายไม่สุภาพ จำเป็นต้องนำออกเพื่อความสงบสุขของสมาชิก (｀・ω・´)")
            if settings ["Api"] == True:
                if msg.text in ["ป๊า","ป๊ามาย","ลุง","มาย","พี่","เพื่อน","จาร์ย","อาจาร์ย","เฮีย"]:
                    beb = "ว่าไงจร้าที่รัก 😘 " +line.getContact(msg._from).displayName + " 􀸂􀆇starry heart􏿿 ตอนนี้พี่เรสไม่อยู่นะไปทำธุระ"
                    line.sendReplyMessage(msg.id,to,beb)
            if settings ["Api"] == True:
                if msg.text in ["เซล","เซลบอท","selfbot","ขายบอท","บอท"]:
                    beb = line.getContact(msg._from).displayName + " สนใจเซลบอทใช่มั้ยคับท่าน\nโปรดสละเวลาอ่านใบโปรตรงนี้สักครู่...👇👇👇"
                    line.sendReplyMessage(msg.id,to, str(settings["comment"]))
                    line.sendReplyMessage(msg.id,to, None, contentMetadata={"STKID":"52114123","STKPKGID":"11539","STKVER":"1"}, contentType=7)
            if settings ["Api"] == True:
                if msg.text in ["55","555","5555","55555","55+","555+","5555+","ขำ",".ขำ"]:
                    beb = line.getContact(msg._from).displayName + " ฮ่าๆๆๆ..ขำไรจ๊ะ..\n😆ขำด้วยคนดิ เอิ๊ก😆"
                    line.sendReplyMessage(msg.id,to,beb)
                    line.sendReplyMessage(msg.id,to, None, contentMetadata={"STKID":"51626504","STKPKGID":"11538","STKVER":"1"}, contentType=7)
            if settings ["Api"] == True:
                if msg.text in [".ประกาศ","โฆษณา","ประชาสัมพัน","ประกาศ"]:
                    line.sendReplyMessage(msg.id,to, None, contentMetadata={"PRDID":"a0768339-c2d3-4189-9653-2909e9bb6f58","PRDTYPE":"THEME","MSGTPL":"6"}, contentType=9)
                    line.sendReplyMessage(msg.id,to, str(settings["comment"]))
                    line.sendReplyMessage(msg.id,to, None, contentMetadata={"STKID":"52114123","STKPKGID":"11539","STKVER":"1"}, contentType=7)
            if settings["Api"] == True:
                if msg.text in ["กำ","กำหำ","กำหอย"]:
                    beb = line.getContact(msg._from).displayName + " กำเบาๆนะเค้าเจ็บ😁😁 " 
                    line.sendReplyMessage(msg.id,to,beb)
                    line.sendReplyMessage(msg.id,to, None, contentMetadata={"STKID":"51626498","STKPKGID":"11538","STKVER":"1"}, contentType=7)
            if settings["Api"] == True:
                if msg.text in ["งง","งงง",]:
                    beb = "ไม่ต้องงงคับ " +line.getContact(msg._from).displayName + " ผมหล่อแบบนี้มาตั้งนานแล้ว😂😂"
                    line.sendReplyMessage(msg.id,to,beb)
                    line.sendReplyMessage(msg.id,to, None, contentMetadata={"STKID":"51626512","STKPKGID":"11538","STKVER":"1"}, contentType=7)
            if settings["Api"] == True:
                if msg.text in ["หรา","หราา",]:
                    line.sendReplyMessage(msg.id,to, None, contentMetadata={"STKID":"51626516","STKPKGID":"11538","STKVER":"1"}, contentType=7)
            if settings ["Api"] == True:
                if msg.text in ["อยู่ใหม","เทส","ออนใหม"]:
                    beb = "สวัสดีคับท่าน🙇 " +line.getContact(msg._from).displayName
                    line.sendReplyMessage(msg.id,to,beb)
                    line.sendReplyMessage(msg.id,to,"🔷บอททำงานอยู่ 100% คับผม")
            if settings ["Api"] == True:
                if msg.text in ["สวัดดี","หวัดดี","ดีจ้า","ดีครับ","ดี","สวัสดีครับ","สวัสดีค่ะ","ดีงับๆ","ดีงับ","หวัดดีงับๆ"]:
                    beb = "🙏สวัสดีครับ🙏" +line.getContact(msg._from).displayName + " ผมชื่อ🌹เรส🌹ยินดีที่ได้รู้จักครับผม..." + " (starry heart)"
                    line.sendReplyMessage(msg.id,to,beb)
            if settings ["Api"] == True:
                if msg.text in ["มอนิ่ง","นิ่งๆ","ดอนิ่ง","มอนิ่งทุกคน","มอนิ่ง ทุกคน","อรุณสวัสดิ์"]:
                    beb = "🙋มอนิ่งเช่นกันคับ" +line.getContact(msg._from).displayName + " ขอให้รวยขอให้เฮงนะคับวันนี้👼"
                    line.sendReplyMessage(msg.id,to,beb)
            if settings ["Api"] == True:
                if msg.text.lower() in ["hi","hai","halo","hallo"]:
                    beb = "ว่าไงจร้าที่รัก 😘 " +line.getContact(msg._from).displayName 
                    line.sendReplyMessage(msg.id,to,beb)
            if settings ["Api"] == True:
                if msg.text.lower() in ["เหงา","ว้าเหว่","ซึม","เบื่อ"]:
                    beb = "@" +line.getContact(msg._from).displayName + "มาเป็นแฟนพี่มั้ย...ชีวิคมีแต่ความครื้นเครง😬😬"
                    line.sendReplyMessage(msg.id,to,beb)
            if settings ["Api"] == True:
                if msg.text.lower() in ["งิ"]:
                    beb = "งิที่แปลว่าเงี่ยนใช่ป่ะ "+line.getContact(msg._from).displayName +" เดะพี่จัดให้2ดอกเอาบ่อ"
                    line.sendReplyMessage(msg.id,to,beb)
            if settings ["Api"] == True:
                if msg.text.lower() in ["เงี่ยน"]:
                    beb = "เงี่ยนอีกละ " +line.getContact(msg._from).displayName +" เมื่อคืนจัดให้ตั้ง2ดอก"
                    line.sendReplyMessage(msg.id,to,beb)
        if op.type == 26:
            msg = op.message
            if msg.contentType == 16:
                if settings["checkPost"] == True:
                        try:
                            ret_ = "[ ข้อมูลของโพสนี้ ]"
                            if msg.contentMetadata["serviceType"] == "GB":
                                contact = line.getContact(msg._from)
                                auth = "\n  ผู้เขียนโพส : {}".format(str(contact.displayName))
                            else:
                                auth = "\n  ผู้เขียนโพส : {}".format(str(msg.contentMetadata["serviceName"]))
                            purl = "\n  ลิ้งโพส : {}".format(str(msg.contentMetadata["postEndUrl"]).replace("line://","https://line.me/R/"))
                            ret_ += auth
                            ret_ += purl
                            if "mediaOid" in msg.contentMetadata:
                                object_ = msg.contentMetadata["mediaOid"].replace("svc=myhome|sid=h|","")
                                if msg.contentMetadata["mediaType"] == "V":
                                    if msg.contentMetadata["serviceType"] == "GB":
                                        ourl = "\n  Objek URL : https://obs-us.line-apps.com/myhome/h/download.nhn?tid=612w&{}".format(str(msg.contentMetadata["mediaOid"]))
                                        murl = "\n  Media URL : https://obs-us.line-apps.com/myhome/h/download.nhn?{}".format(str(msg.contentMetadata["mediaOid"]))
                                    else:
                                        ourl = "\n  Objek URL : https://obs-us.line-apps.com/myhome/h/download.nhn?tid=612w&{}".format(str(object_))
                                        murl = "\n  Media URL : https://obs-us.line-apps.com/myhome/h/download.nhn?{}".format(str(object_))
                                        ret_ += murl
                                else:
                                    if msg.contentMetadata["serviceType"] == "GB":
                                        ourl = "\n Objek URL : https://obs-us.line-apps.com/myhome/h/download.nhn?tid=612w&{}".format(str(msg.contentMetadata["mediaOid"]))
                                    else:
                                        ourl = "\n Objek URL : https://obs-us.line-apps.com/myhome/h/download.nhn?tid=612w&{}".format(str(object_))
                                ret_ += ourl
                            if "stickerId" in msg.contentMetadata:
                                stck = "\n  Stiker : https://line.me/R/shop/detail/{}".format(str(msg.contentMetadata["packageId"]))
                                ret_ += stck
                            if "text" in msg.contentMetadata:
                                text = "\n ข้อความโดยย่อ : {}".format(str(msg.contentMetadata["text"]))
                                ret_ += text
                                ret_ += text
                            ret_ += "\n[ สิ้นสุดการเช็คโพส ]"
                            line.sendReplyMessage(msg.id,to, str(ret_))
                        except:
                            line.sendReplyMessage(msg.id,to, "เกิดข้อผิดะลาดในการเช็คโพสนี้")
        if op.type == 17:
           print ("MEMBER JOIN TO GROUP")
           if op.param1 in welcome:
             if op.param2 in lineMID:
                 return
             dan = line.getContact(op.param2)
             tgb = line.getGroup(op.param1)
             sendMessageWithMention(op.param1, op.param2)
             line.sendMessage(op.param1,"สวัสดีจ้าคนมาใหม่\n 􀬁􀅳sparkle􏿿• 􂘁􀄗w􏿿􂘁􀄅e􏿿􂘁􀄌l􏿿􂘁􀄃c􏿿􂘁􀄏o􏿿􂘁􀄍m􏿿􂘁􀄅e􏿿 •􀬁􀅳sparkle􏿿\n🙏 {} 🙏\n       ❂➣👇 ᵀᴼ ᴳᴿᴼᵁᴾ👇 \n👉{}👈\n\n 􀼂􀅝church􏿿􀼂􀅜arbor􏿿﹏􀼂􀅞limo 1􏿿􀼂􀅟limo 2􏿿􀼂􀅠limo 3􏿿﹏􀼂􀅜arbor􏿿􀼂􀅝church􏿿\n\n❂➣เข้ามาแล้วก็ทำตัวให้น่ารักๆน๊ะ\n🔇⏯อย่าลืมปิดแจ้งตื่นกันด้วยนะจ๊ะ\n🎙มีข้อสงสัยติดต่อถามได้ที่แอดมินนนะจ๊ะ\n\n".format(str(dan.displayName),str(tgb.name)) + str(settings["welcome"]))
             line.sendContact(op.param1, op.param2)
             line.sendMessage(op.param1,"สเตตัส\n{}".format(str(dan.statusMessage)))
             line.sendImageWithURL(op.param1, "http://dl.profile.line-cdn.net{}".format(dan.picturePath))
        if op.type == 19:
           print ("MEMBER KICKOUT TO GROUP")
           if op.param1 in welcome:
             if op.param2 in lineMID:
                 return
             dan = line.getContact(op.param2)
             tgb = line.getGroup(op.param1)
             line.sendMessage(op.param1,"▄︻̷̿ {} ┻̿═━一 ได้ทำการลบสมาชิกในกลุ่ม Σ(っﾟДﾟ；)っ ".format(str(dan.displayName)) + str(settings["kick"]))
             line.sendContact(op.param1, op.param2)
             line.sendMessage(op.param1,"สเตตัส\n{}".format(str(dan.statusMessage)))
             line.sendImageWithURL(op.param1, "http://dl.profile.line-cdn.net{}".format(dan.picturePath))
        if op.type == 15:
           print ("MEMBER LEAVE TO GROUP")
           if op.param1 in welcome:
             if op.param2 in lineMID:
                 return
             dan = line.getContact(op.param2)
             tgb = line.getGroup(op.param1)
             line.sendMessage(op.param1,"🌠สมาชิกผู้มีเกียรตินามว่า🌠\n👉  {}  👈\nได้ออกจากกลุ่ม\n👉 {} 👈\nไปแล้ว  (｀・ω・´)👀ก็ขอให้โชคดีน๊ะจ๊ะ\n      􀼂􀅝church􏿿􀼂􀅜arbor􏿿﹏􀼂􀅞limo 1􏿿􀼂􀅟limo 2􏿿􀼂􀅠limo 3􏿿﹏􀼂􀅜arbor􏿿􀼂􀅝church􏿿\n\n".format(str(dan.displayName),str(tgb.name)) + str(settings["leave"]))
             line.sendContact(op.param1, op.param2)
             line.sendImageWithURL(op.param1, "http://dl.profile.line-cdn.net{}".format(dan.picturePath))
        if op.type == 17:
           print ("MEMBER JOIN TO GROUP")
           if settings["Wc"] == True:
             if op.param2 in lineMID:
                 return
             dan = line.getContact(op.param2)
             tgb = line.getGroup(op.param1)
             sendMessageWithMention(op.param1, op.param2)
             line.sendMessage(op.param1,"สวัสดีจ้าคนมาใหม่\n 􀬁􀅳sparkle􏿿• 􂘁􀄗w􏿿􂘁􀄅e􏿿􂘁􀄌l􏿿􂘁􀄃c􏿿􂘁􀄏o􏿿􂘁􀄍m􏿿􂘁􀄅e􏿿 •􀬁􀅳sparkle􏿿\n🙏 {} 🙏\n       ❂➣👇 ᵀᴼ ᴳᴿᴼᵁᴾ👇 \n👉{}👈\n\n 􀼂􀅝church􏿿􀼂􀅜arbor􏿿﹏􀼂􀅞limo 1􏿿􀼂􀅟limo 2􏿿􀼂􀅠limo 3􏿿﹏􀼂􀅜arbor􏿿􀼂􀅝church􏿿\n\n❂➣เข้ามาแล้วก็ทำตัวให้น่ารักๆน๊ะ\n🔇⏯อย่าลืมปิดแจ้งตื่นกันด้วยนะจ๊ะ\n🎙มีข้อสงสัยติดต่อถามได้ที่แอดมินนนะจ๊ะ\n\n".format(str(dan.displayName),str(tgb.name)) + str(settings["welcome"]))
             line.sendContact(op.param1, op.param2)
             line.sendMessage(op.param1,"สเตตัส\n{}".format(str(dan.statusMessage)))
             line.sendImageWithURL(op.param1, "http://dl.profile.line-cdn.net{}".format(dan.picturePath))
        if op.type == 19:
           print ("MEMBER KICKOUT TO GROUP")
           if settings["Nk"] == True:
             if op.param2 in lineMID:
                 return
             dan = line.getContact(op.param2)
             tgb = line.getGroup(op.param1)
             line.sendMessage(op.param1,"▄︻̷̿ {} ┻̿═━一 ได้ทำการลบสมาชิกในกลุ่ม Σ(っﾟДﾟ；)っ ".format(str(dan.displayName)) + str(settings["kick"]))
             line.sendContact(op.param1, op.param2)
             line.sendMessage(op.param1,"สเตตัส\n{}".format(str(dan.statusMessage)))
             line.sendImageWithURL(op.param1, "http://dl.profile.line-cdn.net{}".format(dan.picturePath))
        if op.type == 15:
           print ("MEMBER LEAVE TO GROUP")
           if settings["Lv"] == True:
             if op.param2 in lineMID:
                 return
             dan = line.getContact(op.param2)
             tgb = line.getGroup(op.param1)
             line.sendMessage(op.param1,"🌠สมาชิกผู้มีเกียรตินามว่า🌠\n👉  {}  👈\nได้ออกจากกลุ่ม\n👉 {} 👈\nไปแล้ว  (｀・ω・´)👀ก็ขอให้โชคดีน๊ะจ๊ะ\n      􀼂􀅝church􏿿􀼂􀅜arbor􏿿﹏􀼂􀅞limo 1􏿿􀼂􀅟limo 2􏿿􀼂􀅠limo 3􏿿﹏􀼂􀅜arbor􏿿􀼂􀅝church􏿿\n\n".format(str(dan.displayName),str(tgb.name)) + str(settings["leave"]))
             line.sendContact(op.param1, op.param2)
             line.sendImageWithURL(op.param1, "http://dl.profile.line-cdn.net{}".format(dan.picturePath))
        if op.type == 55:
            try:
                if RfuCctv['cyduk'][op.param1]==True:
                    if op.param1 in RfuCctv['point']:
                        Name = line.getContact(op.param2).displayName
                        if Name in RfuCctv['sidermem'][op.param1]:
                            pass
                        else:
                            RfuCctv['sidermem'][op.param1] += "\n🔰" + Name
                            pref=['จ๊ะเอ๋','รู้นะว่าแอบอยู่','เล่นซ่อนแอบกันเหรอ','คิดว่าเป็นนินจารึไง','ว่าไง','อ่านอย่างเดียวเลยนะ','ออกมาคุยหน่อย','ออกมาเดี๋ยวนี้']
                            sendMessageWithMention(op.param1, op.param2)
                            line.sendMessage(op.param1, str(random.choice(pref)) + '\n♪ ♬ ヾ(´︶`♡)ﾉ ♬ ♪')
                            line.sendContact(op.param1, op.param2)
                            sider = line.getContact(op.param2).picturePath
                            image = 'http://dl.profile.line.naver.jp'+sider
                            line.sendImageWithURL(op.param1, image)
                            msgSticker = settings["messageSticker"]["listSticker"]["readerSticker"]
                            if msgSticker != None:
                                sid = msgSticker["STKID"]
                                spkg = msgSticker["STKPKGID"]
                                sver = msgSticker["STKVER"]
                                sendSticker(op.param1, sver, spkg, sid)
                    else:
                        pass
                else:
                    pass
            except:
                pass

        if op.type == 55:
            try:
                if RfuCctv['cyduk'][op.param1]==True:
                    if op.param1 in RfuCctv['point']:
                        Name = line.getContact(op.param2).displayName
                        if Name in RfuCctv['sidermem'][op.param1]:
                            pass
                        else:
                            RfuCctv['sidermem'][op.param1] += "\n⌬ " + Name + "\n╚════════════════┛"
                            if " " in Name:
                            	nick = Name.split(' ')
                            if len(nick) == 2:
                            	line.sendMessage(op.param1, "Nah " +nick[0])
                            summon(op.param1, [op.param2])
                    else:
                        pass
                else:
                    pass
            except:
                pass
        if op.type == 55:
            print (" [тєαмвσтмυѕι¢]  ")
            try:
                if op.param1 in read['readPoint']:
                    if op.param2 in read['readMember'][op.param1]:
                        pass
                    else:
                        read['readMember'][op.param1] += op.param2
                    read['ROM'][op.param1][op.param2] = op.param2
                    backupData()
                else:
                   pass
            except:
                pass
    except Exception as error:
        logError(error)
#==============================================================================#
def a2():
    now2 = datetime.now()
    nowT = datetime.strftime(now2,"%M")
    if nowT[14:] in ["10","20","30","40","50","00"]:
        return False
    else:
        return True
 
while True:
    try:
        ops = oepoll.singleTrace(count=50)
        if ops is not None:
            for op in ops:
                lineBot(op)
                oepoll.setRevision(op.revision)
    except Exception as e:
        logError(e)

def atend():
    print("Saving")
    with open("Log_data.json","w",encoding='utf8') as f:
        json.dump(msg_dict, f, ensure_ascii=False, indent=4,separators=(',', ': '))
    print("BYE")
atexit.register(atend)
