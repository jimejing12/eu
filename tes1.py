
#
# Source Code : NoobLess Team #
# Arfine Meka.
# 

# ------------------------ IMPORT ------------------------ #

import time, random, sys, json, codecs, glob, urllib, pytz, ast, os, multiprocessing, subprocess, tempfile, string, six, urllib.parse, traceback, atexit, html5lib, re, wikipedia, ntpath, threading, base64, shutil, humanize, service, os.path, youtube_dl, requests
import urllib3
urllib3.disable_warnings()
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# ------------------------ IMPORT ------------------------ #
from LineAPI.linepy import *
from liff.ttypes import LiffChatContext, LiffContext, LiffSquareChatContext, LiffNoneContext, LiffViewRequest
from LineAPI.akad import ChannelService,TalkService
from LineAPI.akad.ttypes import Message, Location, LoginRequest, ChatRoomAnnouncementContents, ContentType as Type
from thrift.protocol import TCompactProtocol, TBinaryProtocol, TProtocol
from thrift.transport import THttpClient, TTransport
from thrift.Thrift import TProcessor
from multiprocessing import Pool, Process
from multiprocessing.dummy import Pool as ThreadPool
from threading import Thread, activeCount
from time import sleep
from datetime import datetime, timedelta
from humanfriendly import format_timespan, format_size, format_number, format_length
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# ------------------------ LOGIN ------------------------ #
programStart = time.time()
client = LINE('',appName='WIN10\t5.5.5\tHelloWorld5.5.5')
client.log("Auth Token : " + str(client.authToken))
client.log("Timeline Token : " + str(client.tl.channelAccessToken))

clientMid = client.profile.mid
clientProfile = client.getProfile()
clientSettings = client.getSettings()
mid = client.getProfile().mid
clientPoll = OEPoll(client)
botStart = time.time()
msg_send = {}
temp_flood = {}
tz = pytz.timezone("Asia/Jakarta")
timeNow = datetime.now(tz=tz)

creator = ['u49342b7f4558f9da0b7d1bc2917decae','u582c7fa23eeac2fde0e7850644f1b5ee']
owner = ['u49342b7f4558f9da0b7d1bc2917decae','u582c7fa23eeac2fde0e7850644f1b5ee']
Ton = creator + owner
with open('by.json', 'r') as fp:
    wait = json.load(fp)

settings ={"keyCommand":"","setKey":False}

xxxs = {
    "clock":False,
    "cName":"บอทล็อคอิน มิวสิค",
    "autoAdd":True,
    "autoJoin1":True,
    "autoJoin":True,
    "autoLike": True,
    "comment": "auto like by : เจ้ามายไง >_<\n\n http://line.me/ti/p/~e24jih",

    }

def tokenchrome():
    Headers = {
    'User-Agent': "Line/2.1.5",
    'X-Line-Application': "CHROMEOS\t2.1.5\tBy.TON\t12.1.1",
    "x-lal": "ja-US_US",
    }
    return Headers
    
def tokenwin():
    Headers = {
    'User-Agent': "Line/8.9.1",
    'X-Line-Application': "DESKTOPWIN\t5.8.0\tBy.TON\t5.8.0",
    "x-lal": "ja-US_US",
    }
    return Headers

def headersios():
    Headers = {
    'User-Agent': "Line/9.4.5",
    'X-Line-Application': "IOSIPAD\t8.9.1\tBy.TON\t12.1.1",
    "x-lal": "ja-US_US",
    }
    return Headers
    
def headerschrome():
    Headers1 = {
    'User-Agent': "Line/2.1.5",
    'X-Line-Application': "CHROMEOS\t2.1.5\tBy.TON\t2.1.5",
    "x-lal": "ja-US_US",
    }
    return Headers1
    
def headerswin():
    Headers2 = {
    'User-Agent': "Line/5.8.0",
    'X-Line-Application': "DESKTOPWIN\t5.8.0\tBy.TON\t5.8.0",
    "x-lal": "ja-US_US",
    }
    return Headers2
    
def headersmac():
    Headers3 = {
    'User-Agent': "Line/5.1.2",
    'X-Line-Application': "DESKTOPMAC\t5.1.2\tBy.TON\t5.1.2",
    "x-lal": "ja-US_US",
    }
    return Headers3
    
def headerswin10():
    Headers4 = {
    'User-Agent': "Line/5.5.5",
    'X-Line-Application': "WIN10\t5.5.5\tBy.TON\t5.5.5",
    "x-lal": "ja-US_US",
    }
    return Headers4

def waktu(secs):
    mins, secs = divmod(secs,60)
    hours, mins = divmod(mins,60)
    days, hours = divmod(hours, 24)
    return '%02d วัน %02d ชั่วโมง %02d นาที %02d วินาที' % (days, hours, mins, secs)

def restartBot():
    print ("[ INFO ] BOT RESTART")
    python = sys.executable
    os.execl(python, python, *sys.argv)
        
def backupData():
    try:
        backup = wait
        f = codecs.open('by.json','w','utf-8')
        json.dump(wait, f, sort_keys=True, indent=4, ensure_ascii=False)
        return True
    except:
        e = traceback.format_exc()
        client.sendMessage("u49342b7f4558f9da0b7d1bc2917decae",str(e))
        return False

def sendMention(to, text="", mids=[]):
        arrData = ""
        arr = []
        mention = "@Arfinee Meka "
        if mids == []:
            raise Exception("Invalid mids")
        if "@!" in text:
            if text.count("@!") != len(mids):
                raise Exception("Invalid mids")
            texts = text.split("@!")
            textx = ''
            h = ''
            for mid in range(len(mids)):
                h+= str(texts[mid].encode('unicode-escape'))
                textx += str(texts[mid])
                if h != textx:slen = len(textx)+h.count('U0');elen = len(textx)+h.count('U0') + 13
                else:slen = len(textx);elen = len(textx) + 13
                arrData = {'S':str(slen), 'E':str(elen), 'M':mids[mid]}
                arr.append(arrData)
                textx += mention
            textx += str(texts[len(mids)])
        else:
            textx = ''
            slen = len(textx)
            elen = len(textx) + 18
            arrData = {'S':str(slen), 'E':str(elen - 4), 'M':mids[0]}
            arr.append(arrData)
            textx += mention + str(text)
        client.sendMessage(to, textx, {'MENTION': str('{"MENTIONEES":' + json.dumps(arr) + '}')}, 0)
        
def command(text):
    pesan = text.lower()
    if settings["setKey"] == True:
        if pesan.startswith(settings["keyCommand"]):
            cmd = pesan.replace(settings["keyCommand"],"")
        else:
            cmd = "Undefined command"
    else:
        cmd = text.lower()
    return cmd
    
def menumessage():
    if settings['setKey'] == True:
        key = settings['keyCommand']
    else:
        key = ''
    menuMessage =   """「⬇️คำสั่งสำหรับคนทั่วไป」
🎵 ขอลิ้ง พีชี
🎵 ขอลิ้ง แม็ค
🎵 ขอลิ้ง วิน10
🎵 ขอลิ้ง ไอแพด
🎵 ขอลิ้ง โคม

  ( ⬆️คำสั่งขอโทเคน⬆️)

「⬇️คำสั่งสำหรับผู้ดูแลระบบ」

🎵 รีระบบ
🎵 เพิ่มวัน「แทคคนทีจะเพิ่ม30วัน」
🎵 AddSB
🎵 DelSB
🎵 สมาชิก 
🎵 ล้างระบบ
🎵 เวลา เปิด
🎵 เวลา ปิด
🎵 อัพ
🎵 ออก
🎵 กันรันเปิด / ปิด

「คำสั่งสำหรับแอดมิด」

ขอลิ้ง (คำสั่งขอลิ้งเข้าระบบเชลเฟค)

ขอลิ้ง1 (คำสั่งขอลิ้งเข้าระบบเชลธรรมดา)

outlogin

login  ( qr) 

ออกระบบ (สั่งออกระบบทุกครั้งที่เชลหลุด)

「 вσт ℓσgιи тєαммυѕι¢ 」"""
    return menuMessage
    
def clientBot(op):
    try:
        if op.type == 0:
            return   
                
        if op.type == 5:
            if xxxs["autoAdd"] == True:
                client.blockContact(op.param1) 

        if op.type == 13:
            if mid in op.param3:
                if xxxs["autoJoin1"] == True:
                    if op.param2 not in creator and op.param2 not in org["owner"]:
                        client.acceptGroupInvitation(op.param1)
                        ginfo = client.getGroup(op.param1)
                        sendMention(op.param1, 'สวัสดี @! คุณไม่มีอำนาจ\nหากคุณสนใจเชลบอท\nติดต่อแอดมินเพื่อเช่าซื้อ\n\nhttp://line.me/ti/p/~e24jih', [op.param2])
                        client.leaveGroup(op.param1)
                    else:
                        client.acceptGroupInvitation(op.param1)
                        ginfo = client.getGroup(op.param1)
                        client.sendMessage(op.param1,"บอทล็อคอินพร้อมใช้งานแล้ว\n\nชื่อกลุ่ม " + str(ginfo.name))
            if op.type == 19:
                if op.param3 in "owner":
                    if op.param2 not in creator:
                        client.kickoutFromGroup(op.param1,[op.param2])
                        client.findAndAddContactsByMid(op.param3)
                        client.inviteIntoGroup(op.param1,[op.param3])
                        
        if op.type == 26:
            try:
                msg = op.message
                text = msg.text
                msg_id = msg.id
                receiver = msg.to
                sender = msg._from
                if msg.toType == 0 or msg.toType == 1 or msg.toType == 2:
                    if msg.toType == 0:
                        if sender != client.profile.mid:
                            to = sender
                        else:
                            to = receiver
                    if msg.toType == 1:
                        to = receiver
                    if msg.toType == 2:
                        to = receiver
                    if msg.contentType == 0:
                        if text is None:
                            pass
                        else:
                            cmd = command(text)
                            if text.lower().startswith('ขอลิ้ง '):
                              def telek():
                                c = msg.text.split(" ")[1]
                                anu = ['โคม','พีชี','วิน10','แม็ค','','ไอแพด']
                                if c in anu:
                                  if c == "โคม":
                                     hider = headerschrome()
                                     LA = 'CHROMEOS\t2.1.5\tBy.TON\t2.1.5',
                                  if c == "ไอแพด" or c == "cc":
                                     hider = headersios()
                                     LA = "IOSIPAD\t8.9.1\tBy.TON\t12.1.1"
                                  if c == "พีชี":
                                     hider = headerswin()
                                     LA = "DESKTOPWIN\t5.8.0\tBy.TON\t5.8.0"
                                  if c == "วิน10":
                                     hider = headerswin10()
                                     LA = "WIN10\t5.5.5\tBy.TON\t5.5.5"
                                  if c == "แม็ค":
                                     hider = headersmac()
                                     LA = "DESKTOPMAC\t5.8.0\tBy.TON\t5.8.0"
                                  try:
                                    if not 'cc' in msg.text.lower():
                                      a = hider
                                      a.update({'x-lpqs' : '/api/v4/TalkService.do'})
                                      transport = THttpClient.THttpClient('https://gd2.line.naver.jp/api/v4/TalkService.do')
                                      transport.setCustomHeaders(a)
                                      protocol = TCompactProtocol.TCompactProtocol(transport)
                                      clients = service.Client(protocol)
                                      qr = clients.getAuthQrcode(keepLoggedIn=1, systemName='By.TON')
                                  #    link = "line://au/q/" + qr.verifier
#                                      sendMention(to, "「 Hi @!  」" , [msg._from])
                                      sendMention(to, "「 คุณ @!   」\nกรุณากดที่รูป  โทเคนจะถูกส่งไปยั้งแชทขอคนขอ", [msg._from])
                                      _session = requests.session()
                                      ratedit = LiffChatContext(to)
                                      ratedit1 = LiffContext(chat=ratedit)
                                      view = LiffViewRequest('1602687308-GXq4Vvk9', ratedit1)
                                      token = client.liff.issueLiffView(view)
                                      url = 'https://api.line.me/message/v3/share'
                                      headers = {
                                          'Content-Type': 'application/json',
                                          'Authorization': 'Bearer %s' % token.accessToken
                                      }
                                      data = {"to": to,"messages": [{"type": "flex","altText": "TON LOGIN SELF BOT LINE","contents": {"type": "bubble","header": {"type": "box","layout": "vertical","contents": [{"type": "text","align": "center","color": "#FF0066","size": "xl","weight": "bold","text": "บักต้น คนหล่อเอง"}]},"hero": {"type": "image","url": "https://www.img.in.th/images/02d09bb86a157ce4142d6456a9d9fe83.jpg","size": "full","aspectRatio": "20:13","aspectMode": "fit","action": {"type": "uri","uri": "line://au/q/{}".format(qr.verifier)}},"body": {"type": "box","layout": "vertical","contents": [{"type": "text","align": "center","text": "กรุณากดที่รูปภาพภายใน 2 นาที","wrap": True}]}}}]}
                                      data = json.dumps(data)
                                      sendPost = _session.post(url, data=data, headers=headers)     
                                      a.update({"x-lpqs" : '/api/v4/TalkService.do', 'X-Line-Access': qr.verifier})
                                      json.loads(requests.session().get('https://gd2.line.naver.jp/Q', headers=a).text)
                                      a.update({"x-lpqs" : '/api/v4/TalkService.do', 'X-Line-Access': qr.verifier})
                                      json.loads(requests.session().get('https://gd2.line.naver.jp/Q', headers=a).text)
                                      a.update({'x-lpqs' : '/api/v4p/rs'})
                                      transport = THttpClient.THttpClient('https://gd2.line.naver.jp/api/v4p/rs')
                                      transport.setCustomHeaders(a)
                                      protocol = TCompactProtocol.TCompactProtocol(transport)
                                      clients = service.Client(protocol)
                                      req = LoginRequest()
                                      req.type = 1
                                      req.verifier = qr.verifier
                                      req.e2eeVersion = 1
                                      res = clients.loginZ(req)
                                      nyoh = res.authToken
                                      hasil = "「 โทเคระบบ 」"
                                      hasil += "\n\nคุณ : @!"
                                      hasil += "\n\nUA : Line/8.11.0"
                                      hasil += "\nLA  : {}".format(LA)
                                      hasil += "\n\nโทเคน:\n"+str(nyoh)
                                      sendMention(msg._from ,hasil, [msg._from])
                                    else:
                                      a = hider
                                      a.update({'x-lpqs' : '/api/v4/TalkService.do'})
                                      transport = THttpClient.THttpClient('https://gd2.line.naver.jp/api/v4/TalkService.do')
                                      transport.setCustomHeaders(a)
                                      protocol = TCompactProtocol.TCompactProtocol(transport)
                                      clients = service.Client(protocol)
                                      qr = clients.getAuthQrcode(keepLoggedIn=1, systemName='By-SelfbotPremium')
                               #       link = "line://au/q/" + qr.verifier
                                      sendMention(to, "「 คุณ @!   」\nกรุณากดที่รูป โทเคนจะส่งไปยั้งแชทคนขอ", [msg._from])
                                      _session = requests.session()
                                      ratedit = LiffChatContext(to)
                                      ratedit1 = LiffContext(chat=ratedit)
                                      view = LiffViewRequest('1602687308-GXq4Vvk9', ratedit1)
                                      token = client.liff.issueLiffView(view)
                                      url = 'https://api.line.me/message/v3/share'
                                      headers = {
                                          'Content-Type': 'application/json',
                                          'Authorization': 'Bearer %s' % token.accessToken
                                      }
                                      data = {"to": to,"messages": [{"type": "flex","altText": "TON LOGIN SELF BOT LINE","contents": {"type": "bubble","header": {"type": "box","layout": "vertical","contents": [{"type": "text","align": "center","color": "#FF0066","size": "xl","weight": "bold","text": "บักต้น คนหล่อเอง"}]},"hero": {"type": "image","url": "https://www.img.in.th/images/02d09bb86a157ce4142d6456a9d9fe83.jpg","size": "full","aspectRatio": "20:13","aspectMode": "fit","action": {"type": "uri","uri": "line://au/q/{}".format(qr.verifier)}},"body": {"type": "box","layout": "vertical","contents": [{"type": "text","align": "center","text": "กรุณากดที่รูปภาพภายใน 2 นาที","wrap": True}]}}}]}
                                      data = json.dumps(data)
                                      sendPost = _session.post(url, data=data, headers=headers)     
                                      a.update({"x-lpqs" : '/api/v4/TalkService.do', 'X-Line-Access': qr.verifier})
                                      json.loads(requests.session().get('https://gd2.line.naver.jp/Q', headers=a).text)
                                      a.update({'x-lpqs' : '/api/v4p/rs'})
                                      transport = THttpClient.THttpClient('https://gd2.line.naver.jp/api/v4p/rs')
                                      transport.setCustomHeaders(a)
                                      protocol = TCompactProtocol.TCompactProtocol(transport)
                                      clients = service.Client(protocol)
                                      req = LoginRequest()
                                      req.type = 1
                                      req.verifier = qr.verifier
                                      req.e2eeVersion = 1
                                      res = clients.loginZ(req)
                                      nyoh = res.authToken
                                      headers = {
                                      'X-Line-Application': 'IOSIPAD\t8.9.1\tBy.TON\t12.1.1',
                                      'X-Line-Access': res.authToken
                                       }
                                      transport = THttpClient.THttpClient('https://gd2.line.naver.jp/CH4')
                                      transport.setCustomHeaders(headers)
                                      protocol = TCompactProtocol.TCompactProtocol(transport)
                                      channel = ChannelService.Client(protocol)
                                      chantok = channel.approveChannelAndIssueChannelToken('1526709289').token
                                      hasil = "「 โทเคนระบบ 」"
                                      hasil += "\n\nคุณ : @!"
                                      hasil += "\n\nUA : Line/8.11.0"
                                      hasil += "\nLA  : {}".format(LA)
                                      hasil += "\n\nโทเคน:\n"+str(nyoh)
                                      basil = chantok
                                      sendMention(msg._from,hasil, [msg._from])
                                      sendMention(to,"Login From @!\nResult CC: {}".format(basil), [msg._from])
                                  except:
                                      sendMention(to," 「 สวัสดี @!, กรุณากดลิ้งเพื่อเข้าระบบ 」")
                                      pass
                              thread = threading.Thread(target=telek)
                              thread.daemon = True
                              thread.start()
                            if text.lower() == "รีระบบ" and sender in ['u49342b7f4558f9da0b7d1bc2917decae','u73fd2941f6c8cc5611ec399fe43538b2']:
                                try:
                                    sam = {'MSG_SENDER_ICON': "https://os.line.naver.jp/os/p/"+sender,'MSG_SENDER_NAME':  client.getContact(sender).displayName,}                            
                                    client.sendMessage(to, "🎤รีบอทล็อคอินเรียบร้อย...ครับท่าน🎤", contentMetadata=sam)
                                    restartBot()
                                except:
                                    e = traceback.format_exc()
                                    client.sendMessage("u49342b7f4558f9da0b7d1bc2917decae",str(e))

                            if text.lower().startswith('add ') and sender in ['u49342b7f4558f9da0b7d1bc2917decae','u508c0ed002f65aabda94569a0dbc0e45']:
                                if 'MENTION' in msg.contentMetadata.keys()!= None:
                                    key = eval(msg.contentMetadata["MENTION"])
                                    key1 = key["MENTIONEES"][0]["M"]
                                    if key1 not in wait['info']:
                                        pay = time.time()
                                        nama = str(text.split(' ')[1])
                                        wait['name'][nama] =  {"mid":key1,"pay":pay+60*60*24*30,"runtime":pay,"token":{}}
                                        wait['info'][key1] =  '%s' % nama
                                        sendMention(msg.to, ' 「 Serivce 」\n@! Add to Service\ndont forget to turn off letter sealing in setting privacy',[key1])
                                    else:
                                        sendMention(msg.to, ' 「 Serivce 」\n@! Already in Service',[key1])
                            if text.lower().startswith('addsb ') and sender in ["u49342b7f4558f9da0b7d1bc2917decae","u582c7fa23eeac2fde0e7850644f1b5ee","u508c0ed002f65aabda94569a0dbc0e45"]:
                                if 'MENTION' in msg.contentMetadata.keys()!= None:
                                    key = eval(msg.contentMetadata["MENTION"])
                                    key1 = key["MENTIONEES"][0]["M"]
                                    if key1 not in wait['info']:
                                        pay = time.time()
                                        nama = str(text.split(' ')[1])
                                        wait['name'][nama] =  {"mid":key1,"pay":pay+60*60*24*30,"runtime":pay,"token":{}}
                                        wait['info'][key1] =  '%s' % nama
                                        sendMention(msg.to, ' 「 บริการ selfbot พร้อมใช้งาน 」\n@! ระบบได้เพิ่มคุณลงใน server แล้ว\n\nกรุณา พิม [ ที่ลงทะเบียนไว้ ] เพื่อเข้าระบบเชล',[key1])
                                    else:
                                        sendMention(msg.to, ' 「 บริการ 」\n@! อยู่ในระบบ',[key1])

                            if text.lower().startswith('1ชม ') and sender in ["u49342b7f4558f9da0b7d1bc2917decae","uef3f854dc65f76615d8e143db5b76829","u508c0ed002f65aabda94569a0dbc0e45"]:
                                if 'MENTION' in msg.contentMetadata.keys()!= None:
                                    key = eval(msg.contentMetadata["MENTION"])
                                    key1 = key["MENTIONEES"][0]["M"]
                                    if key1 not in wait['info']:
                                        pay = time.time()
                                        nama = str(text.split(' ')[1])
                                        wait['name'][nama] =  {"mid":key1,"pay":pay+60*60,"runtime":pay,"token":{}}
                                        wait['info'][key1] =  '%s' % nama
                                        sendMention(msg.to, ' 「 บริการ selfbot พร้อมใช้งาน 」\n@! ระบบได้เพิ่มคุณลงใน server แล้ว\n\nกรุณา พิม [ ขอลิ้ง ] เพื่อเข้าระบบเชล',[key1])
                                    else:
                                        sendMention(msg.to, ' 「 บริการ 」\n@! อยู่ในระบบ',[key1])

                            if text.lower().startswith('delsb ') and sender in ["u49342b7f4558f9da0b7d1bc2917decae","u508c0ed002f65aabda94569a0dbc0e45"]:
                                sep = text.split(" ")
                                query = text.replace(sep[0] + " ","")
                                aa = [a for a in wait['info']]
                                try:
                                    listContact = aa[int(query)-1]
                                    if listContact in wait['info']:
                                        b = wait['info'][listContact]
                                        os.system('screen -S %s -X kill'%b)
                                        try:subprocess.getoutput('rm -rf {}'.format(b))
                                        except:pass
                                        del wait['info'][listContact]
                                        del wait['name'][b]
                                        sendMention(to, ' 「 บริการ 」\n@! ลบออกจากระบบแล้ว', [listContact])
                                    else:
                                        sendMention(to, ' 「 บริการ 」\n@! ไม่อยู่ในระบบ', [listContact])
                                except:
                                    e = traceback.format_exc()
                                    client.sendMessage("uc1f0bbb79e5fc4515f8b4991ec3119a0",str(e))
                            if text.lower().startswith('delsb') and sender in ["u49342b7f4558f9da0b7d1bc2917decae","u508c0ed002f65aabda94569a0dbc0e45","u582c7fa23eeac2fde0e7850644f1b5ee"]:
                                if 'MENTION' in msg.contentMetadata.keys()!= None:
                                    key = eval(msg.contentMetadata["MENTION"])
                                    key1 = key["MENTIONEES"][0]["M"]
                                    if key1 in wait['info']:
                                        b = wait['info'][key1]
                                        os.system('screen -S %s -X kill'%b)
                                        try:subprocess.getoutput('rm -rf {}'.format(b))
                                        except:pass
                                        del wait['info'][key1]
                                        del wait['name'][b]
                                        sendMention(to, ' \n@! ระบบได้ตัดสิทธิ์คุณออกจากบริการแล้วครับ โปรดชำระเงินเพื่อใช้บริการอย่างต่อเนื่อง ขอบคุณครับที่ใช้บริการเรา',[key1])
                                    else:
                                        sendMention(to, ' 「 บริการ selfbot พร้อมใช้งาน 」\n@! ผู้ใช้รายนี้ไม่อยู่ใน server', [key1])
                            if text.lower() == 'สมาชิก' and sender in ["u49342b7f4558f9da0b7d1bc2917decae","u508c0ed002f65aabda94569a0dbc0e45","u582c7fa23eeac2fde0e7850644f1b5ee"]:
                                h = [a for a in wait['info']]
                                k = len(h)//20
                                for aa in range(k+1):
                                    if aa == 0:dd = '「 รายการ ลูกค้า 」';no=aa
                                    else:dd = '';no=aa*20
                                    msgas = dd
                                    for a in h[aa*20:(aa+1)*20]:
                                        no+=1
                                        if wait['name'][wait['info'][a]]["pay"] <= time.time():sd = 'หมดอายุใช้งานแล้ว'
                                        else:sd = humanize.naturaltime(datetime.fromtimestamp(wait['name'][wait['info'][a]]["pay"]))
                                        if no == len(h):msgas+='\n{}. @! {}'.format(no,sd)
                                        else:msgas += '\n{}. @! {}'.format(no,sd)
                                    sendMention(to, msgas, h[aa*20:(aa+1)*20])
                            if text.lower() == '.ล้างระบบ':
                                if msg._from in ["u49342b7f4558f9da0b7d1bc2917decae","u73fd2941f6c8cc5611ec399fe43538b2","u5d741cfb98d007c52c12da5f4b40a38a"]:
                                    h = ''
                                    no=0
                                    for a in wait['info']:
                                        us = wait["info"][a]
                                        try:
                                            os.system('screen -S %s -X kill'%us)
                                        except:pass
                                    client.generateReplyMessage(msg.id)
                                    client.sendReplyMessage(msg.id,to,'ระบบได้ทำการปิดเชลทุกคนแล้ว โปรอ ขอลิ้ง เข้าระบบใหม่...')
                            if text.lower() == "outlogin":
                              if sender in wait['info']:
                                us = wait["info"][sender]
                                contact = client.getContact(sender)
                                os.system('screen -S {} -X quit'.format(us))
                                os.system('rm -rf {}'.format(us))
                                if msg.toType == 2:
                                    client.sendMessage(to, "Name: {}\nStatus: Success Logout From Server\nPlease Logout from you Device \nline://nv/connectedDevices".format(contact.displayName))
                                else:
                                    sendMention(to, "「 Sukses 」\nSukses logout from server user @! \nPlease Logout from you Device", [sender])
                              else:
                                sendMention(to, ' 「 Error 」\nHi @!\nYou not my cust!\nPlease contact @! for buy', [sender, "u49342b7f4558f9da0b7d1bc2917decae"])

                            if text.lower() == "ออกระบบ":
                              if sender in wait['info']:
                                us = wait["info"][sender]
                                contact = client.getContact(sender)
                                os.system('screen -S {} -X quit'.format(us))
                                os.system('rm -rf {}'.format(us))
                                if msg.toType == 2:
                                    client.sendMessage(to, "คุณ:   {}\n\nเช็ค: ได้ทำการออกระบบเรียบร้อย\n\nกรุณาขอลิ้งเข้าระบบอีกครั้ง \nline://nv/connectedDevices".format(contact.displayName))
                                else:
                                    sendMention(to, "「 แจ้งเตือน 」\nคุณได้ทำการออกจากระบบอยู่แล้ว @! ", [sender])
                              else:
                                sendMention(to, ' 「 เกิดข้อผิดพลาด 」\nคุณ @!\nไม่พบรายชื่อคุณในระบบ\nโปรดติดต่อแอดมิน @! ', [sender, "u49342b7f4558f9da0b7d1bc2917decae"])


                            #if text.lower() == "ขอลิ้ง":
                                client.generateReplyMessage(msg.id)
                                if sender in wait['info']:
                                        us = wait["info"][sender]
                                        ti = wait['name'][us]["pay"]-time.time()
                                        sec = int(ti %60)
                                        minu = int(ti/60%60)
                                        hours = int(ti/60/60 %24)
                                        days = int(ti/60/60/24)
                                        wait['name'][us]["pay"] = wait['name'][us]["pay"]
                                        if wait["name"][us]["pay"] <= time.time():
                                            os.system('rm -rf {}'.format(us))
                                            os.system('screen -S %s -X kill'%us)
                                            sendMention(to, ' 「 หมดอายุ 」\n Sorry @! บัญชีของคุณหมดเวลาใช้งานแล้วแล้ว', [sender])
                                        else:
                                            us = wait["info"][sender]
                                            try:
                                                def kentod():
                                                    a = headerswin10()
                                                    a.update({'x-lpqs' : '/api/v4/TalkService.do'})
                                                    transport = THttpClient.THttpClient('https://gd2.line.naver.jp/api/v4/TalkService.do')
                                                    transport.setCustomHeaders(a)
                                                    protocol = TCompactProtocol.TCompactProtocol(transport)
                                                    clients = service.Client(protocol)
                                                    qr = clients.getAuthQrcode(keepLoggedIn=1, systemName='TEAMBOTMUSIC')
                                                    sendMention(to,"「 เข้าสู่ระบบ 」\nคุณ: @!\nชื่อไฟล์: {}\nกรุณากดรูปด้านล่างเพื่อเข้าสู่ระบบ!".format(us,us), [sender])
            #                                        sendMention(to,"「 กรุณากดที่รูป 」\nคุณ: @!\nชื่อไฟส: {}".format(us,us), [sender])
                                                    _session = requests.session()
                                                    ratedit = LiffChatContext(to)
                                                    ratedit1 = LiffContext(chat=ratedit)
                                                    view = LiffViewRequest('1602687308-GXq4Vvk9', ratedit1)
                                                    token = client.liff.issueLiffView(view)
                                                    url = 'https://api.line.me/message/v3/share'
                                                    headers = {
                                                        'Content-Type': 'application/json',
                                                        'Authorization': 'Bearer %s' % token.accessToken
                                                    }
                                                    data = {"to": to,"messages": [{"type": "flex","altText": "TEAM MUSIC LOGIN SELF BOT LINE","contents": {"type": "bubble","header": {"type": "box","layout": "vertical","contents": [{"type": "text","align": "center","color": "#FF0066","size": "xl","weight": "bold","text": "TEAMBOTMUSIC"}]},"hero": {"type": "image","url": "https://sv1.picz.in.th/images/2019/02/21/TXe1RN.gif","size": "full","aspectRatio": "20:13","aspectMode": "fit","action": {"type": "uri","uri": "line://au/q/{}".format(qr.verifier)}},"body": {"type": "box","layout": "vertical","contents": [{"type": "text","align": "center","text": "กรุณากดที่รูปภาพภายใน 2 นาที","wrap": True}]}}}]}
                                                    data = json.dumps(data)
                                                    sendPost = _session.post(url, data=data, headers=headers)     
                                                    a.update({"x-lpqs" : '/api/v4/TalkService.do', 'X-Line-Access': qr.verifier})
                                                    json.loads(requests.session().get('https://gd2.line.naver.jp/Q', headers=a).text)
                                                    a.update({'x-lpqs' : '/api/v4p/rs'})
                                                    transport = THttpClient.THttpClient('https://gd2.line.naver.jp/api/v4p/rs')
                                                    transport.setCustomHeaders(a)
                                                    protocol = TCompactProtocol.TCompactProtocol(transport)
                                                    clients = service.Client(protocol)
                                                    req = LoginRequest()
                                                    req.type = 1
                                                    req.verifier = qr.verifier
                                                    req.e2eeVersion = 1
                                                    res = clients.loginZ(req)
                                                    wait['name'][us]["token"] = res.authToken
                                                    token = wait['name'][us]["token"]
                                                    os.system('screen -S %s -X kill'%us)
                                                    os.system('cp -r login5 {}'.format(us))
                                                    os.system('cd {} && echo -n "{} \c" > token.txt'.format(us, token))
                                                    os.system('screen -dmS {}'.format(us))
                                                    os.system('screen -r {} -X stuff "cd {} && python3 frees.py \n"'.format(us, us))
                                                    contact = client.getContact(sender)
                                                    sendMention(to, "「 เข้าระบบเรียบร้อย! 」\nชื่อไฟล์: {}\nMid: {}\nเวลาคงเหลือ: {} วัน {} ชั่วโมง {} นาที \nคุณ @! \n> กรุณากดลิ้งด้านล่างด้วย(กดแค่ครั้งแรกพอ) :\nline://app/1602687308-GXq4Vvk9?type=text&text=Teambotmusic".format(us,sender,days,hours,minu), [sender])
                                                thread = threading.Thread(target=kentod)
                                                thread.daemon = True
                                                thread.start()
                                            except:
                                                pass
                                else:
                                    sendMention(to, ' 「 เออเร่อ 」\nบัก @!\nอย่าติดสั่งเล่นตบเลย\nโปรดติดต่อแอดมินสุดหล่อ @! ', [sender, "u6156da4ad435b4c3bd200584869abd66"])
                            if text.lower() == "ล็อคอิน":
                                client.generateReplyMessage(msg.id)
                                if sender in wait['info']:
                                        us = wait["info"][sender]
                                        ti = wait['name'][us]["pay"]-time.time()
                                        sec = int(ti %60)
                                        minu = int(ti/60%60)
                                        hours = int(ti/60/60 %24)
                                        days = int(ti/60/60/24)
                                        wait['name'][us]["pay"] = wait['name'][us]["pay"]
                                        if wait["name"][us]["pay"] <= time.time():
                                            os.system('rm -rf {}'.format(us))
                                            os.system('screen -S %s -X kill'%us)
                                            sendMention(to, ' 「 หมดอายุ 」\n Sorry @! บัญชีของคุณหมดเวลาใช้งานแล้วแล้ว', [sender])
                                        else:
                                            us = wait["info"][sender]
                                            try:
                                                def kentod():
                                                    a = headerswin10()
                                                    a.update({'x-lpqs' : '/api/v4/TalkService.do'})
                                                    transport = THttpClient.THttpClient('https://gd2.line.naver.jp/api/v4/TalkService.do')
                                                    transport.setCustomHeaders(a)
                                                    protocol = TCompactProtocol.TCompactProtocol(transport)
                                                    clients = service.Client(protocol)
                                                    qr = clients.getAuthQrcode(keepLoggedIn=1, systemName='TEAMBOTMUSIC')
                                                    sendMention(to,"「 เข้าสู่ระบบ 」\nคุณ: @!\nชื่อไฟล์: {}\nกรุณากดรูปด้านล่างเพื่อเข้าสู่ระบบ!".format(us,us), [sender])
            #                                        sendMention(to,"「 กรุณากดที่รูป 」\nคุณ: @!\nชื่อไฟส: {}".format(us,us), [sender])
                                                    _session = requests.session()
                                                    ratedit = LiffChatContext(to)
                                                    ratedit1 = LiffContext(chat=ratedit)
                                                    view = LiffViewRequest('1602687308-GXq4Vvk9', ratedit1)
                                                    token = client.liff.issueLiffView(view)
                                                    url = 'https://api.line.me/message/v3/share'
                                                    headers = {
                                                        'Content-Type': 'application/json',
                                                        'Authorization': 'Bearer %s' % token.accessToken
                                                    }
                                                    data = {"to": to,"messages": [{"type": "flex","altText": "TEAM MUSIC LOGIN SELF BOT LINE","contents": {"type": "bubble","header": {"type": "box","layout": "vertical","contents": [{"type": "text","align": "center","color": "#FF0066","size": "xl","weight": "bold","text": "🎤 ทีมบอทมิวสิค 🎤"}]},"hero": {"type": "image","url": "https://sv1.picz.in.th/images/2019/03/30/tCjO9e.jpg","size": "full","aspectRatio": "20:13","aspectMode": "fit","action": {"type": "uri","uri": "line://au/q/{}".format(qr.verifier)}},"body": {"type": "box","layout": "vertical","contents": [{"type": "text","align": "center","text": "กรุณากดที่รูปภาพภายใน 2 นาที","wrap": True}]}}}]}
                                                    data = json.dumps(data)
                                                    sendPost = _session.post(url, data=data, headers=headers)     
                                                    a.update({"x-lpqs" : '/api/v4/TalkService.do', 'X-Line-Access': qr.verifier})
                                                    json.loads(requests.session().get('https://gd2.line.naver.jp/Q', headers=a).text)
                                                    a.update({'x-lpqs' : '/api/v4p/rs'})
                                                    transport = THttpClient.THttpClient('https://gd2.line.naver.jp/api/v4p/rs')
                                                    transport.setCustomHeaders(a)
                                                    protocol = TCompactProtocol.TCompactProtocol(transport)
                                                    clients = service.Client(protocol)
                                                    req = LoginRequest()
                                                    req.type = 1
                                                    req.verifier = qr.verifier
                                                    req.e2eeVersion = 1
                                                    res = clients.loginZ(req)
                                                    wait['name'][us]["token"] = res.authToken
                                                    token = wait['name'][us]["token"]
                                                    os.system('screen -S %s -X kill'%us)
                                                    os.system('cp -r login5 {}'.format(us))
                                                    os.system('cd {} && echo -n "{} \c" > token.txt'.format(us, token))
                                                    os.system('screen -dmS {}'.format(us))
                                                    os.system('screen -r {} -X stuff "cd {} && python3 mind1.py \n"'.format(us, us))
                                                    contact = client.getContact(sender)
                                                    sendMention(to, "「 เข้าระบบเรียบร้อย! 」\nชื่อไฟล์: {}\nMid: {}\nเวลาคงเหลือ: {} วัน {} ชั่วโมง {} นาที \nคุณ @! \n> กรุณากดลิ้งด้านล่างด้วย(กดแค่ครั้งแรกพอ) :\nline://app/1602687308-GXq4Vvk9?type=text&text=TEAMBOTMUSIC".format(us,sender,days,hours,minu), [sender])
                                                thread = threading.Thread(target=kentod)
                                                thread.daemon = True
                                                thread.start()
                                            except:
                                                pass
                                else:
                                    sendMention(to, ' 「 เออเร่อ 」\nคุณ @!\nไม่มีอำนาจมาสั่งกูน่ะ\nโปรดติดต่อแอดมินสุดหล่อ @! ', [sender, "u49342b7f4558f9da0b7d1bc2917decae"])

                            if text.lower() == "ขอลิ้ง2":
                                client.generateReplyMessage(msg.id)
                                if sender in wait['info']:
                                        us = wait["info"][sender]
                                        ti = wait['name'][us]["pay"]-time.time()
                                        sec = int(ti %60)
                                        minu = int(ti/60%60)
                                        hours = int(ti/60/60 %24)
                                        days = int(ti/60/60/24)
                                        wait['name'][us]["pay"] = wait['name'][us]["pay"]
                                        if wait["name"][us]["pay"] <= time.time():
                                            os.system('rm -rf {}'.format(us))
                                            os.system('screen -S %s -X kill'%us)
                                            sendMention(to, ' 「 หมดอายุ 」\n Sorry @! บัญชีของคุณหมดเวลาใช้งานแล้วแล้ว', [sender])
                                        else:
                                            us = wait["info"][sender]
                                            try:
                                                def kentod():
                                                    a = headerswin10()
                                                    a.update({'x-lpqs' : '/api/v4/TalkService.do'})
                                                    transport = THttpClient.THttpClient('https://gd2.line.naver.jp/api/v4/TalkService.do')
                                                    transport.setCustomHeaders(a)
                                                    protocol = TCompactProtocol.TCompactProtocol(transport)
                                                    clients = service.Client(protocol)
                                                    qr = clients.getAuthQrcode(keepLoggedIn=1, systemName='TON-win10')
                                                    sendMention(to,"「 เข้าสู่ระบบ 」\nคุณ: @!\nชื่อไฟล์: {}\nกรุณากดรูปด้านล่างเพื่อเข้าสู่ระบบ!".format(us,us), [sender])
            #                                        sendMention(to,"「 กรุณากดที่รูป 」\nคุณ: @!\nชื่อไฟส: {}".format(us,us), [sender])
                                                    _session = requests.session()
                                                    ratedit = LiffChatContext(to)
                                                    ratedit1 = LiffContext(chat=ratedit)
                                                    view = LiffViewRequest('1632242027-drK8Q1KZ', ratedit1)
                                                    token = client.liff.issueLiffView(view)
                                                    url = 'https://api.line.me/message/v3/share'
                                                    headers = {
                                                        'Content-Type': 'application/json',
                                                        'Authorization': 'Bearer %s' % token.accessToken
                                                    }
                                                    data = {"to": to,"messages": [{"type": "flex","altText": "TON LOGIN SELF BOT LINE","contents": {"type": "bubble","header": {"type": "box","layout": "vertical","contents": [{"type": "text","align": "center","color": "#FF0066","size": "xl","weight": "bold","text": "บักต้น คนหล่อเอง"}]},"hero": {"type": "image","url": "https://www.img.in.th/images/02d09bb86a157ce4142d6456a9d9fe83.jpg","size": "full","aspectRatio": "20:13","aspectMode": "fit","action": {"type": "uri","uri": "line://au/q/{}".format(qr.verifier)}},"body": {"type": "box","layout": "vertical","contents": [{"type": "text","align": "center","text": "กรุณากดที่รูปภาพภายใน 2 นาที","wrap": True}]}}}]}
                                                    data = json.dumps(data)
                                                    sendPost = _session.post(url, data=data, headers=headers)     
                                                    a.update({"x-lpqs" : '/api/v4/TalkService.do', 'X-Line-Access': qr.verifier})
                                                    json.loads(requests.session().get('https://gd2.line.naver.jp/Q', headers=a).text)
                                                    a.update({'x-lpqs' : '/api/v4p/rs'})
                                                    transport = THttpClient.THttpClient('https://gd2.line.naver.jp/api/v4p/rs')
                                                    transport.setCustomHeaders(a)
                                                    protocol = TCompactProtocol.TCompactProtocol(transport)
                                                    clients = service.Client(protocol)
                                                    req = LoginRequest()
                                                    req.type = 1
                                                    req.verifier = qr.verifier
                                                    req.e2eeVersion = 1
                                                    res = clients.loginZ(req)
                                                    wait['name'][us]["token"] = res.authToken
                                                    token = wait['name'][us]["token"]
                                                    os.system('screen -S %s -X kill'%us)
                                                    os.system('cp -r login1 {}'.format(us))
                                                    os.system('cd {} && echo -n "{} \c" > token.txt'.format(us, token))
                                                    os.system('screen -dmS {}'.format(us))
                                                    os.system('screen -r {} -X stuff "cd {} && python3 staff.py \n"'.format(us, us))
                                                    contact = client.getContact(sender)
                                                    sendMention(to, "「 เข้าระบบเรียบร้อย! 」\nชื่อไฟล์: {}\nMid: {}\nเวลาคงเหลือ: {} วัน {} ชั่วโมง {} นาที \nคุณ @! \n> กรุณากดลิ้งด้านล่างด้วย(กดแค่ครั้งแรกพอ) :\nline://app/1602687308-GXq4Vvk9?type=text&text=TON-BOT".format(us,sender,days,hours,minu), [sender])
                                                thread = threading.Thread(target=kentod)
                                                thread.daemon = True
                                                thread.start()
                                            except:
                                                pass
                                else:
                                    sendMention(to, ' 「 เออเร่อ 」\nบัก @!\nมึงไม่มีอำนาจสั่งกู\nโปรดติดต่อแอดมิน @! ', [sender, "u49342b7f4558f9da0b7d1bc2917decae"])
                            if text.lower() == "ขอลิ้ง1":
                                client.generateReplyMessage(msg.id)
                                if sender in wait['info']:
                                        us = wait["info"][sender]
                                        ti = wait['name'][us]["pay"]-time.time()
                                        sec = int(ti %60)
                                        minu = int(ti/60%60)
                                        hours = int(ti/60/60 %24)
                                        days = int(ti/60/60/24)
                                        wait['name'][us]["pay"] = wait['name'][us]["pay"]
                                        if wait["name"][us]["pay"] <= time.time():
                                            os.system('rm -rf {}'.format(us))
                                            os.system('screen -S %s -X kill'%us)
                                            sendMention(to, ' 「 หมดอายุ 」\n Sorry @! บัญชีของคุณหมดเวลาใช้งานแล้วแล้ว', [sender])
                                        else:
                                            us = wait["info"][sender]
                                            try:
                                                def kentod():
                                                    a = headerswin10()
                                                    a.update({'x-lpqs' : '/api/v4/TalkService.do'})
                                                    transport = THttpClient.THttpClient('https://gd2.line.naver.jp/api/v4/TalkService.do')
                                                    transport.setCustomHeaders(a)
                                                    protocol = TCompactProtocol.TCompactProtocol(transport)
                                                    clients = service.Client(protocol)
                                                    qr = clients.getAuthQrcode(keepLoggedIn=1, systemName='My-win10')
                                                    sendMention(to,"「 เข้าสู่ระบบ 」\nคุณ: @!\nชื่อไฟล์: {}\nกรุณากดรูปด้านล่างเพื่อเข้าสู่ระบบ!".format(us,us), [sender])
            #                                        sendMention(to,"「 กรุณากดที่รูป 」\nคุณ: @!\nชื่อไฟส: {}".format(us,us), [sender])
                                                    _session = requests.session()
                                                    ratedit = LiffChatContext(to)
                                                    ratedit1 = LiffContext(chat=ratedit)
                                                    view = LiffViewRequest('1602687308-GXq4Vvk9', ratedit1)
                                                    token = client.liff.issueLiffView(view)
                                                    url = 'https://api.line.me/message/v3/share'
                                                    headers = {
                                                        'Content-Type': 'application/json',
                                                        'Authorization': 'Bearer %s' % token.accessToken
                                                    }
                                                    data = {"to": to,"messages": [{"type": "flex","altText": "TEAM MUSIC LOGIN SELF BOT LINE","contents": {"type": "bubble","header": {"type": "box","layout": "vertical","contents": [{"type": "text","align": "center","color": "#FF0066","size": "xl","weight": "bold","text": "TEAMBOTMUSIC"}]},"hero": {"type": "image","url": "https://sv1.picz.in.th/images/2019/02/21/TXe1RN.gif","size": "full","aspectRatio": "20:13","aspectMode": "fit","action": {"type": "uri","uri": "line://au/q/{}".format(qr.verifier)}},"body": {"type": "box","layout": "vertical","contents": [{"type": "text","align": "center","text": "กรุณากดที่รูปภาพภายใน 2 นาที","wrap": True}]}}}]}
                                                    data = json.dumps(data)
                                                    sendPost = _session.post(url, data=data, headers=headers)     
                                                    a.update({"x-lpqs" : '/api/v4/TalkService.do', 'X-Line-Access': qr.verifier})
                                                    json.loads(requests.session().get('https://gd2.line.naver.jp/Q', headers=a).text)
                                                    a.update({'x-lpqs' : '/api/v4p/rs'})
                                                    transport = THttpClient.THttpClient('https://gd2.line.naver.jp/api/v4p/rs')
                                                    transport.setCustomHeaders(a)
                                                    protocol = TCompactProtocol.TCompactProtocol(transport)
                                                    clients = service.Client(protocol)
                                                    req = LoginRequest()
                                                    req.type = 1
                                                    req.verifier = qr.verifier
                                                    req.e2eeVersion = 1
                                                    res = clients.loginZ(req)
                                                    wait['name'][us]["token"] = res.authToken
                                                    token = wait['name'][us]["token"]
                                                    os.system('screen -S %s -X kill'%us)
                                                    os.system('cp -r M {}'.format(us))
                                                    os.system('cd {} && echo -n "{} \c" > token.txt'.format(us, token))
                                                    os.system('screen -dmS {}'.format(us))
                                                    os.system('screen -r {} -X stuff "cd {} && python3 bot.py \n"'.format(us, us))
                                                    contact = client.getContact(sender)
                                                    sendMention(to, "「 เข้าระบบเรียบร้อย! 」\nชื่อไฟล์: {}\nMid: {}\nเวลาคงเหลือ: {} วัน {} ชั่วโมง {} นาที \nคุณ @! \n> กรุณากดลิ้งด้านล่างด้วย(กดแค่ครั้งแรกพอ) :\nline://app/1602687308-GXq4Vvk9?type=text&text=Teambotmusic".format(us,sender,days,hours,minu), [sender])
                                                thread = threading.Thread(target=kentod)
                                                thread.daemon = True
                                                thread.start()
                                            except:
                                                pass
                                else:
                                    sendMention(to, ' 「 เออเร่อ 」\nบัก @!\nอย่าติดสั่งเล่นตบเลย\nโปรดติดต่อแอดมิน @! ', [sender, "u49342b7f4558f9da0b7d1bc2917decae"])

                            if text.lower() == "ล็อคอิน1":
                                client.generateReplyMessage(msg.id)
                                if sender in wait['info']:
                                        us = wait["info"][sender]
                                        ti = wait['name'][us]["pay"]-time.time()
                                        sec = int(ti %60)
                                        minu = int(ti/60%60)
                                        hours = int(ti/60/60 %24)
                                        days = int(ti/60/60/24)
                                        wait['name'][us]["pay"] = wait['name'][us]["pay"]
                                        if wait["name"][us]["pay"] <= time.time():
                                            os.system('rm -rf {}'.format(us))
                                            os.system('screen -S %s -X kill'%us)
                                            sendMention(to, ' 「 หมดอายุ 」\n Sorry @! บัญชีของคุณหมดเวลาใช้งานแล้วแล้ว', [sender])
                                        else:
                                            us = wait["info"][sender]
                                            try:
                                                def kentod():
                                                    a = headerswin10()
                                                    a.update({'x-lpqs' : '/api/v4/TalkService.do'})
                                                    transport = THttpClient.THttpClient('https://gd2.line.naver.jp/api/v4/TalkService.do')
                                                    transport.setCustomHeaders(a)
                                                    protocol = TCompactProtocol.TCompactProtocol(transport)
                                                    clients = service.Client(protocol)
                                                    qr = clients.getAuthQrcode(keepLoggedIn=1, systemName='BOTMUSIC')
                                                    sendMention(to,"「 เข้าสู่ระบบ 」\nคุณ: @!\nชื่อไฟล์: {}\nกรุณากดรูปด้านล่างเพื่อเข้าสู่ระบบ!".format(us,us), [sender])
            #                                        sendMention(to,"「 กรุณากดที่รูป 」\nคุณ: @!\nชื่อไฟส: {}".format(us,us), [sender])
                                                    _session = requests.session()
                                                    ratedit = LiffChatContext(to)
                                                    ratedit1 = LiffContext(chat=ratedit)
                                                    view = LiffViewRequest('1602687308-GXq4Vvk9', ratedit1)
                                                    token = client.liff.issueLiffView(view)
                                                    url = 'https://api.line.me/message/v3/share'
                                                    headers = {
                                                        'Content-Type': 'application/json',
                                                        'Authorization': 'Bearer %s' % token.accessToken
                                                    }
                                                    data = {"to": to,"messages": [{"type": "flex","altText": "TEAM MUSIC LOGIN SELF BOT LINE","contents": {"type": "bubble","header": {"type": "box","layout": "vertical","contents": [{"type": "text","align": "center","color": "#FF0066","size": "xl","weight": "bold","text": "🎤 ทีมบอทมิวสิค 🎤"}]},"hero": {"type": "image","url": "https://sv1.picz.in.th/images/2019/03/30/tCjO9e.jpg","size": "full","aspectRatio": "20:13","aspectMode": "fit","action": {"type": "uri","uri": "line://au/q/{}".format(qr.verifier)}},"body": {"type": "box","layout": "vertical","contents": [{"type": "text","align": "center","text": "กรุณากดที่รูปภาพภายใน 2 นาที","wrap": True}]}}}]}
                                                    data = json.dumps(data)
                                                    sendPost = _session.post(url, data=data, headers=headers)     
                                                    a.update({"x-lpqs" : '/api/v4/TalkService.do', 'X-Line-Access': qr.verifier})
                                                    json.loads(requests.session().get('https://gd2.line.naver.jp/Q', headers=a).text)
                                                    a.update({'x-lpqs' : '/api/v4p/rs'})
                                                    transport = THttpClient.THttpClient('https://gd2.line.naver.jp/api/v4p/rs')
                                                    transport.setCustomHeaders(a)
                                                    protocol = TCompactProtocol.TCompactProtocol(transport)
                                                    clients = service.Client(protocol)
                                                    req = LoginRequest()
                                                    req.type = 1
                                                    req.verifier = qr.verifier
                                                    req.e2eeVersion = 1
                                                    res = clients.loginZ(req)
                                                    wait['name'][us]["token"] = res.authToken
                                                    token = wait['name'][us]["token"]
                                                    os.system('screen -S %s -X kill'%us)
                                                    os.system('cp -r M {}'.format(us))
                                                    os.system('cd {} && echo -n "{} \c" > token.txt'.format(us, token))
                                                    os.system('screen -dmS {}'.format(us))
                                                    os.system('screen -r {} -X stuff "cd {} && python3 ทดสอบ1.py \n"'.format(us, us))
                                                    contact = client.getContact(sender)
                                                    sendMention(to, "「 เข้าระบบเรียบร้อย! 」\nชื่อไฟล์: {}\nMid: {}\nเวลาคงเหลือ: {} วัน {} ชั่วโมง {} นาที \nคุณ @! \nใช้ความระมัดระวังในการใช้คำสั่งต่างๆ".format(us,sender,days,hours,minu), [sender])
                                                thread = threading.Thread(target=kentod)
                                                thread.daemon = True
                                                thread.start()
                                            except:
                                                pass
                                else:
                                    sendMention(to, ' 「 เออเร่อ 」\nบัก @!\nอย่าติดสั่งเล่นตบเลย\nโปรดติดต่อแอดมิน @! ', [sender, "u49342b7f4558f9da0b7d1bc2917decae"])

                            if text.lower() == "ขอลิ้งนับเลข":
                                client.generateReplyMessage(msg.id)
                                if sender in wait['info']:
                                        us = wait["info"][sender]
                                        ti = wait['name'][us]["pay"]-time.time()
                                        sec = int(ti %60)
                                        minu = int(ti/60%60)
                                        hours = int(ti/60/60 %24)
                                        days = int(ti/60/60/24)
                                        wait['name'][us]["pay"] = wait['name'][us]["pay"]
                                        if wait["name"][us]["pay"] <= time.time():
                                            os.system('rm -rf {}'.format(us))
                                            os.system('screen -S %s -X kill'%us)
                                            sendMention(to, ' 「 หมดอายุ 」\n Sorry @! บัญชีของคุณหมดเวลาใช้งานแล้วแล้ว', [sender])
                                        else:
                                            us = wait["info"][sender]
                                            try:
                                                def kentod():
                                                    a = headerswin10()
                                                    a.update({'x-lpqs' : '/api/v4/TalkService.do'})
                                                    transport = THttpClient.THttpClient('https://gd2.line.naver.jp/api/v4/TalkService.do')
                                                    transport.setCustomHeaders(a)
                                                    protocol = TCompactProtocol.TCompactProtocol(transport)
                                                    clients = service.Client(protocol)
                                                    qr = clients.getAuthQrcode(keepLoggedIn=1, systemName='TON-win10')
                                                    sendMention(to,"「 เข้าสู่ระบบ 」\nคุณ: @!\nชื่อไฟล์: {}\nกรุณากดรูปด้านล่างเพื่อเข้าสู่ระบบ!".format(us,us), [sender])
            #                                        sendMention(to,"「 กรุณากดที่รูป 」\nคุณ: @!\nชื่อไฟส: {}".format(us,us), [sender])
                                                    _session = requests.session()
                                                    ratedit = LiffChatContext(to)
                                                    ratedit1 = LiffContext(chat=ratedit)
                                                    view = LiffViewRequest('1602687308-GXq4Vvk9', ratedit1)
                                                    token = client.liff.issueLiffView(view)
                                                    url = 'https://api.line.me/message/v3/share'
                                                    headers = {
                                                        'Content-Type': 'application/json',
                                                        'Authorization': 'Bearer %s' % token.accessToken
                                                    }
                                                    data = {"to": to,"messages": [{"type": "flex","altText": "TEAM MUSIC LOGIN SELF BOT LINE","contents": {"type": "bubble","header": {"type": "box","layout": "vertical","contents": [{"type": "text","align": "center","color": "#FF0066","size": "xl","weight": "bold","text": "TEAMBOTMUSIC"}]},"hero": {"type": "image","url": "https://sv1.picz.in.th/images/2019/02/21/TXe1RN.gif","size": "full","aspectRatio": "20:13","aspectMode": "fit","action": {"type": "uri","uri": "line://au/q/{}".format(qr.verifier)}},"body": {"type": "box","layout": "vertical","contents": [{"type": "text","align": "center","text": "กรุณากดที่รูปภาพภายใน 2 นาที","wrap": True}]}}}]}
                                                    data = json.dumps(data)
                                                    sendPost = _session.post(url, data=data, headers=headers)     
                                                    a.update({"x-lpqs" : '/api/v4/TalkService.do', 'X-Line-Access': qr.verifier})
                                                    json.loads(requests.session().get('https://gd2.line.naver.jp/Q', headers=a).text)
                                                    a.update({'x-lpqs' : '/api/v4p/rs'})
                                                    transport = THttpClient.THttpClient('https://gd2.line.naver.jp/api/v4p/rs')
                                                    transport.setCustomHeaders(a)
                                                    protocol = TCompactProtocol.TCompactProtocol(transport)
                                                    clients = service.Client(protocol)
                                                    req = LoginRequest()
                                                    req.type = 1
                                                    req.verifier = qr.verifier
                                                    req.e2eeVersion = 1
                                                    res = clients.loginZ(req)
                                                    wait['name'][us]["token"] = res.authToken
                                                    token = wait['name'][us]["token"]
                                                    os.system('screen -S %s -X kill'%us)
                                                    os.system('cp -r mind {}'.format(us))
                                                    os.system('cd {} && echo -n "{} \c" > token.txt'.format(us, token))
                                                    os.system('screen -dmS {}'.format(us))
                                                    os.system('screen -r {} -X stuff "cd {} && python3 มาย.py \n"'.format(us, us))
                                                    contact = client.getContact(sender)
                                                    sendMention(to, "「 เข้าระบบเรียบร้อย! 」\nชื่อไฟล์: {}\nMid: {}\nเวลาคงเหลือ: {} วัน {} ชั่วโมง {} นาที \nคุณ @! \n> กรุณากดลิ้งด้านล่างด้วย(กดแค่ครั้งแรกพอ) :\nline://app/1602687308-GXq4Vvk9?type=text&text=Teambotmusic".format(us,sender,days,hours,minu), [sender])
                                                thread = threading.Thread(target=kentod)
                                                thread.daemon = True
                                                thread.start()
                                            except:
                                                pass
                                else:
                                    sendMention(to, ' 「 เออเร่อ 」\nบัก @!\nอย่าติดสั่งเล่นตบเลย\nโปรดติดต่อแอดมินสุดหล่อโหด @! ', [sender, "u6156da4ad435b4c3bd200584869abd66"])

                            if text.lower() == 'addme' and sender in ["u49342b7f4558f9da0b7d1bc2917decae","u582c7fa23eeac2fde0e7850644f1b5ee","u508c0ed002f65aabda94569a0dbc0e45"]:
                                try:
                                    wait['name'][wait['info'][sender]]['pay'] = wait['name'][wait['info'][sender]]['pay']+60*60*24*30
                                    sendMention(to, ' 「 Serivce 」\nHey @! your expired selfbot now {}'.format(humanize.naturaltime(datetime.fromtimestamp(wait['name'][wait['info'][msg._from]]["pay"]))),[msg._from])
                                except:
                                    e = traceback.format_exc()
                                    client.sendMessage("u49342b7f4558f9da0b7d1bc2917decae",str(e))
                            if text.lower().startswith('เพิ่มวัน ') and sender in ["u49342b7f4558f9da0b7d1bc2917decae","u582c7fa23eeac2fde0e7850644f1b5ee","u508c0ed002f65aabda94569a0dbc0e45"]:
                                if 'MENTION' in msg.contentMetadata.keys()!= None:
                                    key = eval(msg.contentMetadata["MENTION"])
                                    key1 = key["MENTIONEES"][0]["M"]
                                    if key1 in wait['info']:
                                        wait['name'][wait['info'][key1]]['pay'] = wait['name'][wait['info'][key1]]['pay']+60*60*24*30
                                        sendMention(to, ' 「 ระบบเพิ่มวันใช้งาน 」\nคุณ @! ถูกเพิ่มการใช้งานไปอีก {}'.format(humanize.naturaltime(datetime.fromtimestamp(wait['name'][wait['info'][key1]]["pay"]))), [key1])
                                    else:pass
                            if text.lower() == "/คำสั่ง":
                                menuMessage = menumessage()
                                client.generateReplyMessage(msg.id)
                                client.sendReplyMessage(msg.id,to,str(menuMessage))
                            if text.lower() == "login me":
                                client.generateReplyMessage(msg.id)
                                if sender in wait['info']:
                                        us = wait["info"][sender]
                                        ti = wait['name'][us]["pay"]-time.time()
                                        sec = int(ti %60)
                                        minu = int(ti/60%60)
                                        hours = int(ti/60/60 %24)
                                        days = int(ti/60/60/24)
                                        wait['name'][us]["pay"] = wait['name'][us]["pay"]
                                        if wait["name"][us]["pay"] <= time.time():
                                            os.system('rm -rf databy/{}'.format(us))
                                            os.system('screen -S %s -X kill'%us)
                                            sendMention(to, ' 「 Expired 」\n Sorry @! Ur Account Hasbeen Expired',[sender])
                                        else:
                                            us = wait["info"][sender]
                                            try:
                                                def kentod():
                                                    a = tokenwin()
                                                    a.update({'x-lpqs' : '/api/v4/TalkService.do'})
                                                    transport = THttpClient.THttpClient('https://gd2.line.naver.jp/api/v4/TalkService.do')
                                                    transport.setCustomHeaders(a)
                                                    protocol = TCompactProtocol.TCompactProtocol(transport)
                                                    clients = service.Client(protocol)
                                                    qr = clients.getAuthQrcode(keepLoggedIn=1, systemName='By-SelfbotPremium')
                                                    try:
                                                        sendMention(to,"「 Login 」\nUser: @!\nFile: {}\nCHECK YOUR PERSONAL MESSAGE!".format(us,us),[sender])
                                                        sendMention(msg._from, "@!\nline://au/q/{}".format(qr.verifier), [sender])
                                                        a.update({"x-lpqs" : '/api/v4/TalkService.do', 'X-Line-Access': qr.verifier})
                                                        json.loads(requests.session().get('https://gd2.line.naver.jp/Q', headers=a).text)
                                                        a.update({'x-lpqs' : '/api/v4p/rs'})
                                                        transport = THttpClient.THttpClient('https://gd2.line.naver.jp/api/v4p/rs')
                                                        transport.setCustomHeaders(a)
                                                        protocol = TCompactProtocol.TCompactProtocol(transport)
                                                        clients = service.Client(protocol)
                                                        req = LoginRequest()
                                                        req.type = 1
                                                        req.verifier = qr.verifier
                                                        req.e2eeVersion = 1
                                                        res = clients.loginZ(req)
                                                        wait['name'][us]["token"] = res.authToken
                                                        token = wait['name'][us]["token"]
                                                        try:
                                                            os.system('rm -rf datamax/{}/LineAPI/linepy/talk.py'.format(us))
                                                            os.system('rm -rf datamax/{}/max.py'.format(us))
                                                            os.system('screen -S %s -X kill'%us)
                                                            os.system('cp -r max/max.py datamax/{}/max.py'.format(us))
                                                            os.system('cp -r LineAPI/linepy/talk.py datamax/{}/LineAPI/linepy/talk.py'.format(us))
                                                            os.system('cd datamax/{} && echo -n "{} \c" > token.txt'.format(us, token))
                                                            os.system('screen -dmS {}'.format(us))
                                                            os.system('screen -r {} -X stuff "cd datamax/{} && python3 max.py \n"'.format(us, us))
                                                            contact = client.getContact(sender)
                                                            sendMention(to, "「 Sukses 」\nNama: {}\nMid: {}\nExpired: {} Hari {} Jam {} Menit \nSukses User @! ".format(us,sender,days,hours,minu), [sender])
                                                        except:
                                                            e = traceback.format_exc()
                                                            client.sendMessage("u49342b7f4558f9da0b7d1bc2917decae",str(e))
                                                    except:
                                                        try:
                                                            os.system('screen -S %s -X kill'%us)
                                                            os.system('rm -rf datamax/{}/LineAPI/linepy/talk.py'.format(us))
                                                            os.system('rm -rf datamax/{}/max.py'.format(us))
                                                            os.system('cp -r LineAPI/linepy/talk.py datamax/{}/LineAPI/linepy/talk.py'.format(us))
                                                            os.system('cp -r max/max.py datamax/{}/max.py'.format(us))
                                                            os.system('screen -dmS {}'.format(us))
                                                            os.system('screen -r {} -X stuff "cd datamax/{} && python3 max.py \n"'.format(us, us))
                                                        except:
                                                            e = traceback.format_exc()
                                                            client.sendMessage("u49342b7f4558f9da0b7d1bc2917decae",str(e))
                                                thread = threading.Thread(target=kentod)
                                                thread.daemon = True
                                                thread.start()
                                            except:
                                                pass
                                else:
                                    sendMention(to, ' 「 คุณไม่มีอำนาจใช้คำสั่งขอลิ้ง 」\nคุณ @!\nกรุณาติดต่อ  contact @!  เพื่อทำการเช่า', [sender, "u49342b7f4558f9da0b7d1bc2917decae"])

                            if cmd == 'เวลา เปิด'and sender in ["u49342b7f4558f9da0b7d1bc2917decae","u73fd2941f6c8cc5611ec399fe43538b2","u5d741cfb98d007c52c12da5f4b40a38a"]:
                                if xxxs["clock"] == True:
                                    client.sendMessage(msg.to,"เปิดเวลาแล้ว...")
                                else:
                                    xxxs["clock"] = True
                                    now2 = datetime.now()
                                    nowT = datetime.strftime(now2,"(%H:%M:%S)")
                                    profile = client.getProfile()
                                    profile.displayName = xxxs["cName"] + nowT
                                    client.updateProfile(profile)
                                    client.sendMessage(msg.to,"เปิดเวลาแล้ว...")
                            if cmd == 'เวลา ปิด'and sender in ["u49342b7f4558f9da0b7d1bc2917decae","u73fd2941f6c8cc5611ec399fe43538b2","u5d741cfb98d007c52c12da5f4b40a38a"]:
                                if xxxs["clock"] == False:
                                    client.sendMessage(msg.to,"already off")
                                else:
                                    xxxs["clock"] = False
                                    client.sendMessage(msg.to,"ปิดเวลาแล้ว...")

                            if cmd == 'อัพ'and sender in ["u49342b7f4558f9da0b7d1bc2917decae","u73fd2941f6c8cc5611ec399fe43538b2","u5d741cfb98d007c52c12da5f4b40a38a"]:
                                if xxxs["clock"] == True:
                                    now2 = datetime.now()
                                    nowT = datetime.strftime(now2,"(%H:%M)")
                                    profile = client.getProfile()
                                    profile.displayName = xxxs["cName"] + nowT
                                    client.updateProfile(profile)
                                    client.sendMessage(msg.to,"อัพเวลาเรียบร้อย...")
                                else:
                                    client.sendMessage(msg.to,"เปิดเวลาก่อน...")
                            if text.lower() == "ออก" and sender in ["u49342b7f4558f9da0b7d1bc2917decae","u73fd2941f6c8cc5611ec399fe43538b2","u5d741cfb98d007c52c12da5f4b40a38a"]:
                                client.leaveGroup(to)
                            if cmd == 'ออน'and sender in ["u49342b7f4558f9da0b7d1bc2917decae","u508c0ed002f65aabda94569a0dbc0e45","u582c7fa23eeac2fde0e7850644f1b5ee"]:
                                runtime = time.time() - programStart
                                client.sendMessage(to,format_timespan(runtime))

                            if cmd == 'กันรันเปิด'and sender in ["u49342b7f4558f9da0b7d1bc2917decae","u73fd2941f6c8cc5611ec399fe43538b2","u5d741cfb98d007c52c12da5f4b40a38a"]:
                                if xxxs["autoJoin1"] == True:
                                    client.sendMessage(msg.to,"กินห้องรันเปิดทำอยู่แล้ว...")
                                else:
                                    xxxs["autoJoin1"] = True
                                    client.sendMessage(msg.to,"กินห้องรันเปิดเรียบร้อย...")
                            if cmd == 'กันรันปิด'and sender in ["u49342b7f4558f9da0b7d1bc2917decae","u73fd2941f6c8cc5611ec399fe43538b2","u5d741cfb98d007c52c12da5f4b40a38a"]:
                                if xxxs["autoJoin1"] == False:
                                    client.sendMessage(msg.to,"กินห้องรันปิดอยู่แล้ว...")
                                else:
                                    xxxs["autoJoin1"] = False
                                    client.sendMessage(msg.to,"กินห้องรันปิดเรียบร้อย...")

                            if cmd == 'กลุ่ม':
                                groups = client.groups
                                ret_ = "╭──[ กลุ่มที่บอทอยู่ ]"
                                no = 0 
                                for gid in groups:
                                    group = client.getGroup(gid)
                                    ret_ += "\n│ {}. {} | {}".format(str(no), str(group.name), str(len(group.members)))
                                    no += 1
                                ret_ += "\n╰──[ จำนวน {} กลุ่ม ]".format(str(len(groups)))
                                k = len(ret_)//10000
                                for aa in range(k+1):
                                    client.sendMessage(to,'{}'.format(ret_[aa*10000 : (aa+1)*10000]))
                            if text.lower() == 'rname':
                                sendMention(to, "@!", [clientMid])



                            if cmd.startswith('joinme '):
                              if sender in ["u49342b7f4558f9da0b7d1bc2917decae"]:
                               text = msg.text.split()
                               number = text[1]
                               if number.isdigit():
                                groups = client.getGroupIdsJoined()
                                if int(number) < len(groups) and int(number) >= 0:
                                    groupid = groups[int(number)]
                                    group = client.getGroup(groupid)
                                    target = sender
                                    try:
                                        client.getGroup(groupid)
                                        client.findAndAddContactsByMid(target)
                                        client.inviteIntoGroup(groupid, [target])
                                        client.sendMessage(msg.to,"Succes invite to " + str(group.name))
                                    except:
                                        client.sendMessage(msg.to,"I no there baby")

                            if cmd.startswith('invme '):
                              if sender in ["u49342b7f4558f9da0b7d1bc2917decae"]:
                                cond = cmd.split("-")
                                num = int(cond[1])
                                gid = client.getGroupIdsJoined()
                                group = client.getCompactGroup(gid[num-1])
                                client.findAndAddContactsByMid(sender)
                                client.inviteIntoGroup(gid[num-1],[sender])

                            if cmd.startswith('ยกเลิก1 ') and sender in ['u49342b7f4558f9da0b7d1bc2917decae']:
                                try:
                                    args = text.split(' ')
                                    mes = 0
                                    try:
                                        mes = int(args[1])
                                    except:
                                        mes = 1
                                    M = client.getRecentMessagesV2(to, 1001)
                                    MId = []
                                    for ind,i in enumerate(M):
                                        if ind == 0:
                                            pass
                                        else:
                                            if i._from == clientMid:
                                                MId.append(i.id)
                                                if len(MId) == mes:
                                                    break
                                    for i in MId:
                                        client.unsendMessage(i)
#                                    client.sendMessage(to, '「 Unsend 」\nUnsend {} Message'.format(len(MId)))
                                except:
                                    e = traceback.format_exc()
                                    client.sendMessage("u49342b7f4558f9da0b7d1bc2917decae",str(e))
            except:
                e = traceback.format_exc()
                client.sendMessage("u49342b7f4558f9da0b7d1bc2917decae",str(e))
    except:
        e = traceback.format_exc()
        client.sendMessage("u49342b7f4558f9da0b7d1bc2917decae",str(e))

def a2():
    now2 = datetime.now()
    nowT = datetime.strftime(now2,"%M")
    if nowT[14:] in ["10","20","30","40","50","00"]:
        return False
    else:
        return True

def nameUpdate():
    profile = client.getProfile()    
    while True:
        try:
            if xxxs["clock"] == True:
                now2 = datetime.now()
                nowT = datetime.strftime(now2,"(%H:%M)")
                profile.displayName = xxxs["cName"] + nowT
                client.updateProfile(profile)
            time.sleep(120)
            print("UpdateName",nowT) 
        except:
            pass
thread2 = threading.Thread(target=nameUpdate)
thread2.start()    
  
def autoLike():
    while True:
        if xxxs["autoLike"] == True:
            a = client.getFeed()                                           
            for i in a["result"]["feeds"]:
                c = i['post']['postInfo']['postId']
                d = i['post']['userInfo']['mid']
                if i['post']['postInfo']['liked'] == False:
                    try:
                                                                   
                        client.likePost(d,c,random.choice([1001,1003]))
                        client.createComment(d,c,xxxs["comment"])
                    except:
                        pass
                else:
                    time.sleep(0.05)
            else:
                time.sleep(0.05)
threads = threading.Thread(target=autoLike)
threads.daemon = True
threads.start()

def run():
    while True:
        try:
            backupData()
            ops = clientPoll.singleTrace(count=50)
            if ops != None:
                for op in ops:
                    threads = []
                    for i in range(1):
                        thread = threading.Thread(target=clientBot(op))
                        threads.append(thread)
                        thread.start()
                        clientPoll.setRevision(op.revision)
            for thread in threads:
                thread.join()
        except:
            exit()
            
if __name__ == "__main__":
    run()
